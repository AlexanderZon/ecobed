/*
 *  This file is a part of WP Business Directory Wordpress Plugin
 *  http://codecanyon.net/user/ecabuk
 *  Copyright (c) 2012 Evrim Çabuk
 */

jQuery(document).ready(function($) {
    
    var states= [];
    var cities= [];
                                                                                    
    function check_country_status(){
        if($('#bd_country option:selected').attr('org_name') == 'dontchoose'){
            return false;
        }else{
            return true;
        }
    }
    
    $( "#bd_state" ).autocomplete({
        source: function(request, response) {
            response($.ui.autocomplete.filter(states, request.term) );
        },
        select: function(event, ui) {
            get_cities($('#bd_state').val(),$('#bd_country option:selected').attr('org_name'));
            $('#bd_city').val('');
        },
        minLength: 1
    });
    
    $( "#bd_city" ).autocomplete({
        source: function(request, response) {
            response($.ui.autocomplete.filter(cities, request.term) );
        },
        minLength: 1
    });
    
    $('#bd_country').change(function(){
        
        if(check_country_status()){
            get_states($('#bd_country option:selected').attr('org_name'));
        }
        $('#bd_state').removeAttr('readonly').val('');
        $('#bd_city').removeAttr('readonly').val('');
    });
    
    $('#bd_state').change(function(){
        get_cities($('#bd_state').val(),$('#bd_country option:selected').attr('org_name'));
        $('#bd_city').removeAttr('readonly').val('');
    });
    
    function get_states(country){
        
        $.ajax({
            url: 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent('select * from geo.states where place="'+country+'"')+'&format=json',
            dataType: 'jsonp',
            jsonp: 'callback',
            jsonpCallback: 'cbFunc'
        }).done(function( data ) {
            cbFunc(data);
        });
        
        function cbFunc (data){
            states.length = 0;
            if(data.query.count>2){
                $.each(data.query.results.place,function(index,value){
                    states.push(value.name);
                });
                states=states.sort();
            }
        
        }
    }
    
    function get_cities(state,country){
        $.ajax({
            url: 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent('select * from geo.counties where place="' + state + ' ' + country + '"')+'&format=json',
            dataType: 'jsonp',
            jsonp: 'callback',
            jsonpCallback: 'cbFunc'
        }).done(function( data ) {
            cbFunc(data);
        });
        
        function cbFunc (data){
            cities.length = 0;
            if(data.query.count>2){
                $.each(data.query.results.place,function(index,value){
                    cities.push(value.name);
                });
                cities=cities.sort();
            }
        }
    }
    
    
});