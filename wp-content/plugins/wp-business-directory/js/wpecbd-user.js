/*
 *  This file is a part of WP Business Directory Wordpress Plugin
 *  http://codecanyon.net/user/ecabuk
 *  Copyright (c) 2012 Evrim Çabuk
 */

function com_logo_uploader(data) {
    new qq.FileUploader({
        element: document.getElementById(data.element),
        action: data.action,
        params: {
            post_id: data.post_id,
            nonce_user: data.nonce_user,
            logo: 'true'
        },
        messages: {
            typeError: data.error.type,
            sizeError: data.error.size,
            emptyError: data.error.empty,
            onLeave: data.error.onleave
        },
        sizeLimit: data.max_size,
        onSubmit: function(id, fileName){
            jQuery("#bd_user_logo_upload").hide();
            jQuery("#bd_ajax_logo_upload").show();
        },
        onComplete: function(id, fileName, r){
            if(r.success == 'true'){
                jQuery("#company_logo").append('<img class="bd_img" src="'+r.url+'" /><span id="del_com_logo"></span>');
            }
            jQuery("#bd_ajax_logo_upload").hide();
        },
        onCancel: function(id, fileName){
            jQuery("#bd_user_logo_upload").show();
            jQuery("#bd_ajax_logo_upload").hide();
        }
    });

    jQuery("#del_com_logo").live("click", function(){
        jQuery.ajax({
            type: "GET",
            dataType: "json",
            url: data.action,
            data: 'bd_action=delete_logo&post_id='+data.post_id+'&nonce_user='+data.nonce_user
        }).done(function( msg ) {
            if(msg.success == 'true'){
                jQuery("#bd_user_logo_upload").show();
                jQuery("#company_logo").empty();
            }
        });
    });
}

function com_image_uploader(data){
    new qq.FileUploader({
        element: document.getElementById(data.element),
        action: data.action,
        params: {
            post_id: data.post_id,
            nonce_user: data.nonce_user
        },
        messages: {
            typeError: data.error.type,
            sizeError: data.error.size,
            emptyError: data.error.empty,
            onLeave: data.error.onleave
        },
        sizeLimit: data.max_size,
        onSubmit: function(id, fileName){
            jQuery("#bd_user_image_upload").hide();
            jQuery("#bd_ajax_image_upload").show();
        },
        onComplete: function(id, fileName, r){
            if(r.success == 'true'){
                jQuery("#company_images").append(
                    '<div id="bd_img_cont-'+r.attachment_id+'" class="grid-3-12">'+
                    '<div class="image_div_inner">'+
                    '<div class="img_div"><img class="bd_img" src="'+r.url+'" /></div>'+
                    '<div class="del_com_img"><a href="#" id="bd_del_img-'+r.attachment_id+'" attachment_id="'+r.attachment_id+'">'+image_section_translate.delete_image+'</a></div>'+
                    '<div class="title_com_img"><a href="#" id="bd_title_img-'+r.attachment_id+'" attachment_id="'+r.attachment_id+'">'+image_section_translate.edit_title+'</a></div>'+
                    '<input id="img_title-'+r.attachment_id+'" value="" name="images['+r.attachment_id+']" type="hidden" />'+
                    '</div>'+
                    '</div>'
                    );
            }
            if(r.remained > 0){
                jQuery("#bd_user_image_upload").show();
                jQuery("#bd_image_reach_limit").hide();
            }else{
                jQuery("#bd_image_reach_limit").show().delay(9000).fadeTo('slow',0.7);
            }
            jQuery("#bd_ajax_image_upload").hide();
        },
        onCancel: function(id, fileName){
            jQuery("#bd_user_image_upload").show();
            jQuery("#bd_ajax_image_upload").hide();
        }
    });
    
    jQuery('[id^=bd_del_img]').live('click',function(){
        var delete_confirm=confirm(image_section_translate.confirm_delete);
        if(delete_confirm==true){
            var attachment_id = jQuery(this).attr("attachment_id");
            jQuery.ajax({
                type: "GET",
                dataType: "json",
                url: data.action,
                data: {
                    bd_action:'delete_img',
                    id:attachment_id,
                    post_id:data.post_id,
                    nonce_user:data.nonce_user
                }
            }).done(function( msg ) {
                if(msg.success == 'true'){
                    jQuery("#bd_img_cont-"+attachment_id).fadeOut('slow');
                    if(msg.remained > 0){
                        jQuery("#bd_user_image_upload").show();
                        jQuery("#bd_image_reach_limit").hide();
                    }else{
                        jQuery("#bd_image_reach_limit").show().delay(9000).fadeTo('slow',0.7);
                    }
                }
            });   
        }
    });
}

function com_tags(data){
    var bd_total_tag=0;
    var bd_max_tag=data.max_tag;
    
    jQuery('#tags').tagit({
        availableTags:data.tags,
        allowSpaces: true,
        onTagAdded: function(event, tag){
            if(data.max_tag){
                bd_total_tag++;
                if( bd_total_tag >= bd_max_tag && jQuery('.max_tag_limit').length==0){
                    jQuery('.tagit-new').children().prop('disabled', true);
                    jQuery('#tag_err').append('<label for="tags" generated="true" class="reach_error max_tag_limit bd_hidden">'+data.tranlate.max_tag+'</label>');
                    jQuery('.max_tag_limit').fadeIn('slow').delay(9000).fadeTo('slow',0.7);
                }
            }
        },
        onTagRemoved: function(event, tag){
            if(data.max_tag){
                bd_total_tag--;
                if( bd_total_tag >= bd_max_tag){
                    jQuery('.tagit-new').children().prop('disabled', true);
                }else{
                    jQuery('.tagit-new').children().prop('disabled', false);
                    jQuery('.max_tag_limit').fadeOut('fast').remove();
                }
            }
        }
    });
    bd_total_tag=jQuery('.tagit').children().size() - 1;
}

function com_category(data){
    jQuery('#categories').multiselect({
        header: false,
        selectedList: 3,
        noneSelectedText:data.translate.noneselected,
        selectedText:data.translate.selectedtext,
        click: function(e){
            if(data.max_cat){
                if( jQuery(this).multiselect("widget").find("input:checked").length > data.max_cat){
                    if(jQuery('.max_cat_limit').length==0){
                        jQuery('#cat_err').append('<label for="categories[]" generated="true" class="reach_error max_cat_limit bd_hidden">'+data.translate.limit+'</label>');
                        jQuery('.max_cat_limit').fadeIn('slow').delay(9000).fadeTo('slow',0.7);
                    }
                    return false;
                } else {
                    jQuery('.max_cat_limit').fadeOut('fast').remove();
                }
            }
        },
        beforeoptgrouptoggle:function(){
            return false;
        }
    });
}

jQuery('[id^=bd_title_img]').live('click',function(){
    var id=jQuery(this).attr('attachment_id');
    var title=prompt("Please enter title:",jQuery('#img_title-'+id).val());
    if (title!=false){
        jQuery('#img_title-'+id).val(title);   
    }
});