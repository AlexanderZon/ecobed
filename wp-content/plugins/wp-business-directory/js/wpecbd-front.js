/*
 *  This file is a part of WP Business Directory Wordpress Plugin
 *  http://codecanyon.net/user/ecabuk
 *  Copyright (c) 2012 Evrim Çabuk
 */

function bd_widget_search_map(data){
    if(data.show_map){
        var addresspickerMap = jQuery( '#'+data.element.searcher ).addresspicker({
            elements: {
                map:      '#'+data.element.map,
                lat:      '#'+data.element.lat,
                lng:      '#'+data.element.lng
            },
            mapOptions: {
                zoom: 10,
                center: new google.maps.LatLng(data.lat, data.lng),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            selected:function(){
                alert('Sectin');
            }
        });
        var gmarker = addresspickerMap.addresspicker( "marker");
        gmarker.setVisible(true);
        addresspickerMap.addresspicker( "updatePosition");   
    }else{
        jQuery( '#'+data.element.searcher ).addresspicker({
            elements: {
                lat:      '#'+data.element.lat,
                lng:      '#'+data.element.lng
            }
        });
    }
}

function bd_widget_search_submit_form(data){
    var post_data={
        term:jQuery("#"+data.element.term).val(),
        cats:jQuery("#"+data.element.cats).val(),
        lat:jQuery("#"+data.element.lat).val(),
        lng:jQuery("#"+data.element.lng).val(),
        address:jQuery("#"+data.element.address).val(),
        widget_id:data.widget_id,
        detect_location:data.detect_location
    };
    jQuery.fancybox({
        href:data.url+'&'+jQuery.param(post_data),
        height: '90%',
        type: 'iframe',
        width: 960,
        autoScale:false,
        centerOnScroll:true,
        hideOnOverlayClick:false,
        hideOnContentClick:false,
        enableEscapeButton:false,
        margin:30,
        padding:5
    });
}




jQuery(document).ready(function($) {
    $("#bd_map_iframe").fancybox({
        'width'		: '75%',
        'height'	: '75%',
        'autoScale'	: false,
        'transitionIn'	: 'none',
        'transitionOut'	: 'none',
        'type'		: 'iframe'
    });
    
    $("a[rel=bd_img_group]").fancybox({
        'transitionIn'	: 'none',
        'transitionOut'	: 'none',
        'titlePosition' : 'over',
        'titleFormat'	: function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
        }
    });
        
    $('.wpbd_user_edit,#new_company_link').live('click',function(){
        $.fancybox({
            href:$(this).attr('href'),
            height: '96%',
            type: 'iframe',
            width: 660,
            autoScale:false,
            centerOnScroll:true,
            hideOnOverlayClick:false,
            margin:30,
            padding:5,
            onCleanup:function(){
                location.reload(true);
            }
        //Onclose refresh page?
        });
        return false;
    });
});