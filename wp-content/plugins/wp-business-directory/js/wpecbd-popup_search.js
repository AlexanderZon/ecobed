/*
 *  This file is a part of WP Business Directory Wordpress Plugin
 *  http://codecanyon.net/user/ecabuk
 *  Copyright (c) 2012 Evrim Çabuk
 */

var map, geocoder, d_marker, d_marker_p, iw, directionsDisplay, bounds, adUnit;
var markers = [];
var n_timeout = null;
var search_box_open=true;
var directions_panel_open=false;
var directionsService = new google.maps.DirectionsService();

function initialize() {
    if(address_setted){
        d_marker_p=new google.maps.LatLng(jQuery('#lat').val(), jQuery('#lng').val());
    }else{
        d_marker_p=new google.maps.LatLng(def_location.lat, def_location.lng);   
    }
    var myOptions = {
        center: d_marker_p,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
    bounds = new google.maps.LatLngBounds();
    geocoder = new google.maps.Geocoder();
    d_marker=new google.maps.Marker({
        map: map,
        draggable: true,
        position:d_marker_p,
        zIndex:0,
        title:lang.map.d_marker_title,
        icon:new google.maps.MarkerImage(
            plugin_img_folder+'marker-images/image.png',
            new google.maps.Size(32,37),
            new google.maps.Point(0,0),
            new google.maps.Point(16,37)
            ),
        shadow:new google.maps.MarkerImage(
            plugin_img_folder+'marker-images/shadow.png',
            new google.maps.Size(54,37),
            new google.maps.Point(0,0),
            new google.maps.Point(16,37)
            ),
        shape:{
            coord: [29,0,30,1,31,2,31,3,31,4,31,5,31,6,31,7,31,8,31,9,31,10,31,11,31,12,31,13,31,14,31,15,31,16,31,17,31,18,31,19,31,20,31,21,31,22,31,23,31,24,31,25,31,26,31,27,31,28,31,29,30,30,29,31,23,32,22,33,21,34,20,35,19,36,12,36,11,35,10,34,9,33,8,32,2,31,1,30,0,29,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,0,2,1,1,2,0,29,0],
            type: 'poly'
        }
    });
    google.maps.event.addListener(d_marker, 'drag',function(){
        change_adress_by_marker();
    });
    
    if(address_setted ==false)
        change_adress_by_marker();
    
    if(detect_location==true && address_setted==false){
        prepareGeolocation();
        doGeolocation();
    }
    
    if(address_setted==true && (jQuery('#term').val().length > 2 || jQuery('#cat_selector option:selected').length > 0) ){
        submit_search_form();
    }
    
    directionsDisplay = new google.maps.DirectionsRenderer({
        'map': map,
        'preserveViewport': true,
        'draggable': true,
        'panel':document.getElementById("directions_panel")
    });
    
    google.maps.event.addListener(directionsDisplay, 'directions_changed',
        function() {
            directionsDisplay.getDirections();
        });
        
    jQuery("#cat_selector").multiselect({
        header: false,
        noneSelectedText:lang.cat.noneSelectedText,
        selectedText:lang.cat.selectedText
    });   
    
    autocomplete_address();
    
    jQuery('#search_button').click(submit_search_form);
    
    jQuery('#bd_com_search_form').submit(function(){
        return false;
    });
    
    jQuery('.search_close,.search_open').click(toggle_search_box);
    
    jQuery(document).on("click", '.iw_com_get_direction', function(){
        display_direction(jQuery(this).attr('marker_id'));
    });
    
    jQuery('.directions_close').click(close_directions_panel);
    
    if(adsense.active){
        var adUnitDiv = document.createElement('div');
        var adUnitOptions = {
            format: google.maps.adsense.AdFormat[adsense.ad_unit],
            position: google.maps.ControlPosition[adsense.ad_position],
            map: map,
            visible: true,
            publisherId: adsense.publisherId
        }
        adUnit = new google.maps.adsense.AdUnit(adUnitDiv, adUnitOptions);
    }
    
}

function display_direction(i){
    var m_unit;
    directionsDisplay.setMap(map);
    if(measurement_unit=='km'){
        m_unit=google.maps.UnitSystem.METRIC;
    }else{
        m_unit=google.maps.UnitSystem.IMPERIAL;
    }
    var request = {
        origin:d_marker.getPosition(),
        destination:markers[i].getPosition(),
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        unitSystem:m_unit
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            if(!directions_panel_open)open_directions_panel();
            directionsDisplay.setDirections(response);
        }else{
            info_box(lang.directions.error,'error');
        }
    });
}

function open_directions_panel(){
    directions_panel_open=true;
    jQuery('#places').hide('fast');
    jQuery('#directions_main').show(0).animate({
        height: "100%"
    }, 1500 );
}

function close_directions_panel(){
    directions_panel_open=false;
    jQuery('#directions_main').animate({
        height: "0px"
    }, 1500 ,function(){
        jQuery('#places').show('slow');
    });
    directionsDisplay.setMap(null); 
}

function submit_search_form(){

    var data={
        is_bd_search:true,
        lat:jQuery('#lat').val(),
        lng:jQuery('#lng').val(),
        term:jQuery('#term').val(),
        cats:jQuery('#cat_selector').val(),
        unit:measurement_unit,
        distance:jQuery('#distance').val()
    };
    
    if(data.term.length < 3 && jQuery('#cat_selector option:selected').length < 1 ){
        info_box(lang.search.required,'warning');
        return;
    }
    
    jQuery.ajax({
        type: "POST",
        dataType: "json",
        url:ajax_search_url,
        cache:false,
        data:jQuery.param(data),
        beforeSend:function(){
            info_box(lang.search.searching,'info',true);
        },
        error:function(){
            info_box(lang.search.error,'error');
        }
    }).done(function(msg) {
        list_founded_companies(msg);
    });

}

function autocomplete_address(element){
    jQuery("#autocomplete_adress").autocomplete({
        source: function(request, response) {
            geocoder.geocode( {
                'address': request.term
            }, function(results, status) {
                response(jQuery.map(results, function(item) {
                    return {
                        label:  item.formatted_address,
                        value: item.formatted_address,
                        latitude: item.geometry.location.lat(),
                        longitude: item.geometry.location.lng()
                    }
                }));
            })
        },
        select: function(event, ui) {
            $("#lat").val(ui.item.latitude);
            $("#lng").val(ui.item.longitude);
            var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
            d_marker.setPosition(location);
            map.panTo(location);
            
        }
    });
}

function change_adress_by_marker(){
    geocoder.geocode({
        'latLng': d_marker.getPosition()
    },
    function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                jQuery('#lat').val(d_marker.getPosition().lat());
                jQuery('#lng').val(d_marker.getPosition().lng());
            }
        }
    });
}

function list_founded_companies(d){
    if (d.error == undefined){
        clearResults();
        clearMarkers();
        jQuery.each(d,function(i,v){
            markers[i] = new google.maps.Marker({
                position: new google.maps.LatLng(v.lat, v.lng),
                animation: google.maps.Animation.DROP,
                zIndex:10+i
            });
            google.maps.event.addListener(markers[i], 'click', showInfoWindow(v, i));
            setTimeout(dropMarker(i), i * 125);
            addResult(v, i);
            bounds.extend(new google.maps.LatLng(v.lat, v.lng));
            map.fitBounds(bounds);
        });
        
        if(d.length >1){
            info_box(lang.search.xp_found.replace('#',d.length),'success');
        }else{
            info_box(lang.search.one_found,'success');
        }
    }else{
        info_box(lang.search.no_found,'warning');
    }  
}

function addResult(v, i) {
    var is_p='';
    if(v.is_premium)is_p=" premium_li";
    jQuery('#places_ul').append('<li class="com_li'+is_p+'" id="com_id_'+v.id+'">'+
        '<div class="places_inner">'+
        '<div class="place_name">'+v.post_title+'</div>'+
        '<div class="place_desc">'+v.post_excerpt+'</div>'+
        '<div class="place_distance">'+v.distance+' '+measurement_unit+'</div>'+
        '</div>'+
        '</li>');
    
    jQuery('#com_id_'+v.id).live({
        click: function() {
            google.maps.event.trigger(markers[i], 'click');
        }
    });
}

function clearResults() {
    jQuery('#places_ul').empty();
}

function showInfoWindow(v,i) {
    return function(place) {
        if (iw) {
            iw.close();
            iw = null;
        }

        iw = new google.maps.InfoWindow({
            content: getIWContent(v,i)
        });
        iw.open(map, markers[i]);
    }
}

function getIWContent(v,i) {
    //Is premium?
    var is_p='';
    if(v.is_premium)is_p=" premium_iw";
    //Has logo?
    var logo_div='';
    var no_logo='';
    if(v.logo_url ==''){
        no_logo=' no_logo';
    }else{
        logo_div='<table class="iw_logo_table"><tr><td class="iw_logo_table_td">'+
        '<img class="iw_logo_img" src="'+v.logo_url+'" />'+
        '</td></tr></table>';
    }
    
    //Prepare Address
    var address='';
    if(v.address.length>3){
        address='<span class="iw_address_title">'+lang.iw.address+': </span>'+'<span class="iw_address">'+v.address+'</span>';
    }
    
    //Prepare Phone
    var phone='';
    if(v.phone.length>3){
        phone='<span class="iw_phone_title">'+lang.iw.phone+': </span>'+'<span class="iw_phone">'+v.phone+'</span>';
    }
    
    var content = '<div class="com_iw'+is_p+'" id="com_iw_id_'+v.id+'">'+
    '<div class="iw_com_name">'+
    v.post_title+
    '</div>'+ //End Company Name
    logo_div+
    '<div class="iw_com_detail'+no_logo+'">'+
    address+'<br /><br />'+phone+
    '</div>'+ //End Details
    '<div class="iw_com_extra">'+
    '<div class="iw_com_more_info_div">'+
    '<a class="iw_com_more" href="'+v.url+'" target="_blank" title="'+lang.map.visit_company_page+'">'+lang.map.more_info+'</a>'+
    '</div>'+ //End More Info
    '<div class="iw_com_get_direction_div">'+
    '<a class="iw_com_get_direction" marker_id="'+i+'" href="#" title="'+lang.map.get_direction+'" >'+lang.map.get_direction+'</a>'+
    '</div>'+ //End Get Direction
    '</div>'+ //End Extra
    '</div>'; //End Main Container
    return content;
}

function dropMarker(i) {
    return function() {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        if (markers[i]) {
            markers[i].setMap(null);
            markers[i] = null;
        }
    }
}

function doGeolocation() {
    info_box(lang.position.on_progress,'info');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(positionSuccess, positionError);
    } else {
        positionError(-1);
    }
}

function positionError(err) {
    var msg;
    switch(err.code) {
        case err.UNKNOWN_ERROR:
            msg = lang.position.unable_to_find;
            break;
        case err.PERMISSION_DENINED:
            msg = lang.position.permisson_denied;
            break;
        case err.POSITION_UNAVAILABLE:
            msg = lang.position.unknown;
            break;
        case err.BREAK:
            msg = lang.position.too_long;
            break;
        default:
            msg = lang.position.no_browser_support;
    }
    info_box(msg,'error');
}

function positionSuccess(position) {
    // Centre the map on the new location
    var coords = position.coords || position.coordinate || position;
    var latLng = new google.maps.LatLng(coords.latitude, coords.longitude);
    map.setCenter(latLng);
    map.setZoom(12);
    d_marker.setPosition(latLng);
    map.panTo(latLng);
    change_adress_by_marker();
    info_box(lang.position.success,'success');
}

function info_box(msg,d,dontclose){
    var delay_ms=10000;
    var e=jQuery('#info');
    
    
    if (n_timeout){
        clearTimeout(n_timeout);
        e.fadeOut(100,prepare_info_box(e,msg,d));
    }else{
        prepare_info_box(e,msg,d);
    }
    
    if(dontclose){
        delay_ms=99000;
    }
    
    e.fadeIn(200,function(){
        n_timeout=setTimeout(function() {
            e.fadeOut(99);
        }, delay_ms); 
    });
}

function prepare_info_box(e,msg,d){
    e.removeClass('infobox_info').removeClass('infobox_warning').removeClass('infobox_error').removeClass('infobox_success');
    switch(d) {
        case 'info':
            e.addClass('infobox_info');
            break;
        case 'warning':
            e.addClass('infobox_warning');
            break;
        case 'error':
            e.addClass('infobox_error');
            break;
        case 'success':
            e.addClass('infobox_success');
            break;
    }
    e.text(msg);
}

function toggle_search_box(){
    var e=jQuery('#search_base');
    var e_i=jQuery('#info');
    var s_width=e.outerWidth();
    var i_width=e_i.outerWidth();
    if(search_box_open){
        e.animate({
            right:-s_width
        }, 1000);
        e_i.width(i_width+s_width-10);
        search_box_open=false;
        jQuery('.search_open').show('slow');
    }else{
        search_box_open=true;
        e.animate({
            right:10
        }, 1000);
        e_i.width(i_width-s_width);
        jQuery('.search_open').hide('slow');
    }
}

function prepareGeolocation(opt_force) {
    if ( opt_force || typeof navigator.geolocation == "undefined" || navigator.geolocation.shim ) (function(){

        // -- BEGIN GEARS_INIT
        (function() {
            // We are already defined. Hooray!
            if (window.google && google.gears) {
                return;
            }

            var factory = null;

            // Firefox
            if (typeof GearsFactory != 'undefined') {
                factory = new GearsFactory();
            } else {
                // IE
                try {
                    factory = new ActiveXObject('Gears.Factory');
                    // privateSetGlobalObject is only required and supported on WinCE.
                    if (factory.getBuildInfo().indexOf('ie_mobile') != -1) {
                        factory.privateSetGlobalObject(this);
                    }
                } catch (e) {
                    // Safari
                    if ((typeof navigator.mimeTypes != 'undefined')
                        && navigator.mimeTypes["application/x-googlegears"]) {
                        factory = document.createElement("object");
                        factory.style.display = "none";
                        factory.width = 0;
                        factory.height = 0;
                        factory.type = "application/x-googlegears";
                        document.documentElement.appendChild(factory);
                    }
                }
            }

            // *Do not* define any objects if Gears is not installed. This mimics the
            // behavior of Gears defining the objects in the future.
            if (!factory) {
                return;
            }

            // Now set up the objects, being careful not to overwrite anything.
            //
            // Note: In Internet Explorer for Windows Mobile, you can't add properties to
            // the window object. However, global objects are automatically added as
            // properties of the window object in all browsers.
            if (!window.google) {
                google = {};
            }

            if (!google.gears) {
                google.gears = {
                    factory: factory
                };
            }
        })();
        // -- END GEARS_INIT

        var GearsGeoLocation = (function() {
            // -- PRIVATE
            var geo = google.gears.factory.create('beta.geolocation');

            var wrapSuccess = function(callback, self) { // wrap it for lastPosition love
                return function(position) {
                    callback(position);
                    self.lastPosition = position;
                }
            }

            // -- PUBLIC
            return {
                shim: true,

                type: "Gears",

                lastPosition: null,

                getCurrentPosition: function(successCallback, errorCallback, options) {
                    var self = this;
                    var sc = wrapSuccess(successCallback, self);
                    geo.getCurrentPosition(sc, errorCallback, options);
                },

                watchPosition: function(successCallback, errorCallback, options) {
                    geo.watchPosition(successCallback, errorCallback, options);
                },

                clearWatch: function(watchId) {
                    geo.clearWatch(watchId);
                },

                getPermission: function(siteName, imageUrl, extraMessage) {
                    geo.getPermission(siteName, imageUrl, extraMessage);
                }

            };
        })();

        var AjaxGeoLocation = (function() {
            // -- PRIVATE
            var loading = false;
            var loadGoogleLoader = function() {
                if (!hasGoogleLoader() && !loading) {
                    loading = true;
                    var s = document.createElement('script');
                    s.src = 'http://www.google.com/jsapi?callback=_google_loader_apiLoaded';
                    s.type = "text/javascript";
                    document.getElementsByTagName('body')[0].appendChild(s);
                }
            };

            var queue = [];
            var addLocationQueue = function(callback) {
                queue.push(callback);
            }

            var runLocationQueue = function() {
                if (hasGoogleLoader()) {
                    while (queue.length > 0) {
                        var call = queue.pop();
                        call();
                    }
                }
            }

            window['_google_loader_apiLoaded'] = function() {
                runLocationQueue();
            }

            var hasGoogleLoader = function() {
                return (window['google'] && google['loader']);
            }

            var checkGoogleLoader = function(callback) {
                if (hasGoogleLoader()) return true;

                addLocationQueue(callback);

                loadGoogleLoader();

                return false;
            };

            loadGoogleLoader(); // start to load as soon as possible just in case

            // -- PUBLIC
            return {
                shim: true,

                type: "ClientLocation",

                lastPosition: null,

                getCurrentPosition: function(successCallback, errorCallback, options) {
                    var self = this;
                    if (!checkGoogleLoader(function() {
                        self.getCurrentPosition(successCallback, errorCallback, options);
                    })) return;

                    if (google.loader.ClientLocation) {
                        var cl = google.loader.ClientLocation;

                        var position = {
                            latitude: cl.latitude,
                            longitude: cl.longitude,
                            altitude: null,
                            accuracy: 43000, // same as Gears accuracy over wifi?
                            altitudeAccuracy: null,
                            heading: null,
                            velocity: null,
                            timestamp: new Date(),

                            // extra info that is outside of the bounds of the core API
                            address: {
                                city: cl.address.city,
                                country: cl.address.country,
                                country_code: cl.address.country_code,
                                region: cl.address.region
                            }
                        };

                        successCallback(position);

                        this.lastPosition = position;
                    } else if (errorCallback === "function")  {
                        errorCallback({
                            code: 3, 
                            message: "Using the Google ClientLocation API and it is not able to calculate a location."
                        });
                    }
                },

                watchPosition: function(successCallback, errorCallback, options) {
                    this.getCurrentPosition(successCallback, errorCallback, options);

                    var self = this;
                    var watchId = setInterval(function() {
                        self.getCurrentPosition(successCallback, errorCallback, options);
                    }, 10000);

                    return watchId;
                },

                clearWatch: function(watchId) {
                    clearInterval(watchId);
                },

                getPermission: function(siteName, imageUrl, extraMessage) {
                    // for now just say yes :)
                    return true;
                }

            };
        })();

        // If you have Gears installed use that, else use Ajax ClientLocation
        navigator.geolocation = (window.google && google.gears && google.gears.factory.create) ? GearsGeoLocation : AjaxGeoLocation;

    })();
}