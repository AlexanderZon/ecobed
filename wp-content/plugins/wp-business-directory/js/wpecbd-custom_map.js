/*
 *  This file is a part of WP Business Directory Wordpress Plugin
 *  http://codecanyon.net/user/ecabuk
 *  Copyright (c) 2012 Evrim Çabuk
 */

function bd_custom_map_initialize(data,map_element,def_lat,def_lng){
    
    var map,bounds,iw;
    var markers = [];
    
    var myOptions = {
        center: new google.maps.LatLng(def_lat,def_lng),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById(map_element),myOptions);
    bounds = new google.maps.LatLngBounds();
    list_founded_companies(data);
    
    
    function list_founded_companies(d){
        jQuery.each(d,function(i,v){
            markers[i] = new google.maps.Marker({
                position: new google.maps.LatLng(v.lat, v.lng),
                animation: google.maps.Animation.DROP,
                zIndex:10+i
            });
            google.maps.event.addListener(markers[i], 'click', showInfoWindow(v, i));
            setTimeout(dropMarker(i), i * 125);
            bounds.extend(new google.maps.LatLng(v.lat, v.lng));
            map.fitBounds(bounds);
        });
    }
    
    function showInfoWindow(v,i) {
        return function(place) {
            if (iw) {
                iw.close();
                iw = null;
            }

            iw = new google.maps.InfoWindow({
                content: getIWContent(v,i)
            });
            iw.open(map, markers[i]);
        }
    }

    function getIWContent(v,i) {
        //Is premium?
        var is_p='';
        if(v.is_premium)is_p=" premium_iw";
        //Has logo?
        var logo_div='';
        var no_logo='';
        if(v.logo_url ==''){
            no_logo=' no_logo';
        }else{
            logo_div='<table class="iw_logo_table"><tr><td class="iw_logo_table_td">'+
            '<img class="iw_logo_img" src="'+v.logo_url+'" />'+
            '</td></tr></table>';
        }
    
        //Prepare Address
        var address='';
        if(v.address.length>3){
            address='<span class="iw_address_title">Address: </span>'+'<span class="iw_address">'+v.address+'</span>';
        }
    
        //Prepare Phone
        var phone='';
        if(v.phone.length>3){
            phone='<span class="iw_phone_title">Phone: </span>'+'<span class="iw_phone">'+v.phone+'</span>';
        }
    
        var content = '<div class="com_iw'+is_p+'" id="com_iw_id_'+v.id+'">'+
        '<div class="iw_com_name">'+
        v.post_title+
        '</div>'+ //End Company Name
        logo_div+
        '<div class="iw_com_detail'+no_logo+'">'+
        address+'<br /><br />'+phone+
        '</div><br clear="ALL" />'+ //End Details
        '<div class="iw_com_extra">'+
        '<div class="iw_com_more_info_div">'+
        '<a class="iw_com_more" href="'+v.url+'" title="Visit Company Page">More Info</a>'+
        '</div>'+ //End More Info
        '</div>'+ //End Extra
        '</div>'; //End Main Container
        return content;
    }

    function dropMarker(i) {
        return function() {
            markers[i].setMap(map);
        }
    }
    
    
}