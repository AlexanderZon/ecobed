<?php
/*
  Plugin Name: WP Business Directory
  Plugin URI: http://codecanyon.net/item/wp-business-directory/2203258
  Description: WP Business Directory is a WordPress Plugin that allows you to add business directory features to your WP based web site. It is easy to use and has a lot of features. With the help of Premium Membership and Google Adsense with the Google Maps features, you can also earn money.
  Version: 1.6.5
  Author: Evrim Çabuk
  Author URI: http://codecanyon.net/user/ecabuk
 */

if (!defined('WPECBD_PLUGIN_BASENAME'))
    define('WPECBD_PLUGIN_BASENAME', plugin_basename(__FILE__));

if (!defined('WPECBD_PLUGIN_NAME'))
    define('WPECBD_PLUGIN_NAME', trim(dirname(WPECBD_PLUGIN_BASENAME), '/'));

if (!defined('WPECBD_PLUGIN_DIR'))
    define('WPECBD_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . WPECBD_PLUGIN_NAME . '/');

if (!defined('WPECBD_PLUGIN_URL'))
    define('WPECBD_PLUGIN_URL', WP_PLUGIN_URL . '/' . WPECBD_PLUGIN_NAME . '/');

if (!class_exists('wp_business_directory')) {

    class wp_business_directory {

        /**
         * @var array The unserialized array with the stored options
         */
        var $_options = array();

        /**
         * @var string Current installed version of the plugin. 
         */
        var $version = '1.6.5';

        public function __construct() {
            //Load options
            $this->LoadOptions();

            //Register TextDomain
            load_plugin_textdomain('wpecbd', FALSE, dirname(plugin_basename(__FILE__)) . '/languages/');

            //Run Init
            add_action('init', array(&$this, 'init_functions'));

            //Run Admin Init
            add_action('admin_init', array(&$this, 'admin_init_functions'));

            //Prepare plugin when activate
            register_activation_hook(__FILE__, array(&$this, 'activation'));

            //Fix Sql
            add_filter('posts_clauses', array(&$this, 'fix_sql_requests'));

            //Cleanup when post deleted
            add_action('after_delete_post', array(&$this, 'cleanup_after_delete'));

            //Register admin menus
            add_action('admin_menu', array(&$this, 'register_admin_menus'));
        }

        /**
         * Runs when plugin activate
         * @global object $wpdb 
         */
        function activation() {
            global $wpdb;

            require_once (WPECBD_PLUGIN_DIR . 'activation.php');
        }

        /**
         * Sets up the default options
         */
        function InitOptions() {
            $this->_options = array();

            /* Free vs Premium */
            $this->_options['f_logo'] = 1;
            $this->_options['p_logo'] = 1;
            $this->_options['f_images'] = 1;
            $this->_options['p_images'] = 1;
            $this->_options['f_map'] = 1;
            $this->_options['p_map'] = 1;
            $this->_options['f_address'] = 1;
            $this->_options['p_address'] = 1;
            $this->_options['f_city'] = 1;
            $this->_options['p_city'] = 1;
            $this->_options['f_state'] = 1;
            $this->_options['p_state'] = 1;
            $this->_options['f_zip'] = 1;
            $this->_options['p_zip'] = 1;
            $this->_options['f_country'] = 1;
            $this->_options['p_country'] = 1;
            $this->_options['f_phone'] = 1;
            $this->_options['p_phone'] = 1;
            $this->_options['f_fax'] = 1;
            $this->_options['p_fax'] = 1;
            $this->_options['f_website'] = 1;
            $this->_options['p_website'] = 1;
            $this->_options['f_email'] = 1;
            $this->_options['p_email'] = 1;
            $this->_options['f_longd'] = 1;
            $this->_options['p_longd'] = 1;
            $this->_options['f_shortd'] = 1;
            $this->_options['p_shortd'] = 1;
            $this->_options['f_twitter'] = 1;
            $this->_options['p_twitter'] = 1;
            $this->_options['f_facebook'] = 1;
            $this->_options['p_facebook'] = 1;
            $this->_options['f_googleplus'] = 1;
            $this->_options['p_googleplus'] = 1;
            $this->_options['f_linkedin'] = 1;
            $this->_options['p_linkedin'] = 1;
            $this->_options['f_rss'] = 1;
            $this->_options['p_rss'] = 1;
            $this->_options['f_skype'] = 1;
            $this->_options['p_skype'] = 1;

            /* Requirements */
            $this->_options['r_map'] = 1;
            $this->_options['r_address'] = 1;
            $this->_options['r_city'] = 1;
            $this->_options['r_state'] = 1;
            $this->_options['r_country'] = 1;
            $this->_options['r_zip'] = 0;
            $this->_options['r_shortd'] = 0;
            $this->_options['r_phone'] = 1;
            $this->_options['r_fax'] = 0;
            $this->_options['r_website'] = 1;
            $this->_options['r_email'] = 1;
            $this->_options['r_twitter'] = 0;
            $this->_options['r_facebook'] = 0;
            $this->_options['r_googleplus'] = 0;
            $this->_options['r_linkedin'] = 0;
            $this->_options['r_rss'] = 0;
            $this->_options['r_skype'] = 0;

            /* Limits */
            $this->_options['shortd_max'] = 300;
            $this->_options['longd_max'] = 4000;
            $this->_options['shortd_min'] = 100;
            $this->_options['longd_min'] = 200;
            $this->_options['max_cat'] = 5;
            $this->_options['max_tag'] = 10;
            $this->_options['f_maximg'] = 2;
            $this->_options['p_maximg'] = 5;

            /* Map Options */
            $this->_options['zoom_edit'] = 15;
            $this->_options['zoom_front'] = 11;
            $this->_options['def_lat'] = '41.006112031442825';
            $this->_options['def_lng'] = '28.976917084655724';
            $this->_options['maptype_edit'] = 'ROADMAP';
            $this->_options['maptype_front'] = 'ROADMAP';
            $this->_options['map_api_key'] = '';
            $this->_options['ad_pub_id'] = '';
            $this->_options['fe_p_ad_type'] = 0;

            /* Settings */
            $this->_options['dir_slug'] = 'dir';
            $this->_options['cat_slug'] = 'business-category';
            $this->_options['tag_slug'] = 'business-tag';
            $this->_options['dir_slug_with_front'] = 1;
            $this->_options['cat_slug_with_front'] = 1;
            $this->_options['tag_slug_with_front'] = 1;


            $this->_options['sql_fix'] = 1;
            $this->_options['qr'] = 1;
            $this->_options['qr_type'] = 'vcard-embed';
            $this->_options['comments'] = 0;
            $this->_options['premium'] = 1;
            $this->_options['currency'] = 'USD';
            $this->_options['category'] = 1;
            $this->_options['tag'] = 1;
            $this->_options['social'] = 1;
            $this->_options['auto_locality'] = 1;
            $this->_options['premium_first'] = 1;
            $this->_options['alphabetical_order'] = 0;
            $this->_options['purchase_code'] = '';

            /* Image Sizes */
            $this->_options['img_logo_w'] = '200';
            $this->_options['img_logo_h'] = '150';
            $this->_options['img_logo_c'] = 0;
            $this->_options['img_logo_loop_w'] = '200';
            $this->_options['img_logo_loop_h'] = 0;
            $this->_options['img_logo_loop_c'] = 0;
            $this->_options['img_thumb_w'] = '64';
            $this->_options['img_thumb_h'] = '64';
            $this->_options['img_thumb_c'] = 1;
            $this->_options['img_w'] = '1024';
            $this->_options['img_h'] = '1024';
            $this->_options['img_c'] = 0;
            $this->_options['map_img_w'] = 200;
            $this->_options['map_img_h'] = 200;
            $this->_options['qr_w'] = 200;
            $this->_options['qr_h'] = 200;

            /* Masked Inputs */
            $this->_options['mask_actv'] = 1;
            $this->_options['mask_tel'] = '(999) 999-9999';
            $this->_options['mask_fax'] = '(999) 999-9999';

            /* Captcha */
            $this->_options['captcha_public_key'] = '';
            $this->_options['captcha_private_key'] = '';
            $this->_options['captcha'] = 0;

            /* PayPal Options */
            $this->_options['paypal'] = array(
                'user' => '',
                'pass' => '',
                'sign' => '',
                'envi' => 'sandbox',
                'loca' => 'US',
                'img' => '',
            );

            /* Premium Plans */
            $this->_options['prem_plans'] = array(
                '1 Month' => array('price' => '2.00', 'str' => '+1 month'),
                '3 Months' => array('price' => '4.00', 'str' => '+3 months'),
                '6 Months' => array('price' => '7.00', 'str' => '+6 months'),
                '1 Year' => array('price' => '12.00', 'str' => '+1 year'),
                '2 Years' => array('price' => '19.99', 'str' => '+2 years'),
                '3 Years' => array('price' => '29.99', 'str' => '+3 years')
            );

            /* Seo Options */
            $this->_options['seo_desc'] = 0;
            $this->_options['seo_keywords'] = 1;
            $this->_options['seo_noindex'] = 0;

            /* Version & Update */
            $this->_options['current_version'] = $this->version;
        }

        /**
         * Loads the options from the database
         * @param array $save Save options
         */
        function LoadOptions($save = FALSE) {

            $this->InitOptions();

            if ($save) {
                foreach ($this->_options as $ke => $va) {
                    $this->_options[$ke] = (isset($save[$ke])) ? $save[$ke] : 0;
                }
                update_option("wpecbd_options", $this->_options);
                return;
            }

            $storedoptions = get_option("wpecbd_options");

            if ($storedoptions && is_array($storedoptions)) {
                foreach ($storedoptions AS $k => $v) {
                    $this->_options[$k] = $v;
                }
            } else {
                update_option("wpecbd_options", $this->_options);
            }
        }

        /**
         * Contains all init functions
         */
        function init_functions() {

            /*
             * REGISTER wpecbd POST TYPE
             */
            $labels = array(
                'name' => __('Companies', 'wpecbd'),
                'singular_name' => __('Company', 'wpecbd'),
                'add_new' => __('Add New', 'wpecbd'),
                'add_new_item' => __('Add New Company', 'wpecbd'),
                'edit_item' => __('Edit Company', 'wpecbd'),
                'new_item' => __('New Company', 'wpecbd'),
                'all_items' => __('All Companies', 'wpecbd'),
                'view_item' => __('View Company', 'wpecbd'),
                'search_items' => __('Search Companies', 'wpecbd'),
                'not_found' => __('No companies found', 'wpecbd'),
                'not_found_in_trash' => __('No companies found in Trash', 'wpecbd'),
                'menu_name' => __('WP Business Directory', 'wpecbd')
            );
            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => $this->_options['dir_slug'], 'with_front' => (bool) $this->_options['dir_slug_with_front']),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array('title', 'editor', 'author', 'excerpt')
            );
            //Check comment active?
            if ($this->_options['comments'])
                $args['supports'][] = 'comments';

            register_post_type('wpecbd', $args);

            /*
             * REGISTER COMPANY CATEGORIES TAXONOMY
             */
            //Check for category activated
            if ($this->_options['category']):
                $cat_tax_labels = array(
                    'name' => __('Categories', 'wpecbd'),
                    'singular_name' => __('Category', 'wpecbd'),
                    'search_items' => __('Search Categories', 'wpecbd'),
                    'all_items' => __('All Categories', 'wpecbd'),
                    'parent_item' => __('Parent Category', 'wpecbd'),
                    'parent_item_colon' => __('Parent Category:', 'wpecbd'),
                    'edit_item' => __('Edit Category', 'wpecbd'),
                    'update_item' => __('Update Category', 'wpecbd'),
                    'add_new_item' => __('Add New Category', 'wpecbd'),
                    'new_item_name' => __('New Category Name', 'wpecbd'),
                    'menu_name' => __('Categories', 'wpecbd'),
                );

                $cat_tax_args = array(
                    'hierarchical' => true,
                    'labels' => $cat_tax_labels,
                    'show_ui' => true,
                    'query_var' => true,
                    'rewrite' => array('slug' => $this->_options['cat_slug'], 'with_front' => (bool) $this->_options['cat_slug_with_front']));

                register_taxonomy('bd_categories', 'wpecbd', $cat_tax_args);
            endif;

            /*
             * REGISTER COMPANY TAGS TAXONOMY
             */
            //Check for tags activated
            if ($this->_options['tag']):
                $tag_tax_labels = array(
                    'name' => __('Tags', 'wpecbd'),
                    'singular_name' => __('Tag', 'wpecbd'),
                    'search_items' => __('Search Tags', 'wpecbd'),
                    'popular_items' => __('Popular Tags', 'wpecbd'),
                    'all_items' => __('All Tags', 'wpecbd'),
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => __('Edit Tag', 'wpecbd'),
                    'update_item' => __('Update Tag', 'wpecbd'),
                    'add_new_item' => __('Add New Tag', 'wpecbd'),
                    'new_item_name' => __('New Tag Name', 'wpecbd'),
                    'separate_items_with_commas' => __('Separate tags with commas', 'wpecbd'),
                    'add_or_remove_items' => __('Add or remove tags', 'wpecbd'),
                    'choose_from_most_used' => __('Choose from the most used tags', 'wpecbd'),
                    'menu_name' => __('Tags', 'wpecbd'),
                );

                $tag_tax_args = array(
                    'hierarchical' => false,
                    'labels' => $tag_tax_labels,
                    'show_ui' => true,
                    'update_count_callback' => '_update_post_term_count',
                    'query_var' => true,
                    'rewrite' => array('slug' => $this->_options['tag_slug'], 'with_front' => (bool) $this->_options['tag_slug_with_front']));

                register_taxonomy('bd_tags', 'wpecbd', $tag_tax_args);
            endif;

            /*
             * Check Current Template Supports Thumbnails
             */
            if (!current_theme_supports('post-thumbnails') AND ($this->_options['images'] OR $this->_options['logo'])) {
                add_theme_support('post-thumbnails');
            }

            /*
             * Register Image And Thumbnail Size
             */
            if ($this->check_option('logo', TRUE)) {
                add_image_size('bd_logo', $this->_options['img_logo_w'], $this->_options['img_logo_h'], (bool) $this->_options['img_logo_c']);
                add_image_size('bd_logo_inloop', $this->_options['img_logo_loop_w'], $this->_options['img_logo_loop_h'], (bool) $this->_options['img_logo_loop_c']);
            }
            if ($this->check_option('images', TRUE)) {
                add_image_size('bd_thumb', $this->_options['img_thumb_w'], $this->_options['img_thumb_h'], (bool) $this->_options['img_thumb_c']);
                add_image_size('bd_img', $this->_options['img_w'], $this->_options['img_h'], (bool) $this->_options['img_c']);
            }

            /*
             * Register Image Upload JS Script
             */
            wp_register_script('wpecbd_image_uploader', WPECBD_PLUGIN_URL . 'js/fileuploader.js');

            /*
             * Register jquery-addresspicker
             */
            wp_register_script('jquery-addresspicker', WPECBD_PLUGIN_URL . 'js/jquery.ui.addresspicker.js', array('jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-autocomplete', 'google-maps'));

            /*
             * Register Google Maps Api JS
             */
            wp_register_script('google-maps', $this->http_protocol() . 'maps.google.com/maps/api/js?sensor=true&language=' . apply_filters('bd_g_maps_lang', 'en') . ((!empty($this->_options['map_api_key'])) ? '&key=' . $this->_options['map_api_key'] : ''));

            /*
             * Register FancyBox JS
             */
            wp_register_script('fancybox', WPECBD_PLUGIN_URL . 'js/jquery.fancybox-1.3.4.pack.js', array('jquery'));

            /*
             * Register Wp Business Directory FrontEnd Scripts
             */
            wp_register_script('wpecbd-frontend', WPECBD_PLUGIN_URL . 'js/wpecbd-front.js', array('jquery', 'fancybox'));

            /*
             * Register Wp Business Directory Custom Map Script
             */
            wp_register_script('wpecbd-custom_map', WPECBD_PLUGIN_URL . 'js/wpecbd-custom_map.js', array('jquery', 'google-maps'));

            /*
             * Register jQuery columnizer Plugin For Category Shortcode 
             */
            wp_register_script('jquery.columnizer', WPECBD_PLUGIN_URL . 'js/jquery.columnizer.js', array('jquery'));

            /*
             * Register FancyBox CSS
             */
            wp_register_style('fancybox', WPECBD_PLUGIN_URL . 'css/jquery.fancybox-1.3.4.css');

            /*
             * Register FrontEnd Css
             */
            wp_register_style('wpecbd-frontend', WPECBD_PLUGIN_URL . 'css/frontend.css');

            /*
             * Register JQuery UI Smoothness Theme
             */
            wp_register_style('jquery-ui-smoothness', $this->http_protocol() . 'ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css');

            /*
             * Print FrontEnd Styles
             */
            add_action('wp_print_styles', array(&$this, 'frontend_styles'));

            /*
             * Print FrontEnd Scripts
             */
            add_action('wp_print_scripts', array(&$this, 'frontend_scripts'));

            /*
             * Custom ShortCode Handler
             */
            add_shortcode('wpbd_cat', array(&$this, 'shortcode_handler_cat'));

            /*
             * User Area ShortCode Handler
             */
            add_shortcode('wpbd_user', array(&$this, 'shortcode_handler_user'));

            /*
             * Custom Company Shows on Map ShortCode
             */
            add_shortcode('wpbd_map', array(&$this, 'shortcode_handler_custom_map'));

            /*
             * Content Filter to Display Fields On The Page
             */
            add_filter('the_content', array(&$this, 'content_filter'));
            add_filter('the_excerpt', array(&$this, 'content_filter'));

            /*
             * Content Filter to Display Fields On Feeds
             */
            if (get_option('rss_use_excerpt')) {
                add_filter('the_excerpt_rss', array(&$this, 'feed_content_filter'));
            } else {
                add_filter('the_content_feed', array(&$this, 'feed_content_filter'));
            }

            /*
             * Template Category Fix
             */
            add_filter('the_category', array(&$this, 'template_category_fix'));

            /*
             * REWRITE
             */
            add_filter('rewrite_rules_array', array(&$this, 'create_rewrite_rules'));
            add_filter('query_vars', array(&$this, 'add_query_vars'));
            add_action('template_redirect', array(&$this, 'template_redirect_intercept'));

            /*
             * SEO
             */
            add_action('wp_head', array(&$this, 'seo'));

            /*
             * Auto Update
             */
            if (!empty($this->_options['purchase_code']) AND strlen($this->_options['purchase_code']) > 20) {
                require_once (WPECBD_PLUGIN_DIR . 'auto_update.php');
                new wp_business_directory_auto_update($this->version, plugin_basename(__FILE__), trim($this->_options['purchase_code']));
            }
        }

        /**
         * Adds JOIN,ORDER BY etc. to sql request
         * @global object $wp_query Wordpress Query Object
         * @global object $wpdb WPDB Object
         * @param array $sql_parts
         * @return array Return fixed sql parts
         * @todo Need some improvements?
         */
        function fix_sql_requests($sql_parts) {
            global $wp_query, $wpdb;
            if (!$this->_options['sql_fix'] AND !isset($_POST['bd_w_search'])) {
                return $sql_parts;
            }


            $orderby_buff = array();

            if ((!empty($wp_query->query_vars['post_type']) AND $wp_query->query_vars['post_type'] == 'wpecbd') OR ((!empty($wp_query->query_vars['bd_categories'])) OR (!empty($wp_query->query_vars['bd_tags'])))) {

                $sql_parts['join'].=' LEFT JOIN ' . $wpdb->prefix . 'wpecbd ON ' . $wpdb->posts . '.ID = ' . $wpdb->prefix . 'wpecbd.post_id ';
                $sql_parts['fields'].=', ' . $wpdb->prefix . 'wpecbd.* ';

                if (!$wp_query->is_singular) {
                    if ($this->_options['premium_first']) {
                        $orderby_buff[] = " CASE WHEN DATE( " . $wpdb->prefix . "wpecbd.premium_end ) > NOW( ) THEN 1 ELSE 0 END DESC ";
                    }

                    if ($this->_options['alphabetical_order']) {
                        $orderby_buff[] = " {$wpdb->posts}.post_title ASC ";
                    }
                }
            }

            $sql_parts['orderby'] = implode(' ,', $orderby_buff) . ((!empty($orderby_buff)) ? ', ' : '') . $sql_parts['orderby'];

            return $sql_parts;
        }

        /**
         * Contains admin init functions
         */
        function admin_init_functions() {

            //Update
            if (version_compare($this->version, $this->_options['current_version'], '>')) {
                require_once (WPECBD_PLUGIN_DIR . 'inc/' . 'local_update.php');
                wpecbd_update();
            }

            //Admin Scripts
            add_action('admin_print_scripts', array(&$this, 'admin_scripts'));
            //Admin Styles
            add_action('admin_print_styles', array(&$this, 'admin_styles'));
            //Register Custom Meta Boxes
            add_action('add_meta_boxes', array(&$this, 'register_meta_boxes'));
            //Save Metabox Entries
            add_action('save_post', array(&$this, 'save_meta_box_entries'));
            //Ajax Admin Images Upload Handler
            add_action('wp_ajax_wpecbd_admin_image', array(&$this, 'ajax_admin_image_handler'));
            //When post approved by admin add a post meta
            add_action('publish_wpecbd', array(&$this, 'make_post_admin_approved'));
            //When post status changed by admin make this post rejected
            add_action('pending_wpecbd', array(&$this, 'make_post_admin_rejected'));
            add_action('trash_wpecbd', array(&$this, 'make_post_admin_rejected'));
            add_action('draft_wpecbd', array(&$this, 'make_post_admin_rejected'));
            //Right Now Admin Dashboard Widget
            add_action('right_now_content_table_end', array(&$this, 'right_now_admin_widget'));

            //CSV Importer
            if ((isset($_GET['page']) && $_GET['page'] == 'wpecbd_csv_importer') OR (isset($_GET['action']) && $_GET['action'] == 'wpecbd_csv_uploader')) {
                require_once WPECBD_PLUGIN_DIR . 'inc/tools/csv_import.php';
                add_action('wp_ajax_wpecbd_csv_uploader', array('wpecbd_csv_importer', 'handle_upload'));
            }

            //CSV Exporter
            if (isset($_GET['page']) && $_GET['page'] == 'wpecbd_csv_exporter') {
                require_once WPECBD_PLUGIN_DIR . 'inc/tools/csv_export.php';
            }
        }

        /**
         * Enqueue Admin Scripts
         * @global object $post 
         */
        function admin_scripts() {
            global $post;
            if (isset($post) AND $post->post_type == 'wpecbd') {
                //Image Uploader
                if ($this->check_option('logo', TRUE) OR $this->check_option('images', TRUE)) {
                    wp_enqueue_script('wpecbd_image_uploader');
                }

                //Location Picker
                if ($this->check_option('map', TRUE)) {
                    wp_enqueue_script('jquery-addresspicker');
                }

                //DatePicker
                if ($this->_options['premium']) {
                    wp_enqueue_script('jquery-ui-datepicker');
                }
            } elseif (isset($_GET['post_type']) AND $_GET['post_type'] == 'wpecbd') {
                wp_enqueue_script('jquery-addresspicker');
            }
        }

        /**
         * Enqueue Admin Styles
         * @global object $post 
         */
        function admin_styles() {
            global $post;
            if (isset($post) AND $post->post_type == 'wpecbd') {
                //JQuery UI Smoothness Theme
                wp_enqueue_style('jquery-ui-smoothness');
                //Wordpress Business Directory Admin Style
                wp_enqueue_style('wpecbd-admin', WPECBD_PLUGIN_URL . 'css/admin.css');
            } elseif (isset($_GET['post_type']) AND $_GET['post_type'] == 'wpecbd') {
                wp_enqueue_style('jquery-ui-smoothness');
            }
        }

        /**
         * Enqueue Frontend Styles
         * @global object $post 
         */
        function frontend_styles() {
            global $post;
            if ((isset($post) AND $post->post_type == 'wpecbd') OR (is_page() AND strpos($post->post_content, 'wpbd_user') !== FALSE)) {
                wp_enqueue_style('wpecbd-frontend');
                wp_enqueue_style('fancybox');
            }

            /* Custom Map Shortcode */
            if (isset($post) AND strstr($post->post_content, '[wpbd_map')) {
                wp_enqueue_style('wpecbd-custom_map', WPECBD_PLUGIN_URL . 'css/custom_map.css');
            }

            /*
             * WIDGETS
             */

            /* Company Search */
            if (is_active_widget(NULL, NULL, 'wpbd_com_search') AND !is_admin()) {
                $multiselect_theme = apply_filters('bd_search_multiselect_theme', 'smoothness');
                wp_enqueue_style('jquery-ui-' . $multiselect_theme, $this->http_protocol() . 'ajax.googleapis.com/ajax/libs/jqueryui/1/themes/' . $multiselect_theme . '/jquery-ui.css');
                wp_enqueue_style('wpecbd-frontend');
                wp_enqueue_style('fancybox');
            }
            /* Premium Companies */
            if (is_active_widget(NULL, NULL, 'wpbd_com_premium_companies') AND !is_admin()) {
                wp_enqueue_style('wpecbd-frontend');
            }
        }

        /**
         * Enqueue Frontend Scripts
         * @global object $post 
         */
        function frontend_scripts() {
            global $post;
            if ((isset($post) AND $post->post_type == 'wpecbd') OR (is_page() AND strpos($post->post_content, 'wpbd_user'))) {
                wp_enqueue_script('wpecbd-frontend');
            }

            /* Category Shortcode */
            if (isset($post) AND strstr($post->post_content, '[wpbd_cat')) {
                wp_enqueue_script('jquery.columnizer');
            }

            /* Custom Map Shortcode */
            if (isset($post) AND strstr($post->post_content, '[wpbd_map')) {
                wp_enqueue_script('wpecbd-custom_map');
            }

            /*
             * WIDGETS
             */

            /* Company Search */
            if (is_active_widget(NULL, NULL, 'wpbd_com_search') AND !is_admin()) {
                wp_enqueue_script('jquery.multiselect', WPECBD_PLUGIN_URL . 'js/jquery.multiselect.js', array('jquery', 'jquery-ui-core', 'jquery-ui-mouse', 'jquery-ui-widget'));
                wp_enqueue_script('wpecbd-frontend');
                wp_enqueue_script('jquery-addresspicker');
            }
        }

        /**
         * Registers metaboxes for wpecbd post type
         * @global object $post
         * @global object $wpdb WP DB object
         * @global boolean $is_premium
         * @return null If user can't edit companies 
         */
        function register_meta_boxes() {
            global $post, $wpdb, $is_premium;

            if (!current_user_can('edit_post', $post->ID))
                return;

            if (isset($post->post_type) AND $post->post_type == "wpecbd"):

                //Check Post Exist
                $com = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "wpecbd WHERE post_id=" . $post->ID, ARRAY_A);
                if (empty($com)) {
                    $com = NULL;
                }

                $is_premium = $this->is_premium();

                //Check Logo Active?
                if ($this->check_option('logo', TRUE))
                    add_meta_box('wpecbd_logo', _x('Logo', 'metabox_company_logo', 'wpecbd'), array(&$this, 'metabox_logo'), 'wpecbd', 'side', 'high', NULL);

                //Check Images Active?
                if ($this->check_option('images', TRUE))
                    add_meta_box('wpecbd_images', _x('Images', 'metabox_company_images', 'wpecbd'), array(&$this, 'metabox_images'), 'wpecbd', 'advanced', 'high', NULL);

                //Check Social Active?
                if ($this->_options['social'])
                    add_meta_box('wpecbd_social', _x('Social', 'metabox_company_social', 'wpecbd'), array(&$this, 'metabox_social'), 'wpecbd', 'side', 'low', $com);

                //Check Premium Active?
                if ($this->_options['premium'])
                    add_meta_box('wpecbd_premiumend', _x('Premium End', 'metabox_company_premiumend', 'wpecbd'), array(&$this, 'metabox_premiumend'), 'wpecbd', 'side', 'low', $com);

                //Add Information metabox
                add_meta_box('wpecbd_info', _x('Information', 'metabox_company_information', 'wpecbd'), array(&$this, 'metabox_info'), 'wpecbd', 'normal', 'high', $com);
            endif;
        }

        /**
         * METABOX-LOGO
         * @param object $post 
         */
        function metabox_logo($post) {
            ?>
            <div id="wpecbd_logo_uploader">
                <noscript>
                <p><?php _e('Please enable JavaScript to use file uploader.', 'wpecbd'); ?></p>
                </noscript>
            </div>
            <div id="bd_ajax_logo_upload"><strong>Uploading file... Please Wait...</strong></div>
            <div id="company_logo">
                <?php
                if (has_post_thumbnail()) {
                    $post_thumbnail_id = get_post_thumbnail_id($post->ID);
                    //Sometimes partially uploaded images causes error,check for that
                    if (!isset($post_thumbnail_id->errors)) {
                        $thumbnail = wp_get_attachment_image_src($post_thumbnail_id, 'bd_logo');
                        echo '<img class="bd_img" thumbnail_id="' . $post_thumbnail_id . '" src="' . $thumbnail[0] . '" width="' . $thumbnail[1] . '" height="' . $thumbnail[2] . '" />';
                        echo '<span id="del_com_logo"></span>';
                        echo '<script>jQuery(document).ready(function() {jQuery("#wpecbd_logo_uploader").hide();});</script>';
                    }
                }
                ?>
            </div>
            <script>
                var wpecbd_logo_uploader = new qq.FileUploader({
                    element: document.getElementById('wpecbd_logo_uploader'),
                    action: ajaxurl+'?action=wpecbd_admin_image',
                    params: {
                        post_id: '<?php echo $post->ID ?>',
                        nonce_admin: '<?php echo wp_create_nonce('wpecbd_admin_images'); ?>',
                        logo:'true'
                    },
                    sizeLimit: <?php echo $this->wpecbd_max_upload_size(); ?>,
                    messages: {
                        typeError: "<?php _e('{file} has invalid extension. Only {extensions} are allowed.', 'wpecbd'); ?>",
                        sizeError: "<?php _e('{file} is too large, maximum file size is {sizeLimit}.', 'wpecbd'); ?>",
                        minSizeError: "<?php _e('{file} is too small, minimum file size is {minSizeLimit}.', 'wpecbd'); ?>",
                        emptyError: "<?php _e('{file} is empty, please select files again without it.', 'wpecbd'); ?>",
                        onLeave: "<?php _e('The files are being uploaded, if you leave now the upload will be cancelled.', 'wpecbd'); ?>"
                    },
                    onSubmit: function(id, fileName){
                        jQuery("#wpecbd_logo_uploader").hide();
                        jQuery("#bd_ajax_logo_upload").show();
                    },
                    onComplete: function(id, fileName, r){
                        if(r.success == 'true'){
                            jQuery("#company_logo").append(<?php echo "'<img class=\"bd_img\" src=\"'+r.url+'\" /><span id=\"del_com_logo\"></span>'"; ?>);
                        }
                        jQuery("#bd_ajax_logo_upload").hide();
                    },
                    onCancel: function(id, fileName){
                        jQuery("#wpecbd_logo_uploader").show();
                        jQuery("#bd_ajax_logo_upload").hide();
                    }
                });

                jQuery("#del_com_logo").live("click", function(){
                    jQuery.ajax({
                        type: "GET",
                        dataType: "json",
                        url: ajaxurl+'?action=wpecbd_admin_image',
                        data: <?php echo "'bd_action=delete_logo&post_id=" . $post->ID . "&nonce_admin=" . wp_create_nonce('wpecbd_admin_images') . "'"; ?>
                    }).done(function( msg ) {
                        if(msg.success == 'true'){
                            jQuery("#wpecbd_logo_uploader").show();
                            jQuery("#company_logo").empty();
                        }
                    });
                });
            </script>
            <?php
        }

        /**
         * METABOX-IMAGES
         * @param object $post 
         */
        function metabox_images($post) {
            ?>
            <div id="wpecbd_image_uploader">
                <noscript>
                <p><?php _e('Please enable JavaScript to use file uploader.', 'wpecbd'); ?></p>
                </noscript>
            </div>
            <div id="bd_ajax_image_upload"><strong>Uploading file... Please Wait...</strong></div>
            <ul id="company_images">
                <?php
                $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID);
                $attachments = get_posts($args);

                //Check for logo
                $post_thumbnail_id = ((has_post_thumbnail()) ? get_post_thumbnail_id($post->ID) : 0);

                if ($attachments) {
                    foreach ($attachments as $attachment) {
                        //Remove logo
                        if ($attachment->ID == $post_thumbnail_id)
                            continue;
                        $src = wp_get_attachment_image_src($attachment->ID);
                        echo '<li id="bd_' . $attachment->ID . '" class="com_img"><img class="bd_img" src="' . $src[0] . '" /><span class="del_com_img"></span></li>' . "\n";
                    }
                }
                ?>
            </ul>
            <script>
                var wpecbd_image_uploader = new qq.FileUploader({
                    element: document.getElementById('wpecbd_image_uploader'),
                    action: ajaxurl+'?action=wpecbd_admin_image',
                    params: {
                        post_id: '<?php echo $post->ID ?>',
                        nonce_admin: '<?php echo wp_create_nonce('wpecbd_admin_images'); ?>'
                    },
                    sizeLimit: <?php echo $this->wpecbd_max_upload_size(); ?>,
                    messages: {
                        typeError: "<?php _e('{file} has invalid extension. Only {extensions} are allowed.', 'wpecbd'); ?>",
                        sizeError: "<?php _e('{file} is too large, maximum file size is {sizeLimit}.', 'wpecbd'); ?>",
                        minSizeError: "<?php _e('{file} is too small, minimum file size is {minSizeLimit}.', 'wpecbd'); ?>",
                        emptyError: "<?php _e('{file} is empty, please select files again without it.', 'wpecbd'); ?>",
                        onLeave: "<?php _e('The files are being uploaded, if you leave now the upload will be cancelled.', 'wpecbd'); ?>"
                    },
                    onSubmit: function(id, fileName){
                        jQuery("#wpecbd_image_uploader").hide();
                        jQuery("#bd_ajax_image_upload").show();
                    },
                    onComplete: function(id, fileName, r){
                        if(r.success == 'true'){
                            jQuery("#company_images").append(<?php echo "'<li id=\"bd_'+r.attachment_id+'\"><img class=\"bd_img\" src=\"'+r.url+'\" /><span class=\"del_com_img\"></span></li>'"; ?>);
                        };
                        if(r.remained > 0){
                            jQuery("#wpecbd_image_uploader").show();
                        }
                        jQuery("#bd_ajax_image_upload").hide();
                    },
                    onCancel: function(id, fileName){
                        jQuery("#wpecbd_image_uploader").show();
                        jQuery("#bd_ajax_image_upload").hide();
                    }
                });
                jQuery('.del_com_img').live('click',function(){
                    var attachment_id = jQuery(this).parent().attr("id");
                    jQuery.ajax({
                        type: "GET",
                        dataType: "json",
                        url: ajaxurl+'?action=wpecbd_admin_image',
                        data: {
                            bd_action:'delete_img',
                            id:attachment_id,
                            post_id:<?php echo $post->ID; ?>,
                            nonce_admin:'<?php echo wp_create_nonce('wpecbd_admin_images'); ?>'
                        }
                    }).done(function( msg ) {
                        if(msg.success == 'true'){
                            jQuery("#"+attachment_id).fadeOut('slow');
                            jQuery("#wpecbd_image_uploader").show();
                        }
                    });
                });
            <?php
            if ($this->_can_upload_more_image($post->ID) <= 0)
                echo 'jQuery("#wpecbd_image_uploader").hide();' . "\n";
            ?>
            </script>
            <br class="clear" />
            <?php
        }

        /**
         * METABOX-SOCIAL
         * @param object $post
         * @param array $args 
         */
        function metabox_social($post, $args) {
            if (!empty($args['args'])) {
                extract($args['args']);
            } else {
                $twitter = $facebook = $googleplus = $linkedin = $rss = $skype = '';
            }
            ?>
            <table class="widefat">
                <?php if ($this->check_option('twitter')): ?>
                    <tr>
                        <td>
                            <img class="socials" src="<?php echo WPECBD_PLUGIN_URL; ?>img/social-icons/twitter.png"></img><strong class="socials_text">Twitter</strong>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td>
                            <input name="bd[twitter]" value="<?php echo $twitter; ?>" type="text" class="all-options" />
                        </td>
                    </tr>
                    <?php
                endif;
                if ($this->check_option('facebook')):
                    ?>
                    <tr>
                        <td>
                            <img class="socials" src="<?php echo WPECBD_PLUGIN_URL; ?>img/social-icons/facebook.png"></img><strong class="socials_text">Facebook</strong>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td>
                            <input name="bd[facebook]" value="<?php echo $facebook; ?>" type="text" class="all-options" />
                        </td>
                    </tr>
                    <?php
                endif;
                if ($this->check_option('linkedin')):
                    ?>
                    <tr>
                        <td>
                            <img class="socials" src="<?php echo WPECBD_PLUGIN_URL; ?>img/social-icons/linkedin.png"></img><strong class="socials_text">Linkedin</strong>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td>
                            <input name="bd[linkedin]" value="<?php echo $linkedin; ?>" type="text" class="all-options" />
                        </td>
                    </tr>
                    <?php
                endif;
                if ($this->check_option('rss')):
                    ?>
                    <tr>
                        <td>
                            <img class="socials" src="<?php echo WPECBD_PLUGIN_URL; ?>img/social-icons/rss.png"></img><strong class="socials_text">Rss</strong>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td>
                            <input name="bd[rss]" value="<?php echo $rss; ?>" type="text" class="all-options" />
                        </td>
                    </tr>
                    <?php
                endif;
                if ($this->check_option('googleplus')):
                    ?>
                    <tr>
                        <td>
                            <img class="socials" src="<?php echo WPECBD_PLUGIN_URL; ?>img/social-icons/google+color.png"></img><strong class="socials_text">GooglePlus</strong>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td>
                            <input name="bd[googleplus]" value="<?php echo $googleplus; ?>" type="text" class="all-options" />
                        </td>
                    </tr>
                    <?php
                endif;
                if ($this->check_option('skype')):
                    ?>
                    <tr>
                        <td>
                            <img class="socials" src="<?php echo WPECBD_PLUGIN_URL; ?>img/social-icons/skype.png"></img><strong class="socials_text">Skype</strong>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td>
                            <input name="bd[skype]" value="<?php echo $skype; ?>" type="text" class="all-options" />
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
            <?php
        }

        /**
         * METABOX-INFO
         * @param object $post
         * @param array $args
         */
        function metabox_info($post, $args) {
            if (!empty($args['args'])) {
                extract($args['args']);
            } else {
                $web_site = $email = $phone = $fax = $address = $city = $state = $country = $zip = '';
                $lat = $this->_options['def_lat'];
                $lng = $this->_options['def_lng'];
            }
            if ($this->check_option('map')):
                ?>
                <script>
                    jQuery(function() {
                        var addresspickerMap = jQuery( "#addresspicker_map" ).addresspicker({
                            elements: {
                                map:      "#map",
                                lat:      "#bd_lat",
                                lng:      "#bd_lng"
                            },
                            mapOptions: {
                                zoom: <?php echo $this->_options['zoom_edit']; ?>,
                                center: new google.maps.LatLng(<?php echo $lat . ', ' . $lng; ?>),
                                scrollwheel: false,
                                mapTypeId: google.maps.MapTypeId.<?php echo $this->_options['maptype_edit']; ?>
                            }
                        });
                        var gmarker = addresspickerMap.addresspicker( "marker");
                        gmarker.setVisible(true);
                        addresspickerMap.addresspicker( "updatePosition");
                    });
                </script>
            <?php endif; ?>
            <div class="wpecbd">
                <div id="edit_left_side" class="<?php ($this->check_option('map')) ? 'map_active' : 'map_deactive'; ?>">
                    <div class="inside">
                        <ul>
                            <?php
                            echo ($this->check_option('website')) ? '<li><label>' . __('Web Site', 'wpecbd') . ':</label><input name="bd[web_site]" value="' . esc_url($web_site) . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('email')) ? '<li><label>' . __('eMail', 'wpecbd') . ':</label><input name="bd[email]" value="' . $email . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('phone')) ? '<li><label>' . __('Telephone', 'wpecbd') . ':</label><input name="bd[phone]" value="' . esc_attr($phone) . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('fax')) ? '<li><label>' . __('Fax', 'wpecbd') . ':</label><input name="bd[fax]" value="' . esc_attr($fax) . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('address')) ? '<li><label>' . __('Address', 'wpecbd') . ':</label><textarea name="bd[address]" rows="2" class="all-options">' . esc_textarea(stripslashes($address)) . '</textarea></li>' : '';
                            echo ($this->check_option('city')) ? '<li><label>' . __('City', 'wpecbd') . ':</label><input name="bd[city]" value="' . esc_attr(stripslashes($city)) . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('state')) ? '<li><label>' . __('State', 'wpecbd') . ':</label><input name="bd[state]" value="' . esc_attr(stripslashes($state)) . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('country')) ? '<li><label>' . __('Country', 'wpecbd') . ':</label><input name="bd[country]" value="' . esc_attr(stripslashes($country)) . '" type="text" class="all-options" /></li>' : '';
                            echo ($this->check_option('zip')) ? '<li><label>' . __('Zip', 'wpecbd') . ':</label><input name="bd[zip]" value="' . esc_attr($zip) . '" type="text" class="all-options" /></li>' : '';
                            ?>
                        </ul>
                    </div>
                </div>
                <?php if ($this->check_option('map')): ?>
                    <div id="edit_right_side">
                        <div class="inside">
                            <input id="addresspicker_map" type="text" onclick="this.value='';" value="<?php _e('Search', 'wpecbd'); ?>" />
                            <div id="map"></div>
                            <div id="map_legend"><?php _e('You can drag and drop the marker to the correct location.', 'wpecbd'); ?></div>
                            <input id="bd_lat" name="bd[lat]" type="hidden" />
                            <input id="bd_lng" name="bd[lng]" type="hidden" />
                        </div>
                    </div>
                <?php endif; ?>
                <br class="clear" />
            </div>
            <?php
            //Nonce
            wp_nonce_field('edit_company', 'bd_edit_company');
        }

        /**
         * MATABOX-PREMIUM END
         * @param object $post
         * @param array $args
         */
        function metabox_premiumend($post, $args) {
            ?>
            <script>
                jQuery(function() {
                    jQuery( "#datepicker" ).datepicker({
                        altField: "#hidden_premiumend_date",
                        altFormat: "yy-mm-dd"
                    });
            <?php
            if (!empty($args['args'])) {
                echo "var premium_date_end='" . date('m/d/Y', strtotime($args['args']['premium_end'])) . "';";
            } else {
                echo "var premium_date_end='" . gmdate('m/d/Y') . "';";
            }
            ?>
                    jQuery( "#datepicker" ).datepicker( "setDate" , premium_date_end );
                });
            </script>
            <div id="datepicker"></div>
            <input name="bd[premium_end]" value="" id="hidden_premiumend_date" type="hidden"/>
            <?php
        }

        /**
         * Save Metabox Entries
         * @global object $wpdb
         * @param int $post_id
         * @return NULL 
         */
        function save_meta_box_entries($post_id) {
            global $wpdb;

            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return;

            //Check PostType
            if (isset($_POST['post_type']) AND $_POST['post_type'] != 'wpecbd')
                return;

            //Security Check
            if (isset($_POST['bd_edit_company']) AND !wp_verify_nonce($_POST['bd_edit_company'], 'edit_company')) {
                _e('Security Check Failed!', 'wpecbd');
                return;
            }

            //Check Our Data
            if (!isset($_POST['bd']))
                return;

            // check user capability
            if (!current_user_can('edit_post', $post_id)) {
                _e('Error! You can not edit this post!', 'wpecbd');
                return;
            }

            $p = $_POST['bd'];

            $save_data = array(
                'web_site' => isset($p['web_site']) ? esc_url($p['web_site']) : '',
                'email' => isset($p['email']) ? sanitize_email($p['email']) : '',
                'phone' => isset($p['phone']) ? apply_filters('wpecbd_phone_save', $p['phone']) : '',
                'fax' => isset($p['fax']) ? $p['fax'] : '',
                'address' => isset($p['address']) ? stripslashes($p['address']) : '',
                'city' => isset($p['city']) ? stripslashes($p['city']) : '',
                'state' => isset($p['state']) ? stripslashes($p['state']) : '',
                'country' => isset($p['country']) ? stripslashes($p['country']) : '',
                'zip' => isset($p['zip']) ? stripslashes($p['zip']) : '',
                'lat' => isset($p['lat']) ? $p['lat'] : '',
                'lng' => isset($p['lng']) ? $p['lng'] : '',
                'twitter' => isset($p['twitter']) ? $p['twitter'] : '',
                'facebook' => isset($p['facebook']) ? $p['facebook'] : '',
                'googleplus' => isset($p['googleplus']) ? $p['googleplus'] : '',
                'linkedin' => isset($p['linkedin']) ? $p['linkedin'] : '',
                'rss' => isset($p['rss']) ? esc_url($p['rss']) : '',
                'skype' => isset($p['skype']) ? $p['skype'] : '',
                'premium_end' => isset($p['premium_end']) ? $p['premium_end'] : gmdate('Y-m-d')
            );

            $save_data_format = array(
                '%s', //web_site
                '%s', //email
                '%s', //phone
                '%s', //fax
                '%s', //address
                '%s', //city
                '%s', //state
                '%s', //country
                '%s', //zip
                '%f', //lat
                '%f', //lng
                '%s', //twitter
                '%s', //facebook
                '%s', //googleplus
                '%s', //linkedin
                '%s', //rss
                '%s', //skype
                '%s', //premium_end
            );

            //Check Company Exist
            if ($wpdb->get_var($wpdb->prepare("SELECT post_id FROM " . $wpdb->prefix . "wpecbd WHERE post_id=%d", $post_id))) {
                //Update
                $a = $wpdb->update($wpdb->prefix . 'wpecbd', $save_data, array('post_id' => $post_id), $save_data_format, array('%d'));
            } else {
                //Create New Record
                $save_data = array('post_id' => $post_id) + $save_data;
                array_unshift($save_data_format, '%d');
                $wpdb->insert($wpdb->prefix . 'wpecbd', $save_data, $save_data_format);
            }
        }

        /**
         * Register Plugin Admin Menus
         */
        function register_admin_menus() {
            add_submenu_page('edit.php?post_type=wpecbd', 'Options &rsaquo; WP Business Directory', 'Options', 'update_core', 'options', array(&$this, 'page_options'));
            add_submenu_page('edit.php?post_type=wpecbd', 'Templates &rsaquo; WP Business Directory', 'Templates', 'update_core', 'templates', array(&$this, 'page_templates'));

            //CSV Importer
            add_submenu_page('tools.php', __('WP Business Directory - CSV Company Importer', 'wpecbd'), __('Directory CSV Importer', 'wpecbd'), 'update_core', 'wpecbd_csv_importer', array('wpecbd_csv_importer', 'view'));
            add_action('admin_print_scripts-tools_page_wpecbd_csv_importer', create_function('', "wp_enqueue_script('wpecbd_image_uploader');"));

            //CSV Exporter
            add_submenu_page('tools.php', __('WP Business Directory - CSV Company Exporter', 'wpecbd'), __('Directory CSV Exporter', 'wpecbd'), 'update_core', 'wpecbd_csv_exporter', array('wpecbd_csv_exporter', 'view'));
        }

        /**
         * Options Page
         */
        function page_options() {
            $nonce = wp_create_nonce('wpecbd_save_options');
            require_once(WPECBD_PLUGIN_DIR . 'inc/views/admin_options_page.php');
        }

        /**
         * Templates Page 
         */
        function page_templates() {
            $nonce = wp_create_nonce('wpecbd_save_templates');
            require_once(WPECBD_PLUGIN_DIR . 'inc/views/admin_templates_page.php');
        }

        /**
         * Get Uploaded Images
         */
        function ajax_admin_image_handler() {
            //User Logged In?
            if (!is_user_logged_in())
                die($this->ajax_error());

            //Check admin nonce
            if (isset($_GET['nonce_admin']) AND !wp_verify_nonce($_GET['nonce_admin'], 'wpecbd_admin_images')) {
                die($this->ajax_error());
                //else check normal user nonce
            } elseif (isset($_GET['nonce_user']) AND !wp_verify_nonce($_GET['nonce_user'], 'wpecbd_user_images')) {
                die($this->ajax_error());
                //who are you?
            } elseif (!isset($_GET['nonce_user']) AND !isset($_GET['nonce_admin'])) {
                die($this->ajax_error());
            }

            if (!current_user_can('edit_post', $_GET['post_id'])) {
                //Check for normal user
                $wpost = get_post($_GET['post_id']);
                if (!$wpost)
                    die($this->ajax_error());
                $user = wp_get_current_user();
                if ($wpost->post_author != $user->ID) {
                    die(json_encode(array('error' => __('Error! You can not edit this post!', 'wpecbd'))));
                }
            }

            //DELETE LOGO
            if (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'delete_logo') {
                if (wp_delete_attachment(get_post_thumbnail_id($_GET['post_id']), TRUE)) {
                    die(json_encode(array('success' => 'true')));
                } else {
                    die(json_encode(array('success' => 'false')));
                }
            }

            //DELETE IMAGE
            if (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'delete_img') {
                if (wp_delete_attachment((int) str_replace('bd_', '', $_GET['id']), TRUE)) {
                    die(json_encode(array('success' => 'true', 'remained' => $this->_can_upload_more_image($_GET['post_id']))));
                } else {
                    die(json_encode(array('success' => 'false')));
                }
            }

            if (!isset($_FILES['wpecbd_file'])) {
                die(json_encode(array('error' => __('Image file missing!', 'wpecbd'))));
            }
            if ($this->_can_upload_more_image($_GET['post_id']) <= 0 AND !isset($_GET['logo'])) {
                die(json_encode(array('error' => __('You have reached the image upload limit! In order to upload new image, please delete some images.', 'wpecbd'))));
            }

            $logo = (isset($_GET['logo'])) ? TRUE : FALSE;

            //if company already have logo delete old one
            if ($logo AND ($remove_logo = get_post_thumbnail_id($_GET['post_id']))) {
                wp_delete_attachment($remove_logo, TRUE);
            }

            $attachment_id = $this->get_image('wpecbd_file', $_GET['post_id'], $logo);
            $attachment = wp_get_attachment_image_src($attachment_id);

            if (!$logo) {
                die(json_encode(array('success' => 'true', 'attachment_id' => $attachment_id, 'url' => $attachment[0], 'remained' => $this->_can_upload_more_image($_GET['post_id']))));
            } else {
                die(json_encode(array('success' => 'true', 'attachment_id' => $attachment_id, 'url' => $attachment[0])));
            }
        }

        /**
         * Return Ajax Error
         * @return string Returns json encoded error message 
         */
        function ajax_error() {
            return json_encode(array('error' => __('Security Check Failed!', 'wpecbd')));
        }

        /**
         * Process Images
         * @param array $file $_FILES['X']
         * @param int $post_id
         * @param boolean $is_logo When its TRUE, add image as featured image
         * @param string $caption Image Description
         * @return array Return attachment_id
         */
        function get_image($file, $post_id, $is_logo = FALSE, $caption = NULL) {

            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');

            $attachment_id = media_handle_upload($file, $post_id);

            if ($is_logo) {
                update_post_meta($post_id, '_thumbnail_id', $attachment_id);
            }

            $attachment_data = array();
            $attachment_data['ID'] = $attachment_id;

            if (!is_null($caption)) {
                $attachment_data['post_excerpt'] = $caption;
            }
            wp_update_post($attachment_data);

            return $attachment_id;
        }

        function shortcode_handler_custom_map($attributes, $content = null) {
            $atts = array();
            $editable_atts = array('lat', 'lng', 'categories', 'term', 'distance',
                'unit', 'include', 'exclude', 'id');
            if (!empty($attributes)) {
                foreach ($attributes as $atts_k => $atts_v) {
                    if (in_array($atts_k, $editable_atts)) {
                        switch ($atts_k) {
                            case 'categories':
                            case 'include':
                            case 'exclude':
                                $attr_mid_buf = NULL;
                                $attr_mid_buf = @explode(',', $atts_v);
                                if (!empty($attr_mid_buf)) {
                                    $atts[$atts_k] = array();
                                    foreach ($attr_mid_buf as $attr_mid_buf_value) {
                                        $atts[$atts_k][] = trim($attr_mid_buf_value);
                                    }
                                }
                                break;
                            default:
                                $atts[$atts_k] = $atts_v;
                                break;
                        }
                    }
                }
            }

            $defaults = array(
                'lat' => NULL,
                'lng' => NULL,
                'categories' => array(),
                'term' => NULL,
                'distance' => 500,
                'unit' => 'km',
                'include' => array(),
                'exclude' => array(),
                'id' => 1,
                'width' => '100%',
                'height' => '500px'
            );

            $atts = wp_parse_args($atts, $defaults);

            //var_dump($atts);

            $results = $this->custom_map_searcher($atts['lat'], $atts['lng'], $atts['categories'], $atts['term'], (int) $atts['distance'], $atts['unit'], $atts['include'], $atts['exclude']);

            if (is_null($results)) {
                return 'No Company Found!';
            } elseif ($results === FALSE) {
                return 'ERROR!';
            } else {
                $out = '';
                $out.='<script>';
                $out.='var bd_com_results_' . $atts['id'] . '=' . json_encode($results) . ";\r\n";
                $out.='jQuery(document).ready(function() {bd_custom_map_initialize(bd_com_results_' . $atts['id'] . ',"wpecbd_custom_map-' . $atts['id'] . '",' . $this->_options['def_lat'] . ',' . $this->_options['def_lng'] . ');});';
                $out.='</script>';
                $out.='<div style="width: ' . $atts['width'] . '; height: ' . $atts['height'] . ';" class="wpecbd_custom_map" id="wpecbd_custom_map-' . $atts['id'] . '"></div>';
                $out.='<style>';
                $out.='.wpecbd_custom_map img{max-width:none !important}';
                $out.='</style>';
                return $out;
            }
        }

        /**
         * wpbd_cat Shortcode Handler
         * @param array $attributes
         * @param string $content 
         */
        function shortcode_handler_cat($attributes, $content = null) {
            //Disable Comments
            $this->disable_comments();

            $atts = array();
            $editable_atts = array('show_option_none', 'orderby', 'order',
                'show_count', 'hide_empty', 'use_desc_for_title', 'child_of',
                'feed', 'feed_type', 'feed_image', 'exclude', 'exclude_tree',
                'current_category', 'hierarchical', 'depth', 'pad_counts', 'type');
            if (!empty($attributes)) {
                foreach ($attributes as $atts_k => $atts_v) {
                    if (in_array($atts_k, $editable_atts)) {
                        $atts[$atts_k] = $atts_v;
                    }
                }
            }

            $defaults = array(
                'show_option_all' => '',
                'show_option_none' => __('No categories'),
                'orderby' => 'name',
                'order' => 'ASC',
                'style' => 'list',
                'show_count' => 0,
                'hide_empty' => 0,
                'use_desc_for_title' => 1,
                'child_of' => 0,
                'feed' => '',
                'feed_type' => '',
                'feed_image' => '',
                'exclude' => '',
                'exclude_tree' => '',
                'current_category' => 0,
                'hierarchical' => true,
                'title_li' => '',
                'echo' => 0,
                'depth' => 0,
                'taxonomy' => 'bd_categories',
                'pad_counts' => 0,
                'type' => 'v'
            );

            $atts = wp_parse_args($atts, $defaults);

            if ($atts['type'] == 'h' AND $atts['depth'] == 0) {
                $selector = 'children';
            } else {
                $selector = 'bd_cat_list';
            }

            $out = '';
            $out.='<ul class="bd_cat_list">';
            $out.=wp_list_categories($atts);
            $out.='</ul>';
            $out.='<script>jQuery(document).ready(function() { 
                    jQuery(".' . $selector . '").makeacolumnlists({
                    cols: 2, colWidth: 0,
                    equalHeight: "ul", startN: 1});});</script>';
            return $out;
        }

        /**
         * wpbd_user ShortCode Handler
         * @global object $post
         * @global object $wpdb
         * @global string $premium_end
         * @param array $atts
         * @param string $content
         * @return null
         */
        function shortcode_handler_user($atts, $content = null) {
            global $post, $wpdb, $premium_end;
            //Disable Comments
            $this->disable_comments();

            $page_post_id = $post->ID;
            $page_url = get_permalink($post->ID);

            $out = '';

            $replace_del = array();
            $replace_val = array();
            //User Page URL
            $replace_del[] = '%%PAGE_URL%%';
            $replace_val[] = $page_url;
            //Login URL
            $replace_del[] = '%%LOGIN_URL%%';
            $replace_val[] = wp_login_url($page_url);
            //Register URL
            $replace_del[] = '%%REGISTER_URL%%';
            $replace_val[] = site_url('wp-login.php?action=register', 'login');

            //User editabe content prepare
            $user_content = array();
            //Process wpbd shortcodes
            if (!empty($content) AND preg_match_all("#{wpbd_[a-z]{4}}(.*){/wpbd_[a-z]{4}}#smU", $content, $content_a) AND !empty($content_a[0])) {
                foreach ($content_a[0] as $content_ak => $content_av) {
                    $sp_h = substr($content_av, 6, 4);
                    $user_content[$sp_h] = trim($content_a[1][$content_ak]);
                }
            }

            //Check user logged in
            if (!is_user_logged_in()):
                if (!empty($user_content['nlog'])) {
                    return str_replace($replace_del, $replace_val, $user_content['nlog']);
                } else {
                    return "You have to use {wpbd_nlog}???{/wpbd_nlog}<br /> Please check Wordpress Business Directory Referance Document";
                }
            else:
                $current_user = wp_get_current_user();
                $posts_array = $wpdb->get_results("SELECT $wpdb->posts.*," . $wpdb->prefix . "wpecbd.* FROM $wpdb->posts
                                                INNER JOIN " . $wpdb->prefix . "wpecbd ON $wpdb->posts.ID=" . $wpdb->prefix . "wpecbd.post_id
                                                WHERE post_type='wpecbd' AND post_author=$current_user->ID AND post_status IN ('pending','publish','trash') ORDER BY post_date");

                //Translate Package Names
                $name_translate = array(
                    '1 Month' => '1 ' . __('Month', 'wpecbd'),
                    '3 Months' => '3 ' . __('Months', 'wpecbd'),
                    '6 Months' => '6 ' . __('Months', 'wpecbd'),
                    '1 Year' => '1 ' . __('Year', 'wpecbd'),
                    '2 Years' => '2 ' . __('Years', 'wpecbd'),
                    '3 Years' => '3 ' . __('Years', 'wpecbd')
                );

                /*
                 * ===============
                 * PAYPAL CHECKOUT
                 * ===============
                 */

                if (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'buy_premium') {

                    if (!empty($user_content['buex'])) {
                        // {wpbd_buex} Buy premium page head {/wpbd_buex}
                        $out.= str_replace($replace_del, $replace_val, $user_content['buex']);
                    } else {
                        return "You have to use {wpbd_buex}???{/wpbd_buex}<br /> Please check Wordpress Business Directory Referance Document";
                    }

                    $out.= '<form id="bd_premium_buy" action="' . $page_url . '?bd_action=set_paypal_order" method="post">';
                    $out.= '<table>';
                    $out.= '<tr>';
                    $out.= '<td>' . __('Select Company', 'wpecbd') . '</td>';
                    $out.= '<td>';
                    $out.= '<select id="bd_prem_select_company" name="company">';
                    $out.= '<option value="x" disabled="disabled">' . __('Select Company', 'wpecbd') . '</option>';
                    foreach ($posts_array as $post_for_prem_set) {
                        $out.= '<option value="' . $post_for_prem_set->ID . '" ' . (($post_for_prem_set->post_status != 'publish') ? 'disabled="disabled"' : '') . '>' . $post_for_prem_set->post_title . '</option>' . "\n";
                    }
                    $out.= '</select>';
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '<tr>';
                    $out.= '<td>' . __('Select Duration', 'wpecbd') . '</td>';
                    $out.= '<td>';
                    $out.= '<select id="bd_prem_select_plan" name="plan">';
                    foreach ($this->_options['prem_plans'] as $p_name => $p_v) {
                        if ($p_v['price'] == 0)
                            continue;
                        $out.= '<option id="bd_prem_select_option" value="' . $p_name . '">' . sprintf('%s - %.2f %s', $name_translate[$p_name], $p_v['price'], $this->_options['currency']) . '</option>' . "\n";
                    }
                    $out.= '</select>';
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '<tr>';
                    $out.= '<td></td>';
                    $out.= '<td><img id="bd_paypal_buy" style="cursor: pointer;" src="' . WPECBD_PLUGIN_URL . 'img/btn_xpressCheckout.gif" /></td>';
                    $out.= '</tr>';
                    $out.= '</table>';
                    $out.= '</form>';
                    $out.= '<script>';
                    $out.= 'jQuery("#bd_paypal_buy").click(function(){if(jQuery("#bd_prem_select_company option:selected").val()=="x"){alert("' . __('Please select Company!', 'wpecbd') . '");return false;}else{jQuery("#bd_premium_buy").submit();}});';
                    $out.= '</script>';

                    return $out;

                    //SET ORDER
                }elseif (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'set_paypal_order') {

                    if (!isset($this->_options['prem_plans'][$_POST['plan']])) {
                        return __('Security Check Failed!', 'wpecbd');
                    }
                    $nvpArray = array();
                    //Amount
                    $nvpArray['PAYMENTREQUEST_0_ITEMAMT'] = $this->_options['prem_plans'][$_POST['plan']]['price'];
                    //Amount Total
                    $nvpArray['PAYMENTREQUEST_0_AMT'] = $this->_options['prem_plans'][$_POST['plan']]['price'];
                    //Currency Code
                    $nvpArray['PAYMENTREQUEST_0_CURRENCYCODE'] = $this->_options['currency'];
                    //Payment Type
                    $nvpArray['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
                    //Item name
                    $nvpArray['L_PAYMENTREQUEST_0_NAME0'] = $name_translate[$_POST['plan']] . ' ' . __('Premium Listing', 'wpecbd');
                    //Item description
                    $nvpArray['L_PAYMENTREQUEST_0_DESC0'] = get_bloginfo('name') . '(' . get_bloginfo('wpurl') . ') ' . $name_translate[$_POST['plan']] . ' ' . __('Premium Listing', 'wpecbd');
                    //Cost of item
                    $nvpArray['L_PAYMENTREQUEST_0_AMT0'] = $this->_options['prem_plans'][$_POST['plan']]['price'];
                    //Item quantity
                    $nvpArray['L_PAYMENTREQUEST_0_QTY0'] = '1';
                    //Indicates whether an item is digital or physical
                    $nvpArray['L_PAYMENTREQUEST_0_ITEMCATEGORY0'] = 'Digital';
                    //Return URL
                    $nvpArray['ReturnUrl'] = $page_url . '?bd_action=get_paypal_order&company=' . $_POST['company'] . '&plan=' . $_POST['plan'] . '&control=' . wp_create_nonce('bd_pp_' . $_POST['plan'] . '_' . $_POST['company']);
                    //Cancel URL
                    $nvpArray['CANCELURL'] = $page_url;
                    //No Shipping For Digital Goods
                    $nvpArray['NOSHIPPING'] = '1';
                    //Allow Note
                    $nvpArray['ALLOWNOTE'] = '1';
                    //Language
                    $nvpArray['LOCALECODE'] = $this->_options['paypal']['loca'];
                    //SiteName
                    $nvpArray['BRANDNAME'] = get_bloginfo('name');
                    //Customer User Name
                    $nvpArray['BUYERUSERNAME'] = $current_user->user_login;
                    //Customer Registration Date
                    $nvpArray['BUYERREGISTRATIONDATE'] = date(DATE_ISO8601, strtotime($current_user->user_registered));
                    //Company ID
                    $nvpArray['PAYMENTREQUEST_0_CUSTOM'] = $_POST['company'];
                    if (!empty($this->_options['paypal']['img'])) {
                        $nvpArray['HDRIMG'] = $this->_options['paypal']['img'];
                    }
                    
                    $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $nvpArray);

                    if ("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
                        // Redirect to paypal.com.
                        $token = urldecode($httpParsedResponseAr["TOKEN"]);
                        $payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token";
                        if ("sandbox" === $this->_options['paypal']['envi'] || "beta-sandbox" === $this->_options['paypal']['envi']) {
                            $payPalURL = "https://www." . $this->_options['paypal']['envi'] . ".paypal.com/webscr&cmd=_express-checkout&token=$token";
                        }
                        $out.= '<div><img id="bd_paypal_redirecting" src="' . WPECBD_PLUGIN_URL . 'img/loading.gif' . '" /> ' . sprintf(__('You Will Be Redirected To PayPal In A Few Seconds, Please Wait... <br />If You Are Not Redirected Within 10 Seconds Please <a href="%s">Click Here</a>', 'wpecbd'), $payPalURL) . '</div>';
                        $out.= '<script>jQuery(document).ready(function() {window.location.replace("' . $payPalURL . '");});</script>';
                    } else {
                        error_log('SetExpressCheckout failed: ' . print_r($httpParsedResponseAr, true));
                        $out.= 'SetExpressCheckout failed! Please check error log..';
                    }

                    return $out;

                    //REVIEW ORDER
                } elseif (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'get_paypal_order' AND array_key_exists('token', $_REQUEST)) {

                    $get_details = $this->PPHttpPost('GetExpressCheckoutDetails', array('TOKEN' => urlencode(htmlspecialchars($_REQUEST['token']))));

                    if ((!isset($_GET['control']) OR !isset($_GET['company']) OR !isset($_GET['plan'])) AND !wp_verify_nonce($_GET['control'], 'bd_pp_' . $_GET['plan'] . '_' . $_GET['company'])) {
                        return __('Security Check Failed!', 'wpecbd');
                    }

                    if ("SUCCESS" != strtoupper($get_details["ACK"]) || "SUCCESSWITHWARNING" != strtoupper($get_details["ACK"]))
                        return __('PayPal Checkout Failed', 'wpecbd');

                    //Preview Page
                    if (!empty($user_content['pppr'])) {
                        // {wpbd_pppr} Paypal returned users for buying premium, review your order and confirm page {/wpbd_pppr}
                        $out.= str_replace($replace_del, $replace_val, $user_content['pppr']);
                    } else {
                        return "You have to use {wpbd_pppr}???{/wpbd_pppr}<br /> Please check Wordpress Business Directory Referance Document";
                    }

                    $out.= '<table id="bd_pp_prev_table">';
                    $out.= '<tr>';
                    $out.= '<td class="bd_pp_prev_title">';
                    $out.= '<strong>' . __('Your eMail', 'wpecbd') . '</strong>';
                    $out.= '</td>';
                    $out.= '<td class="bd_pp_prev_desc">';
                    $out.= urldecode($get_details['EMAIL']);
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '<tr>';
                    $out.= '<td class="bd_pp_prev_title">';
                    $out.= '<strong>' . __('Selected Package', 'wpecbd') . '</strong>';
                    $out.= '</td>';
                    $out.= '<td class="bd_pp_prev_desc">';
                    $out.= $name_translate[$_GET['plan']];
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '<tr>';
                    $out.= '<td class="bd_pp_prev_title">';
                    $out.= '<strong>' . __('Premium Listing For', 'wpecbd') . '</strong>';
                    $out.= '</td>';
                    $out.= '<td class="bd_pp_prev_desc">';
                    $out.= '<a href="' . get_permalink($_GET['company']) . '">' . get_the_title($_GET['company']) . '</a>';
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '<tr>';
                    $out.= '<td class="bd_pp_prev_title">';
                    $out.= '<strong>' . __('Total Amount', 'wpecbd') . '</strong>';
                    $out.= '</td>';
                    $out.= '<td class="bd_pp_prev_desc">';
                    $out.= urldecode($get_details['L_AMT0']) . ' ' . $get_details['CURRENCYCODE'];
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '<tr>';
                    $out.= '<td id="bd_pp_prev_title" colspan="2">';
                    $out.= '<input id="bd_pp_prev_confirm" type="button" value="' . __('Confirm', 'wpecbd') . '" />';
                    $out.= '</td>';
                    $out.= '</tr>';
                    $out.= '</tr>';
                    $out.= '</table>';
                    $out.='<script>';
                    $out.='jQuery("#bd_pp_prev_confirm").click(function(){window.location.href = "' . $page_url . '?bd_action=make_paypal_order&company=' . $_GET['company'] . '&plan=' . $_GET['plan'] . '&control=' . $_GET['control'] . '&token=' . $_REQUEST['token'] . '";});';
                    $out.='</script>';

                    return $out;

                    //GET PAYMENT
                } elseif (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'make_paypal_order' AND array_key_exists('token', $_REQUEST)) {
                    //Security Check for prevent the any fraud
                    if ((!isset($_GET['control']) OR !isset($_GET['company']) OR !isset($_GET['plan'])) AND !wp_verify_nonce($_GET['control'], 'bd_pp_' . $_GET['plan'] . '_' . $_GET['company'])) {
                        return __('Security Check Failed!', 'wpecbd');
                    }
                    $get_details = $this->PPHttpPost('GetExpressCheckoutDetails', array('TOKEN' => urlencode(htmlspecialchars($_REQUEST['token']))));

                    if ("SUCCESS" == strtoupper($get_details["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($get_details["ACK"])) {

                        $nvpArray = array();
                        //TOKEN
                        $nvpArray['TOKEN'] = $_REQUEST['token'];
                        //Unique PayPal buyer account identification number
                        $nvpArray['PAYERID'] = $get_details['PAYERID'];
                        //Payment Type
                        $nvpArray['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
                        //Amount
                        $nvpArray['PAYMENTREQUEST_0_ITEMAMT'] = $this->_options['prem_plans'][$_GET['plan']]['price'];
                        //Amount
                        $nvpArray['PAYMENTREQUEST_0_AMT'] = $this->_options['prem_plans'][$_GET['plan']]['price'];
                        //Currency Code
                        $nvpArray['PAYMENTREQUEST_0_CURRENCYCODE'] = $this->_options['currency'];
                        //Item name
                        $nvpArray['L_PAYMENTREQUEST_0_NAME0'] = $name_translate[$_GET['plan']] . ' ' . __('Premium Listing', 'wpecbd');
                        //Item description
                        $nvpArray['L_PAYMENTREQUEST_0_DESC0'] = get_bloginfo('name') . '(' . get_bloginfo('wpurl') . ') ' . $name_translate[$_GET['plan']] . ' ' . __('Premium Listing', 'wpecbd');
                        //Cost of item
                        $nvpArray['L_PAYMENTREQUEST_0_AMT0'] = $this->_options['prem_plans'][$_GET['plan']]['price'];
                        //Item quantity
                        $nvpArray['L_PAYMENTREQUEST_0_QTY0'] = '1';
                        //Indicates whether an item is digital or physical
                        $nvpArray['L_PAYMENTREQUEST_0_ITEMCATEGORY0'] = 'Digital';
                        //A per transaction description of the payment that is passed to the buyer���s credit card statement.
                        $nvpArray['PAYMENTREQUEST_n_SOFTDESCRIPTOR'] = strtolower(get_bloginfo('name'));

                        $paypal_last = $this->PPHttpPost('DoExpressCheckoutPayment', $nvpArray);

                        if (("SUCCESS" == strtoupper($paypal_last["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($paypal_last["ACK"])) AND "COMPLETED" == strtoupper($paypal_last["PAYMENTINFO_0_PAYMENTSTATUS"])) {
                            //Upgrade Company to Premium
                            $now = strtotime(gmdate('Y-m-d'));
                            if ($this->is_premium($_GET['company'])) {
                                $old_prem_end_date = $wpdb->get_var($wpdb->prepare("SELECT premium_end FROM " . $wpdb->prefix . "wpecbd WHERE post_id=%d", $_GET['company']));
                            } else {
                                $old_prem_end_date = gmdate('Y-m-d');
                            }

                            $new_prem_end_date = gmdate('Y-m-d', strtotime($this->_options['prem_plans'][$_GET['plan']]['str'] . ' 1 day', strtotime($old_prem_end_date)));

                            $wpdb->query($wpdb->prepare("UPDATE " . $wpdb->prefix . "wpecbd SET premium_end=%s WHERE post_id=%d", $new_prem_end_date, $_GET['company']));

                            $com_name = get_the_title($_GET['company']);
                            $com_url = get_permalink($_GET['company']);
                            $sale_amt = urldecode($paypal_last['PAYMENTINFO_0_AMT']) . urldecode($paypal_last['PAYMENTINFO_0_CURRENCYCODE']);
                            $date_end = date_i18n(get_option('date_format'), strtotime($new_prem_end_date));

                            //Mail to admin
                            add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
                            wp_mail(get_bloginfo('admin_email'), 'Premium Sale #' . $_GET['company'], 'Hi admin,<br />
                                You have just sell a premium listing on your web site.<br />
                                <b>Company Name:</b> ' . $com_name . '<br />
                                <b>Company ID:</b> ' . $_GET['company'] . '<br />
                                <b>Company URL:</b> ' . $com_url . '<br />
                                <b>Amount:</b> ' . $sale_amt . '<br />
                                <b>Duration:</b> ' . $name_translate[$_GET['plan']] . '<br />
                                <b>Old Date:</b> ' . date_i18n(get_option('date_format'), strtotime($old_prem_end_date)) . '<br />
                                <b>New Date:</b> ' . $date_end . '<br />
                                <b>Transaction ID:</b> ' . $paypal_last['PAYMENTINFO_0_TRANSACTIONID'] . '<br />
                                <b>Order Time:</b> ' . urldecode($paypal_last['PAYMENTINFO_0_ORDERTIME']) . '<br /><br />
                                ' . print_r($paypal_last, TRUE));

                            $replace_del[] = '%%DATEEND%%';
                            $replace_val[] = $date_end;

                            $replace_del[] = '%%LEFTTIME%%';
                            $replace_val[] = $prem_data = $this->time_diff(strtotime($new_prem_end_date) - $now);

                            $replace_del[] = '%%COMNAME%%';
                            $replace_val[] = $com_name;

                            $replace_del[] = '%%COMURL%%';
                            $replace_val[] = $com_url;

                            $replace_del[] = '%%SALEAMT%%';
                            $replace_val[] = $sale_amt;

                            if (!empty($user_content['ppsu'])) {
                                $out .= str_replace($replace_del, $replace_val, $user_content['ppsu']);
                            } else {
                                return "You have to use {wpbd_ppsu}???{/wpbd_ppsu}<br /> Please check Wordpress Business Directory Referance Document";
                            }
                        } else {
                            return __('PayPal Checkout Failed', 'wpecbd');
                        }
                    } else {
                        return __('PayPal Checkout Failed', 'wpecbd');
                    }

                    return $out;

                    /*
                     * /===================
                     * /PAYPAL CHECKOUT END
                     * /===================
                     */
                } else {
                    if (empty($posts_array)) {
                        $page_nonce = wp_create_nonce('wpecbd_user');
                        $replace_del[] = '%%NEWCOM%%';
                        $replace_val[] = get_bloginfo('wpurl') . '/wpecbd_user/new_company.html?nonce=' . $page_nonce;
                        if (!empty($user_content['usnc'])) {
                            //{wpbd_usnc} Signed in but has no company users {/wpbd_usnc}
                            return str_replace($replace_del, $replace_val, $user_content['usnc']);
                        } else {
                            return "You have to use {wpbd_usnc}???{/wpbd_usnc}<br /> Please check Wordpress Business Directory Referance Document";
                        }
                    } else {
                        $page_nonce = wp_create_nonce('wpecbd_user');
                        //Buy Premium
                        $replace_del[] = '%%BUYPRE%%';
                        $replace_val[] = $page_url . '?bd_action=buy_premium';
                        //New Company
                        $replace_del[] = '%%NEWCOM%%';
                        $replace_val[] = get_bloginfo('wpurl') . '/wpecbd_user/new_company.html?nonce=' . $page_nonce;

                        if (!empty($user_content['lica'])) {
                            //{wpbd_lica} Signed in and has company users, header{/wpbd_lica}
                            $out.= str_replace($replace_del, $replace_val, $user_content['lica']);
                        } else {
                            return "You have to use {wpbd_lica}???{/wpbd_lica}<br /> Please check Wordpress Business Directory Referance Document";
                        }

                        $out.= "<script type='text/javascript' src='//www.google.com/jsapi'></script>";
                        $out.= "<script type='text/javascript'>";
                        $out.= "var cssClassNames = {
                                'headerRow': 'bd_com_list_headerRow',
                                'tableRow': 'bd_com_list_tableRow',
                                'oddTableRow': 'bd_com_list_oddTableRow',
                                'selectedTableRow': 'bd_com_list_selectedTableRow',
                                'hoverTableRow': 'bd_com_list_hoverTableRow',
                                'headerCell': 'bd_com_list_headerCell',
                                'tableCell': 'bd_com_list_tableCell',
                                'rowNumberCell': 'bd_com_list_rowNumberCell'};";
                        $out.= "var table_options = {
                                'showRowNumber': false,
                                'allowHtml': true,
                                'cssClassNames': cssClassNames,
                                'page':'enable'
                            };";
                        $out.= "google.load('visualization', '1', {packages:['table']});";
                        $out.= "google.setOnLoadCallback(drawTable);";
                        $out.= 'function drawTable() {';
                        $out.= 'var data = new google.visualization.DataTable();';
                        $out.= "data.addColumn('string', '" . __('Name', 'wpecbd') . "');";
                        $out.= "data.addColumn('string', '" . __('Status', 'wpecbd') . "');";
                        if ($this->_options['premium'])
                            $out.= "data.addColumn('string', '" . __('Premium', 'wpecbd') . "');";
                        $out.= 'data.addRows(' . count($posts_array) . ');';

                        for ($com_index = 0; $com_index < count($posts_array); $com_index++) {
                            switch ($posts_array[$com_index]->post_status) {
                                case 'publish':
                                    $post_status = __('Approved', 'wpecbd');
                                    break;
                                case 'pending':
                                    $post_status = __('Pending Approval', 'wpecbd');

                                    break;
                                case 'trash':
                                    $post_status = __('Not Accepted', 'wpecbd');
                                    break;
                                default :
                                    continue;
                            }
                            //Prepare Title
                            if ($posts_array[$com_index]->post_status == 'publish') {
                                $title_buff = '<a href="' . get_permalink($posts_array[$com_index]->ID) . '">' . addslashes($posts_array[$com_index]->post_title) . '</a>';
                            } else {
                                $title_buff = addslashes($posts_array[$com_index]->post_title);
                            }

                            $out.= "data.setCell($com_index, 0, '[<a class=\"wpbd_user_edit\" href=\"" . get_bloginfo('wpurl') . "/wpecbd_user/" . $posts_array[$com_index]->ID . ".html?nonce=$page_nonce\">" . __('Edit', 'wpecbd') . "</a>] $title_buff');\n";
                            $out.= "data.setCell($com_index, 1, '$post_status');\n";
                            if ($this->_options['premium']) {
                                $premium_end = $posts_array[$com_index]->premium_end;
                                if ($this->is_premium()) {
                                    $prem_end = strtotime($premium_end);
                                    $now = strtotime(gmdate('Y-m-d'));
                                    $prem_data = $this->time_diff($prem_end - $now) . ' ' . __('left', 'wpecbd');
                                } else {
                                    $prem_data = __('No', 'wpecbd');
                                }
                                $out.= "data.setCell($com_index, 2, '$prem_data');\n";
                            }
                        }

                        $out.= "var table = new google.visualization.Table(document.getElementById('table_div'));";
                        $out.= "table.draw(data, table_options);";
                        $out.= "}";
                        $out.= "</script>";
                        $out.= "<div id='table_div'></div>";

                        if (!empty($user_content['licb'])) {
                            $out.= str_replace($replace_del, $replace_val, $user_content['licb']);
                        } else {
                            $out.= "You have to use {wpbd_licb}???{/wpbd_licb}<br /> Please check Wordpress Business Directory Referance Document";
                        }

                        return $out;
                    }
                }
            endif;
        }

        /**
         * Remove all stuff which is related with post when post delete
         * @global object $wpdb
         * @param int $post_id Post ID
         * @return NULL 
         */
        function cleanup_after_delete($post_id) {
            global $wpdb;

            //Check for our post type?
            if (get_post_type($post_id) != 'wpecbd')
                return;

            //Delete row on plugin table
            $wpdb->query("DELETE FROM " . $wpdb->prefix . "wpecbd WHERE post_id=" . $wpdb->escape($post_id));

            //Delete attachments
            $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post_id);
            $attachments = get_posts($args);
            foreach ($attachments as $attachment) {
                wp_delete_attachment($attachment->ID, TRUE); //Force delete
            }
        }

        /**
         * Counts uploaded images for company exclude logo
         * @param int $post_id Post id
         * @return int Total number of images exclude logo
         */
        function _count_post_images($post_id) {
            if (!is_numeric($post_id))
                return;
            $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post_id);
            $attachments = get_posts($args);
            return (int) (count($attachments) - ((has_post_thumbnail($post_id)) ? 1 : 0));
        }

        /**
         * Returns remained image slot for company
         * @param int $post_id Post ID
         * @return int Remained image for post
         */
        function _can_upload_more_image($post_id) {
            if (!is_numeric($post_id))
                return;
            return (int) (($this->is_premium($post_id)) ? $this->_options['p_maximg'] : $this->_options['f_maximg']) - ($this->_count_post_images($post_id));
        }

        /**
         * Check Is This Post Premium?
         * @global object $post Post object
         * @global object $wpdb Databse object
         * @global string $premium_end Premium end date
         * @global boolean $is_premium
         * @param int $post_id_ Post ID, if $post not definated
         * @param boolean $premium_end_defined
         * @return boolean Is premium or not
         */
        function is_premium($post_id_ = NULL, $premium_end_defined = FALSE) {
            global $post, $wpdb, $premium_end, $is_premium;

            //Check Premium Feature Active
            //If its deactive make all posts free
            if ($this->_options['premium'] == 0) {
                $is_premium = FALSE;
                return;
            }

            if (!$premium_end_defined) {
                if (!empty($post->premium_end)) {
                    $premium_end = $post->premium_end;
                } elseif (isset($post->ID)) {
                    $post_id = $post->ID;
                } elseif (!is_numeric($post_id_)) {
                    return FALSE;
                }

                if (empty($premium_end)) {
                    $premium_end = $wpdb->get_var("SELECT premium_end FROM " . $wpdb->prefix . "wpecbd WHERE post_id=" . $wpdb->escape((is_null($post_id_) ? $post_id : $post_id_)));
                }
            }

            if (empty($premium_end))
                return FALSE;

            //Create Date Object
            $prem_end = new DateTime($premium_end);
            $now = new DateTime(gmdate('Y-m-d'));
            //Compare dates
            if ($prem_end <= $now) {
                $is_premium = FALSE;
            } else {
                $is_premium = TRUE;
            }
            return $is_premium;
        }

        /**
         * Gives remaining time in x years x months x days
         * @param int $endtime Must be differance two timestamp $end-$now
         * @return string x years x months x days
         * @todo Minus?
         */
        function time_diff($endtime) {
            $days = (date("j", $endtime) - 1);
            $months = (date("n", $endtime) - 1);
            $years = (date("Y", $endtime) - 1970);

            return (($years > 0) ? ' ' . $years . ' ' . (($years === 1) ? __('Year', 'wpecbd') : __('Years', 'wpecbd')) : '') . (($months > 0) ? ' ' . $months . ' ' . (($months === 1) ? __('Month', 'wpecbd') : __('Months', 'wpecbd')) : '') . (($days > 0) ? ' ' . $days . ' ' . (($days === 1) ? __('Day', 'wpecbd') : __('Days', 'wpecbd')) : '');
        }

        /**
         * Gives option values for country selector.
         * @param string $selected If there are a selected country it marks selected
         * @return string Returns all countries in <option val="Country_Name">Country_Name</option> format
         */
        function country_selector($selected = NULL) {
            $countries = parse_ini_file(WPECBD_PLUGIN_DIR . 'inc/countries.ini');

            $out = '<option org_name="dontchoose" disabled="disabled">' . __('Select Country', 'wpecbd') . '</option>';
            foreach ($countries as $org_name => $country) {
                $out.='<option org_name="' . $org_name . '" value="' . $country . '" ' . selected($selected, $country, FALSE) . '>' . $country . '</option>' . "\r\n";
            }
            return $out;
        }

        /**
         * Checks options status for free, premium or both
         * @global boolean $is_premium
         * @global object $post
         * @param string $s Option Name
         * @param boolean $general When it is true checks options for both case, free or premium
         * @return boolean Return feature active or not for company or general
         */
        function check_option($s, $general = FALSE) {
            global $is_premium, $post;

            if ($general) {
                if ($this->_options['p_' . $s] OR $this->_options['f_' . $s]) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }

            if (!isset($is_premium)) {
                $is_premium = $this->is_premium();
            }

            return (bool) (($is_premium) ? $this->_options['p_' . $s] : $this->_options['f_' . $s]);
        }

        /**
         * Prepares New/Edit Company Popup Page
         * @global object $post
         * @global object $wpdb
         * @global boolean $is_premium
         * @global string $premium_end
         * @global boolean $is_new
         * @global array $post_array
         * @global array $val_r
         * @global array $val_m
         * @param int $post_id 
         */
        function user_edit_page($post_id) {
            global $post, $wpdb, $is_premium, $premium_end, $is_new, $post_array, $val_r, $val_m;

            if (is_numeric($post_id)) {
                $post_id_buff = $post_id;
            } elseif (!empty($_POST['post_id'])) {
                $post_id_buff = $_POST['post_id'];
            } else {
                $post_id_buff = FALSE;
            }

            if ($post_id_buff) {
                $premium_end = $wpdb->get_var($wpdb->prepare("SELECT premium_end FROM " . $wpdb->prefix . "wpecbd WHERE post_id=%d", $post_id_buff));
                if (!$premium_end)
                    $premium_end = '1980-01-01';
            }else {
                $premium_end = '1980-01-01';
            }

            //Check Premium Status
            $is_premium = $this->is_premium();
            ?>
            <!doctype html>
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" />
                <link rel="stylesheet" href="<?php echo WPECBD_PLUGIN_URL . 'css/formee-structure.css'; ?>" />
                <link rel="stylesheet" href="<?php echo WPECBD_PLUGIN_URL . 'css/formee-style.css'; ?>" />
                <link rel="stylesheet" href="<?php echo WPECBD_PLUGIN_URL . 'css/user_area.css'; ?>" />
                <script src="//maps.google.com/maps/api/js?sensor=false<?php echo ((!empty($this->_options['map_api_key'])) ? '&key=' . $this->_options['map_api_key'] : ''); ?>"></script>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/fileuploader.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/jquery.ui.addresspicker.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/formee.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/jquery.maskedinput-1.3.min.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/tag-it.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/jquery.multiselect.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/jquery.validate.js'; ?>"></script>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/wpecbd-user.js'; ?>"></script>
            </head>
            <body>
                <div>
                    <?php
                    //jQuery Form Validation Variables
                    $val_r = array();
                    $val_m = array();

                    $user = wp_get_current_user();
                    if (!wp_verify_nonce($_GET['nonce'], 'wpecbd_user') OR !is_user_logged_in())
                        die('<div class="formee-msg-error"><h4>' . __('Something Wrong! Security Check Failed!', 'wpecbd') . '</h4></div>');


                    if (isset($_GET['bd_action']) AND $_GET['bd_action'] == 'save') {
                        if ($_POST['post_id'] != 'new_company') {
                            $post_id = $_POST['post_id'];
                        }

                        $p = $_POST;

                        //Form validate & errors
                        $erra = array();

                        foreach ($p as $p_key => $p_value) {
                            if (!is_array($p_value))
                                $p[$p_key] = strip_tags($p_value);
                        }

                        //Captcha Check
                        if ($this->_options['captcha']) {
                            require_once(WPECBD_PLUGIN_DIR . 'inc/recaptchalib.php');
                            $cap_resp = recaptcha_check_answer($this->_options['captcha_private_key'], $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
                            if (!$cap_resp->is_valid) {
                                $erra['recaptcha_response_field'] = __('Wrong Captcha', 'wpecbd');
                            }
                        }

                        //Server Side Form Validation
                        $general_url_pattern = '([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i';
                        if (strlen($p['title']) < 3) {
                            $erra['title'] = __('Please enter your company name', 'wpecbd');
                        }

                        if ($this->check_option('website') AND $this->_options['r_website'] AND !preg_match("/^((http|https):\/\/)?" . $general_url_pattern, $p['web_site'])) {
                            $erra['web_site'] = __('Please enter your company web site', 'wpecbd');
                        }

                        if ($this->check_option('email') AND $this->_options['r_email'] AND !preg_match("/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i", $p['email'])) {
                            $erra['email'] = __('Please enter your company e-mail address', 'wpecbd');
                        }

                        if ($this->check_option('phone') AND $this->_options['r_phone'] AND strlen($p['phone']) < 3) {
                            $erra['phone'] = __('Please enter your company phone number', 'wpecbd');
                        }

                        if ($this->check_option('fax') AND $this->_options['r_fax'] AND strlen($p['fax']) < 3) {
                            $erra['fax'] = __('Please enter your company fax number', 'wpecbd');
                        }

                        if ($this->check_option('address') AND $this->_options['r_address'] AND strlen($p['address']) < 5) {
                            $erra['address'] = __('Please enter your company address', 'wpecbd');
                        }

                        if ($this->check_option('city') AND $this->_options['r_city'] AND empty($p['city'])) {
                            $erra['city'] = __('Please enter your city', 'wpecbd');
                        }

                        if ($this->check_option('state') AND $this->_options['r_state'] AND empty($p['state'])) {
                            $erra['state'] = __('Please enter your state', 'wpecbd');
                        }

                        if ($this->check_option('zip') AND $this->_options['r_zip'] AND empty($p['zip'])) {
                            $erra['zip'] = __('Please enter your zip code', 'wpecbd');
                        }

                        if ($this->check_option('country') AND $this->_options['r_country'] AND empty($p['country'])) {
                            $erra['country'] = __('Please select your country', 'wpecbd');
                        }

                        if ($this->check_option('shortd') AND $this->_options['r_shortd'] AND strlen($p['excerpt']) <= $this->_options['shortd_min']) {
                            $erra['excerpt'] = __('Please enter your company short description', 'wpecbd');
                        }

                        if ($this->check_option('longd') AND strlen($p['content']) <= $this->_options['longd_min']) {
                            $erra['content'] = __('Please enter your company description', 'wpecbd');
                        }

                        if ($this->check_option('twitter') AND $this->_options['r_twitter'] AND strlen($p['twitter']) < 2) {
                            $erra['twitter'] = __('Please enter your company twitter username', 'wpecbd');
                        }

                        if ($this->check_option('facebook') AND $this->_options['r_facebook'] AND !((bool) preg_match("/^((http|https):\/\/)?" . $general_url_pattern, $p['facebook']))) {
                            $erra['facebook'] = __('Please enter your company facebook url', 'wpecbd');
                        }

                        if ($this->check_option('googleplus') AND $this->_options['r_googleplus'] AND !((bool) preg_match("/^((http|https):\/\/)?" . $general_url_pattern, $p['googleplus']))) {
                            $erra['googleplus'] = __('Please enter your company googleplus url', 'wpecbd');
                        }

                        if ($this->check_option('linkedin') AND $this->_options['r_linkedin'] AND !((bool) preg_match("/^((http|https):\/\/)?" . $general_url_pattern, $p['linkedin']))) {
                            $erra['linkedin'] = __('Please enter your company linkedin url', 'wpecbd');
                        }

                        if ($this->check_option('rss') AND $this->_options['r_rss'] AND !((bool) preg_match("/^((http|https|feed|rss):\/\/)?" . $general_url_pattern, $p['rss']))) {
                            $erra['rss'] = __('Please enter your company rss url', 'wpecbd');
                        }

                        if ($this->check_option('skype') AND $this->_options['r_skype'] AND strlen($p['skype']) < 3) {
                            $erra['skype'] = __('Please enter your skype username', 'wpecbd');
                        }

                        if ($this->_options['category']) {
                            if (!empty($p['categories']) AND $this->_options['max_cat'] < count($p['categories'])) {
                                $erra['categories[]'] = __('You have reached the limit for choosing category', 'wpecbd');
                            } elseif ($this->_options['category'] AND !isset($p['categories'])) {
                                $erra['categories[]'] = __('You have to select at least one category', 'wpecbd');
                            }
                        }

                        if ($this->_options['tag'] AND $this->_options['max_tag'] AND $this->_options['max_tag'] < count(explode(',', $p['tags']))) {
                            $erra['tags'] = __('You have reached the limit for adding tag', 'wpecbd');
                        }

                        if ($this->check_option('map') AND $this->_options['r_map'] AND $this->_options['def_lat'] == $p['lat']) {
                            $erra['map_search'] = __('Please select your company location on the map', 'wpecbd');
                        }

                        $save_data = array(
                            'web_site' => isset($p['web_site']) ? esc_url($p['web_site']) : '',
                            'email' => isset($p['email']) ? sanitize_email($p['email']) : '',
                            'phone' => isset($p['phone']) ? apply_filters('wpecbd_phone_save', $p['phone']) : '',
                            'fax' => isset($p['fax']) ? $p['fax'] : '',
                            'address' => isset($p['address']) ? stripslashes($p['address']) : '',
                            'city' => isset($p['city']) ? stripslashes($p['city']) : '',
                            'state' => isset($p['state']) ? stripslashes($p['state']) : '',
                            'country' => isset($p['country']) ? stripslashes($p['country']) : '',
                            'zip' => isset($p['zip']) ? $p['zip'] : '',
                            'lat' => isset($p['lat']) ? $p['lat'] : '',
                            'lng' => isset($p['lng']) ? $p['lng'] : '',
                            'twitter' => isset($p['twitter']) ? $p['twitter'] : '',
                            'facebook' => isset($p['facebook']) ? $p['facebook'] : '',
                            'googleplus' => isset($p['googleplus']) ? $p['googleplus'] : '',
                            'linkedin' => isset($p['linkedin']) ? $p['linkedin'] : '',
                            'rss' => isset($p['rss']) ? esc_url($p['rss']) : '',
                            'skype' => isset($p['skype']) ? $p['skype'] : ''
                        );

                        $save_data_format = array(
                            '%s', //web_site
                            '%s', //email
                            '%s', //phone
                            '%s', //fax
                            '%s', //address
                            '%s', //city
                            '%s', //state
                            '%s', //country
                            '%d', //zip
                            '%f', //lat
                            '%f', //lng
                            '%s', //twitter
                            '%s', //facebook
                            '%s', //googleplus
                            '%s', //linkedin
                            '%s', //rss
                            '%s', //skype
                            '%s', //premium_end
                        );

                        $insert_post_data = array(
                            'post_excerpt' => $p['excerpt'],
                            'post_title' => $p['title'],
                            'post_type' => 'wpecbd',
                            'post_content' => $p['content'],
                            'post_status' => 'pending'
                        );

                        //Prepare Taxonomies
                        if ($this->_options['category'] AND !empty($p['categories'])) {
                            //Check parent categories
                            $all_cats = get_terms('bd_categories', 'hide_empty=0&');
                            $parent_categories = array();
                            if (is_array($all_cats)) {
                                foreach ($all_cats as $all_cat_v) {
                                    if (in_array($all_cat_v->term_id, $p['categories']) AND $all_cat_v->parent != 0) {
                                        $parent_categories[] = $all_cat_v->parent;
                                    }
                                }
                            }
                            $insert_post_data['tax_input']['bd_categories'] = array_merge($p['categories'], $parent_categories);
                        }
                        if ($this->_options['tag'] AND !empty($p['tags'])) {
                            $insert_post_data['tax_input']['bd_tags'] = explode(',', $p['tags']);
                        }
                        //Sure that no error
                        if (empty($erra)) {
                            if (is_numeric($post_id)) {
                                $save_user_control = $wpdb->get_row($wpdb->prepare("SELECT post_id,post_author,post_status FROM " . $wpdb->prefix . "wpecbd INNER JOIN $wpdb->posts ON $wpdb->posts.ID=" . $wpdb->prefix . "wpecbd.post_id  WHERE post_id=%d", $post_id));
                                if (!$save_user_control OR ($user->ID != $save_user_control->post_author)) {
                                    die('<div class="formee-msg-error"><h4>' . __('Something Wrong! Security Check Failed!', 'wpecbd') . '</h4></div>');
                                }
                                $wpdb->update($wpdb->prefix . 'wpecbd', $save_data, array('post_id' => $post_id), $save_data_format, array('%d'));

                                //Check for company previously approved by admin
                                if ($save_user_control->post_status == 'publish' AND get_post_meta($post_id, 'bd_approved', TRUE)) {
                                    $insert_post_data['post_status'] = 'publish';
                                }

                                $insert_post_data['ID'] = $post_id;
                                wp_update_post($insert_post_data);

                                //Update Categories
                                if (!empty($insert_post_data['tax_input']['bd_categories']))
                                    wp_set_post_terms($post_id, $insert_post_data['tax_input']['bd_categories'], 'bd_categories', FALSE);
                                //Update Tags
                                if (!empty($insert_post_data['tax_input']['bd_tags']))
                                    wp_set_post_terms($post_id, $insert_post_data['tax_input']['bd_tags'], 'bd_tags', FALSE);

                                $not_message = __('You have successfully updated your company!', 'wpecbd');
                            } else {
                                //Create New Post
                                $post_id = wp_insert_post($insert_post_data);

                                /* http://core.trac.wordpress.org/ticket/19373 */
                                //Update Categories
                                if (!empty($insert_post_data['tax_input']['bd_categories']))
                                    wp_set_post_terms($post_id, $insert_post_data['tax_input']['bd_categories'], 'bd_categories', FALSE);
                                //Update Tags
                                if (!empty($insert_post_data['tax_input']['bd_tags']))
                                    wp_set_post_terms($post_id, $insert_post_data['tax_input']['bd_tags'], 'bd_tags', FALSE);

                                //Create New Record
                                $save_data['premium_end'] = gmdate('Y-m-d');
                                $save_data = array('post_id' => $post_id) + $save_data;
                                array_unshift($save_data_format, '%d');
                                $wpdb->insert($wpdb->prefix . 'wpecbd', $save_data, $save_data_format);
                                $not_message = __('You have successfully added your company!', 'wpecbd');
                            }
                        }

                        //IMAGE TITLE AND DESC SAVE
                        if (is_numeric($post_id) AND !empty($p['images']) AND is_array($p['images'])) {
                            foreach ($p['images'] as $image_id => $image_title) {
                                $img_post_buff = NULL;
                                if (($img_post_buff = get_post($image_id)) AND $img_post_buff->post_parent == $post_id) {
                                    wp_update_post(array('ID' => $image_id, 'post_title' => $image_title));
                                }
                            }
                        }
                    }

                    //New Post or Edit?
                    if (is_numeric($post_id) AND empty($erra)) {
                        //Edit
                        $post_g = $wpdb->get_row($wpdb->prepare("SELECT $wpdb->posts.*," . $wpdb->prefix . "wpecbd.* FROM $wpdb->posts
                                        INNER JOIN " . $wpdb->prefix . "wpecbd ON $wpdb->posts.ID=" . $wpdb->prefix . "wpecbd.post_id
                                        WHERE post_type='wpecbd' AND post_author=$user->ID AND post_status IN ('pending','publish','trash') AND ID=%d", (int) $post_id));
                        if (!$post_g OR ($user->ID != $post_g->post_author))
                            die('<div class="formee-msg-error"><h4>' . __('Something Wrong! Security Check Failed!', 'wpecbd') . '</h4></div>');
                        $post_array = array(
                            'ID' => $post_g->ID,
                            'post_title' => $post_g->post_title,
                            'post_excerpt' => $post_g->post_excerpt,
                            'post_content' => $post_g->post_content,
                            'web_site' => $post_g->web_site,
                            'email' => $post_g->email,
                            'phone' => $post_g->phone,
                            'fax' => $post_g->fax,
                            'address' => stripslashes($post_g->address),
                            'city' => $post_g->city,
                            'state' => $post_g->state,
                            'country' => $post_g->country,
                            'zip' => ($post_g->zip) ? $post_g->zip : '',
                            'lat' => $post_g->lat,
                            'lng' => $post_g->lng,
                            'twitter' => $post_g->twitter,
                            'facebook' => $post_g->facebook,
                            'googleplus' => $post_g->googleplus,
                            'linkedin' => $post_g->linkedin,
                            'rss' => $post_g->rss,
                            'skype' => $post_g->skype,
                            'categories' => (empty($insert_post_data['tax_input']['bd_categories']) ? array() : $insert_post_data['tax_input']['bd_categories']),
                            'tags' => (empty($insert_post_data['tax_input']['bd_tags']) ? array() : $insert_post_data['tax_input']['bd_tags'])
                        );
                    } elseif (isset($p['post_id'])) {
                        //Returning with error
                        $post_array = array_merge($save_data, array(
                            'post_title' => ((isset($p['title']) ? $p['title'] : '')),
                            'post_excerpt' => ((isset($p['excerpt']) ? $p['excerpt'] : '')),
                            'post_content' => ((isset($p['content']) ? $p['content'] : '')),
                            'lat' => ((isset($p['lat']) ? $p['lat'] : $this->_options['def_lng'])),
                            'lng' => ((isset($p['lng']) ? $p['lng'] : $this->_options['def_lng'])),
                            'ID' => (isset($p['post_id']) AND is_numeric($p['post_id'])) ? $p['post_id'] : 0,
                            'categories' => (empty($insert_post_data['tax_input']['bd_categories']) ? array() : $insert_post_data['tax_input']['bd_categories']),
                            'tags' => (empty($insert_post_data['tax_input']['bd_tags']) ? array() : $insert_post_data['tax_input']['bd_tags'])));
                    } else {
                        //New Post
                        $post_array = array('ID' => 0, 'post_title' => '', 'post_excerpt' => '', 'post_content' => '', 'web_site' => '', 'email' => '', 'phone' => '', 'fax' => '',
                            'address' => '', 'city' => '', 'state' => '', 'country' => '', 'zip' => '', 'lat' => '', 'lng' => '',
                            'twitter' => '', 'facebook' => '', 'googleplus' => '', 'linkedin' => '', 'rss' => '', 'skype' => '',);
                    }

                    $is_new = (is_numeric($post_id)) ? FALSE : TRUE;

                    echo '<form id="bd_user_edit_form" class="formee" action="' . get_bloginfo('wpurl') . '/wpecbd_user/' . (($is_new) ? 'new_company' : $post_array['ID']) . '.html/?bd_action=save&nonce=' . wp_create_nonce('wpecbd_user') . '" method="post">';

                    //Notification Messages
                    if (!empty($not_message)) {
                        echo '<div class="formee-msg-success"><h3>' . $not_message . '</h3></div>';
                    }

                    //Error Messages
                    if (!empty($erra)) {
                        echo '<div class="formee-msg-error"><h3>There are serious errors in your form submission, please see below for details.</h3></div>';
                    }

                    echo '<input name="post_id" type="hidden" value="' . $post_id . '"/>';

                    if (!$is_new)
                        $this->user_logo_images();

                    $this->user_info();

                    if ($this->_options['tag'] OR $this->_options['category'])
                        $this->user_tag_category();

                    if ($this->check_option('map'))
                        $this->user_map();

                    if ($this->_options['social'])
                        $this->user_social();

                    if ($this->_options['captcha'])
                        $this->user_captcha();

                    //Prepare Jquery Validator
                    $validator_data_rules = implode($val_r, ",\n");
                    $validator_data_messages = implode($val_m, ",\n");

                    //Display server side validation results
                    if (!empty($erra)) {
                        foreach ($erra as $f_name => $f_error) {
                            $err_buff[] = '"' . $f_name . '":"' . $f_error . '"';
                        }
                        $ssvm = sprintf('validator.showErrors({%s});', implode(', ', $err_buff));
                    } else {
                        $ssvm = '';
                    }
                    ?>
                    <input class="formee-button right" type="submit" value="<?php _e('Submit', 'wpecbd'); ?>" />
                </form>
            </div>
            <script>
                jQuery(document).ready(function() {
                    var validator=jQuery('#bd_user_edit_form').validate({
                        rules:{<?php echo $validator_data_rules; ?>},
                        messages:{<?php echo $validator_data_messages; ?>}
                    });
            <?php echo $ssvm; ?>
                });
            </script>
            </body>
            </html>
            <?php
        }

        /**
         * Prepares and Prints Logo And Images Section For New/Edit Company Popup Page
         * @global object $post
         * @global boolean $is_new
         * @global array $post_array
         * @return NULL 
         */
        function user_logo_images() {
            global $post, $is_new, $post_array;

            $logo = (!$this->check_option('logo')) ? FALSE : TRUE;
            $images = (!$this->check_option('images')) ? FALSE : TRUE;

            if (!$logo AND !$images)
                return;
            ?>
            <fieldset id="logo_image_fieldset">
                <legend>
                    <?php
                    if ($logo AND $images) {
                        echo __('Logo', 'wpecbd') . ' & ' . __('Images', 'wpecbd');
                    } elseif ($logo AND !$images) {
                        _e('Logo', 'wpecbd');
                    } elseif (!$logo AND $images) {
                        _e('Images', 'wpecbd');
                    }
                    ?>
                </legend>
                <?php
                if ($logo):
                    echo '<div class="grid-12-12">';
                    ?>
                    <fieldset id="logo_fieldset">
                        <legend><?php _e('Logo', 'wpecbd'); ?></legend>
                        <div id="company_logo">

                            <?php
                            if (!$is_new AND ($logo_id = get_post_thumbnail_id($post_array['ID']))) {
                                if (!isset($logo_id->errors)) {
                                    $logo_small = wp_get_attachment_image_src($logo_id, 'bd_logo');
                                    echo '<img id="bd_user_logo_img" src="' . $logo_small[0] . '" />';
                                    echo '<span id="del_com_logo"></span>';
                                    echo '<script>jQuery(document).ready(function() {jQuery("#bd_user_logo_upload").hide();});</script>';
                                }
                            }
                            ?>
                        </div>
                        <div id="bd_ajax_logo_upload" style="display:none;"><strong>Uploading file... Please Wait...</strong></div>
                        <div id="bd_user_logo_upload">

                        </div>

                        <script>
                            jQuery(document).ready(function() {
                                com_logo_uploader({
                                    element:'bd_user_logo_upload',
                                    post_id:<?php echo $post_array['ID']; ?>,
                                    action:'<?php bloginfo('wpurl'); ?>/wpecbd_user_ajax_handler.html/',
                                    nonce_user:'<?php echo wp_create_nonce('wpecbd_user_images'); ?>',
                                    max_size:<?php echo $this->wpecbd_max_upload_size(); ?>,
                                    error:{
                                        type:'<?php _e('{file} has invalid extension. Only {extensions} are allowed.', 'wpecbd'); ?>',
                                        size:'<?php _e('{file} is too large, maximum file size is {sizeLimit}.', 'wpecbd'); ?>',
                                        empty:'<?php _e('{file} is empty, please select files again without it.', 'wpecbd'); ?>',
                                        onleave:'<?php _e('The files are being uploaded, if you leave now the upload will be cancelled.', 'wpecbd'); ?>'
                                    }
                                });
                            });
                        </script>
                    </fieldset>
                    <?php
                    echo '</div>';
                endif;
                ?>

                <?php
                if ($images):
                    echo '<div class="grid-12-12">';
                    ?>
                    <fieldset id="images_fieldset">
                        <legend><?php _e('Images', 'wpecbd'); ?></legend>
                        <div id="bd_ajax_image_upload" style="display:none;"><strong>Uploading file... Please Wait...</strong></div>
                        <div id="company_images">
                            <?php
                            $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post_array['ID']);
                            $attachments = get_posts($args);

                            //Check for logo
                            $post_thumbnail_id = (($post_thumbnail_id = get_post_thumbnail_id($post_array['ID'])) ? $post_thumbnail_id : 0);
                            if ($attachments) {
                                foreach ($attachments as $attachment) {
                                    //Remove logo
                                    if ($attachment->ID == $post_thumbnail_id)
                                        continue;
                                    $img = get_post($attachment->ID);
                                    $src = wp_get_attachment_image_src($attachment->ID);
                                    ?>
                                    <div id="bd_img_cont-<?php echo $attachment->ID; ?>" class="grid-3-12">
                                        <div class="image_div_inner">
                                            <div class="img_div"><img class="bd_img" src="<?php echo $src[0]; ?>" /></div>
                                            <div class="del_com_img"><a href="#" id="bd_del_img-<?php echo $attachment->ID; ?>" attachment_id="<?php echo $attachment->ID; ?>"><?php _e('Delete Image', 'wpecbd'); ?></a></div>
                                            <div class="title_com_img"><a href="#" id="bd_title_img-<?php echo $attachment->ID; ?>" attachment_id="<?php echo $attachment->ID; ?>"><?php _e('Edit Title', 'wpecbd'); ?></a></div>
                                            <input id="img_title-<?php echo $attachment->ID; ?>" value="<?php echo esc_attr($img->post_title); ?>" name="images[<?php echo $attachment->ID; ?>]" type="hidden" />
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <label id="bd_image_reach_limit" class="reach_error" style="display:none;"><?php _e('You have reached the image upload limit! In order to upload new image, please delete some images.', 'wpecbd'); ?></label>
                        <div id="bd_user_image_upload"></div>
                        <script>
                            var image_section_translate={
                                confirm_delete:"<?php _e('Do you really want to delete?', 'wpecbd'); ?>",
                                delete_image:"<?php _e('Delete Image', 'wpecbd'); ?>",
                                edit_title:"<?php _e('Edit Title', 'wpecbd'); ?>"
                            };
                        </script>
                    </fieldset>
                    <?php
                    echo '</div>';
                endif;
                ?>
                <script>
                    jQuery(document).ready(function() {
                        com_image_uploader({
                            element:'bd_user_image_upload',
                            post_id:<?php echo $post_array['ID']; ?>,
                            action:'<?php bloginfo('wpurl'); ?>/wpecbd_user_ajax_handler.html/',
                            nonce_user:'<?php echo wp_create_nonce('wpecbd_user_images'); ?>',
                            max_size:<?php echo $this->wpecbd_max_upload_size(); ?>,
                            error:{
                                type:'<?php _e('{file} has invalid extension. Only {extensions} are allowed.', 'wpecbd'); ?>',
                                size:'<?php _e('{file} is too large, maximum file size is {sizeLimit}.', 'wpecbd'); ?>',
                                empty:'<?php _e('{file} is empty, please select files again without it.', 'wpecbd'); ?>',
                                onleave:'<?php _e('The files are being uploaded, if you leave now the upload will be cancelled.', 'wpecbd'); ?>'
                            }
                        });
            <?php echo ($this->_can_upload_more_image($post_array['ID']) <= 0) ? 'jQuery("#bd_user_image_upload").hide();jQuery("#bd_image_reach_limit").show().delay(9000).fadeTo("slow",0.7);' . "\n" : ''; ?>
                });
                </script>
            </fieldset>
            <?php
        }

        /**
         * Prepares and Prints Company Information Section For New/Edit Company Popup Page
         * @global object $post
         * @global boolean $is_new
         * @global array $post_array
         * @global array $val_r
         * @global array $val_m 
         */
        function user_info() {
            global $post, $is_new, $post_array, $val_r, $val_m;
            ?>
            <fieldset>
                <legend><?php _e('General Info', 'wpecbd'); ?></legend>
                <?php
                $val_r[] = 'title: {required: true}';
                $val_m[] = 'title: {required:"' . __('Please enter your company name', 'wpecbd') . '"}';
                ?>
                <div class="grid-12-12">
                    <label><?php _e('Company Name', 'wpecbd'); ?> <em class="formee-req">*</em></label>
                    <input id="bd_title" class="formee-medium" name="title" type="text" value="<?php echo $post_array['post_title']; ?>" />
                </div>
                <?php
                if ($this->check_option('website')):
                    $val_r[] = 'web_site: {url:true ,required: ' . (($this->_options['r_website']) ? 'true' : 'false') . '}';
                    $val_m[] = 'web_site: {required:"' . __('Please enter your company web site', 'wpecbd') . '",url:"' . __('Please enter a valid web site url', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Web Site', 'wpecbd'); ?> <?php echo ($this->_options['r_website']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_website" class="formee-medium" name="web_site" type="text" value="<?php echo $post_array['web_site']; ?>" />
                    </div>
                    <?php
                endif;
                if ($this->check_option('email')):
                    $val_r[] = 'email: {email:true, required: ' . (($this->_options['r_email']) ? 'true' : 'false') . '}';
                    $val_m[] = 'email: {required:"' . __('Please enter your company e-mail address', 'wpecbd') . '",email:"' . __('Please enter a valid email address', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('eMail', 'wpecbd'); ?> <?php echo ($this->_options['r_email']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_email" class="formee-medium" name="email" type="text" value="<?php echo $post_array['email']; ?>" />
                    </div>
                    <?php
                endif;
                if ($this->check_option('phone')):
                    $val_r[] = 'phone: {required: ' . (($this->_options['r_phone']) ? 'true' : 'false') . '}';
                    $val_m[] = 'phone: {required:"' . __('Please enter your company phone number', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Telephone', 'wpecbd'); ?> <?php echo ($this->_options['r_phone']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_phone" class="formee-small" name="phone" id="phone" type="text" value="<?php echo $post_array['phone']; ?>" />
                    </div>
                    <?php
                endif;
                if ($this->check_option('fax')):
                    $val_r[] = 'fax: {required: ' . (($this->_options['r_fax']) ? 'true' : 'false') . '}';
                    $val_m[] = 'fax: {required:"' . __('Please enter your company fax number', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Fax', 'wpecbd'); ?> <?php echo ($this->_options['r_fax']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_fax" class="formee-small" name="fax" type="text" value="<?php echo $post_array['fax']; ?>" />
                    </div><div class="clear"/>
                    <?php
                endif;
                if ($this->check_option('address')):
                    $val_r[] = 'address: {minlength: 10,required: ' . (($this->_options['r_address']) ? 'true' : 'false') . '}';
                    $val_m[] = 'address: {required:"' . __('Please enter your company address', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Address', 'wpecbd'); ?> <?php echo ($this->_options['r_address']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <textarea id="bd_address" class="formee-medium" name="address" rows="2"><?php echo $post_array['address']; ?></textarea>
                    </div>
                    <?php
                endif;
                if ($this->check_option('country')):
                    $val_r[] = 'country: {country: ' . (($this->_options['r_country']) ? 'true' : 'false') . '}';
                    $val_m[] = 'country: {country:"' . __('Please select your country', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Country', 'wpecbd'); ?> <?php echo ($this->_options['r_country']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <select id="bd_country" name="country" class="formee-small" >
                            <?php echo $this->country_selector($post_array['country']); ?>
                        </select>
                    </div>
                    <?php
                endif;
                if ($this->check_option('state')):
                    $val_r[] = 'state: {state:' . (($this->_options['r_state']) ? 'true' : 'false') . '}';
                    $val_m[] = 'state: {state:"' . __('Please enter your state', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-4-12">
                        <label><?php _e('State', 'wpecbd'); ?> <?php echo ($this->_options['r_state']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_state" name="state" type="text" value="<?php echo $post_array['state']; ?>" />
                    </div>
                    <?php
                endif;
                if ($this->check_option('city')):
                    $val_r[] = 'city: {city: ' . (($this->_options['r_city']) ? 'true' : 'false') . '}';
                    $val_m[] = 'city: {city:"' . __('Please enter your city', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-4-12">
                        <label><?php _e('City', 'wpecbd'); ?> <?php echo ($this->_options['r_city']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_city" name="city" type="text" value="<?php echo $post_array['city']; ?>" />
                    </div>
                    <?php
                endif;
                if ($this->check_option('zip')):
                    $val_r[] = 'zip: {required: ' . (($this->_options['r_zip']) ? 'true' : 'false') . '}';
                    $val_m[] = 'zip: {required:"' . __('Please enter your zip code', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-4-12">
                        <label><?php _e('Zip', 'wpecbd'); ?> <?php echo ($this->_options['r_zip']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <input id="bd_zip" name="zip" type="text" value="<?php echo $post_array['zip']; ?>" />
                    </div>
                <?php endif; ?>
                <div class="clear"></div>
                <?php
                if ($this->check_option('shortd')):
                    $val_r[] = 'excerpt: {maxlength:' . $this->_options['shortd_max'] . ',minlength: ' . $this->_options['shortd_min'] . ',required: ' . (($this->_options['r_shortd']) ? 'true' : 'false') . '}';
                    $val_m[] = 'excerpt: {required:"' . __('Please enter your company short description', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Short Information', 'wpecbd'); ?> <?php echo ($this->_options['r_shortd']) ? '<em class="formee-req">*</em>' : ''; ?></label>
                        <textarea id="bd_shortd" name="excerpt" name="excerpt" rows="4"><?php echo $post_array['post_excerpt']; ?></textarea>
                    </div>
                    <?php
                endif;
                if ($this->check_option('longd')):
                    $val_r[] = 'content: {maxlength:' . $this->_options['longd_max'] . ',minlength: ' . $this->_options['longd_min'] . ',required: true}';
                    $val_m[] = 'content: {required:"' . __('Please enter your company description', 'wpecbd') . '"}';
                    ?>
                    <div class="grid-12-12">
                        <label><?php _e('Long Information', 'wpecbd'); ?> <em class="formee-req">*</em></label>
                        <textarea id="bd_longd" name="content" name="content" rows="6"><?php echo $post_array['post_content']; ?></textarea>
                    </div>
                <?php endif; ?>
            </fieldset>
            <?php
            if ($this->_options['mask_actv']) {
                echo '<script>';
                if (!empty($this->_options['mask_tel'])) {
                    echo 'jQuery("#bd_phone").mask("' . $this->_options['mask_tel'] . '");';
                }
                if (!empty($this->_options['mask_fax'])) {
                    echo 'jQuery("#bd_fax").mask("' . $this->_options['mask_fax'] . '");';
                }
            }
            echo '</script>';
            ?>
            <?php
            if ($this->_options['auto_locality'] AND $this->check_option('country')) {
                ?>
                <script src="<?php echo WPECBD_PLUGIN_URL . 'js/auto_locality.js'; ?>"></script>
                <?php
            }
        }

        /**
         * Prepares and Prints Tag and Category Section For New/Edit Company Popup Page
         * @global object $post
         * @global boolean $is_new
         * @global array $post_array
         * @global array $val_r
         * @global array $val_m 
         */
        function user_tag_category() {
            global $post, $is_new, $post_array, $val_r, $val_m;

            $terms_args = array('hide_empty' => FALSE);
            $all_tags = array();
            $all_categories = array();
            foreach (get_terms(array('bd_categories', 'bd_tags'), $terms_args) as $v1) {
                if ($v1->taxonomy == 'bd_tags') {
                    $all_tags[] = "'" . str_replace("'", "\'", $v1->name) . "'";
                } elseif ($v1->taxonomy == 'bd_categories') {
                    $all_categories[] = $v1;
                }
            }

            //Organize Categories
            if (!empty($all_categories)) {
                $cat_buff = array();
                foreach ($all_categories as $cat1 => $cat2) {
                    if ($cat2->parent == 0) {
                        $cat_buff[$cat2->term_id]['name'] = $cat2->name;
                    } else {
                        $cat_buff[$cat2->parent]['child'][$cat2->term_id] = $cat2->name;
                    }
                }
            }

            echo '<fieldset> <legend>' . (($this->_options['tag']) ? (($this->_options['category']) ? __('Categories', 'wpecbd') . ' & ' . __('Tags', 'wpecbd') : __('Tags', 'wpecbd')) : __('Categories', 'wpecbd')) . '</legend>';

            //Check Company Tags
            if ($this->_options['tag']):

                if (!empty($post_array['tags'])) {
                    $tags = implode(',', $post_array['tags']);
                } elseif (!$is_new AND ($tags_get = get_the_terms($post_array['ID'], 'bd_tags')) AND !is_wp_error($tags_get)) {
                    $tag_buff = array();
                    foreach ($tags_get as $v_t) {
                        $tag_buff[] = $v_t->name;
                    }
                    $tags = implode(',', $tag_buff);
                } else {
                    $tags = '';
                }
                ?>
                <div class="grid-12-12">
                    <label><?php _e('Tags', 'wpecbd'); ?> <em class="formee-req">*</em></label>
                    <input name="tags" id="tags" type="text" value="<?php echo $tags; ?>" />
                    <div id="tag_err"></div>
                </div>
                <script>
                    jQuery(document).ready(function() {
                        com_tags({
                            max_tag:<?php echo $this->_options['max_tag']; ?>,
                            tranlate:{max_tag:"<?php _e('You have reached the limit for adding tag', 'wpecbd'); ?>"},
                            tags:<?php echo '[' . ((empty($all_tags) ? "''" : implode(', ', $all_tags))) . ']'; ?>
                        });
                    });
                </script>
                <?php
            endif;

            //Check Company Categories
            if ($this->_options['category']):
                if (!empty($post_array['categories'])) {
                    $categories = $post_array['categories'];
                } elseif (!$is_new AND ($categories_get = get_the_terms($post_array['ID'], 'bd_categories')) AND !is_wp_error($categories_get)) {
                    $cat_buff2 = array();
                    foreach ($categories_get as $v_c) {
                        $categories[] = $v_c->term_id;
                    }
                } else {
                    $categories = array();
                }
                ?>
                <div class="grid-12-12">
                    <label><?php _e('Categories', 'wpecbd'); ?> <em class="formee-req">*</em></label>
                    <select name="categories[]" id="categories" multiple="multiple">
                        <?php
                        foreach ($cat_buff as $catea1 => $catea2) {
                            echo '<optgroup label="' . $catea2['name'] . '">';
                            if (empty($catea2['child'])) {
                                echo '<option value="' . $catea1 . '" ' . ((in_array($catea1, $categories)) ? 'selected="selected"' : '') . '>' . $catea2['name'] . '</option>';
                            } else {
                                foreach ($catea2['child'] as $cateb1 => $cateb2) {
                                    echo '<option value="' . $cateb1 . '" ' . ((in_array($cateb1, $categories)) ? 'selected="selected"' : '') . '>' . $cateb2 . '</option>';
                                }
                            }
                            echo '</optgroup>';
                        }
                        ?>
                    </select>
                    <div id="cat_err"></div>
                </div>
                <script>
                    jQuery(document).ready(function() {
                        com_category({
                            max_cat:<?php echo $this->_options['max_cat']; ?>,
                            translate:{
                                limit:"<?php _e('You have reached the limit for choosing category', 'wpecbd'); ?>",
                                noneselected:"<?php _e('Select Categories', 'wpecbd'); ?>",
                                selectedtext:"<?php _e('# selected', 'wpecbd'); ?>"
                            }
                        });
                    });
                </script>
                <?php
            endif;
            echo '</fieldset>';
        }

        /**
         * Prepares and Prints Map Section For New/Edit Company Popup Page
         * @global object $post
         * @global boolean $is_new
         * @global array $post_array
         * @global array $val_r
         * @global array $val_m 
         */
        function user_map() {
            global $post, $is_new, $post_array, $val_r, $val_m;

            if ($is_new AND empty($post_array['lat'])) {
                $lat = (string) $this->_options['def_lat'];
                $lng = (string) $this->_options['def_lng'];
            } else {
                $lat = (string) $post_array['lat'];
                $lng = (string) $post_array['lng'];
            }
            ?>
            <fieldset>
                <legend><?php _e('Map', 'wpecbd'); ?></legend>
                <script>
                    var validate_map_lat=<?php echo (string) $this->_options['def_lat']; ?>;
                    jQuery(function() {
                        var addresspickerMap = jQuery( "#addresspicker_map" ).addresspicker({
                            elements: {
                                map:      "#bd_user_map",
                                lat:      "#bd_user_lat",
                                lng:      "#bd_user_lng"
                            },
                            mapOptions: {
                                zoom: <?php echo $this->_options['zoom_edit']; ?>,
                                center: new google.maps.LatLng(<?php echo $lat . ', ' . $lng; ?>),
                                scrollwheel: false,
                                mapTypeId: google.maps.MapTypeId.<?php echo $this->_options['maptype_edit']; ?>
                            }
                        });
                        var gmarker = addresspickerMap.addresspicker( "marker");
                        gmarker.setVisible(true);
                        addresspickerMap.addresspicker( "updatePosition");

                    });
                </script>
                <div class="grid-12-12">
                    <input id="addresspicker_map" name="map_search" type="text" onclick="this.value='';" value="<?php _e('Search', 'wpecbd'); ?>" />
                </div>
                <div class="grid-12-12">
                    <div id="bd_user_map"></div>
                </div>
                <input id="bd_user_lat" name="lat" type="hidden" />
                <input id="bd_user_lng" name="lng" type="hidden" />
            </fieldset>
            <?php
            if ($this->_options['r_map']) {
                $val_r[] = 'map_search: {map:true}';
                $val_m[] = 'map_search: {map:"' . __('Please select your company location on the map', 'wpecbd') . '"}';
            }
        }

        /**
         * Prepares and Prints Social Accounts Section For New/Edit Company Popup Page
         * @global object $post
         * @global boolean $is_new
         * @global array $post_array
         * @global array $val_r
         * @global array $val_m 
         */
        function user_social() {
            global $post, $is_new, $post_array, $val_r, $val_m;

            echo '<fieldset>
                <legend>' . __('Social Accounts', 'wpecbd') . '</legend>';

            $pattern = '<div class="grid-6-12"><label>%s %s</label><input type="text" name="%s" value="%s"></div>';

            if ($this->check_option('twitter')) {
                $val_r[] = 'twitter: {required: ' . (($this->_options['r_twitter']) ? 'true' : 'false') . ',maxlength:15}';
                $val_m[] = 'twitter: {required:"' . __('Please enter your company twitter username', 'wpecbd') . '"}';
                printf($pattern, __('Twitter UserName', 'wpecbd'), (($this->_options['r_twitter']) ? '<em class="formee-req">*</em>' : ''), 'twitter', ((!empty($post_array['twitter'])) ? $post_array['twitter'] : ''));
            }

            if ($this->check_option('facebook')) {
                $val_r[] = 'facebook: {url:true, required: ' . (($this->_options['r_facebook']) ? 'true' : 'false') . ', maxlength:255}';
                $val_m[] = 'facebook: {required:"' . __('Please enter your company facebook url', 'wpecbd') . '",url:"' . __('Please enter a valid facebook url', 'wpecbd') . '"}';
                printf($pattern, __('Facebook Url', 'wpecbd'), (($this->_options['r_facebook']) ? '<em class="formee-req">*</em>' : ''), 'facebook', ((!empty($post_array['facebook'])) ? $post_array['facebook'] : ''));
            }

            if ($this->check_option('googleplus')) {
                $val_r[] = 'googleplus: {url:true, required: ' . (($this->_options['r_googleplus']) ? 'true' : 'false') . ', maxlength:255}';
                $val_m[] = 'googleplus: {required:"' . __('Please enter your company googleplus url', 'wpecbd') . '",url:"' . __('Please enter a valid googleplus url', 'wpecbd') . '"}';
                printf($pattern, __('GooglePlus Url', 'wpecbd'), (($this->_options['r_googleplus']) ? '<em class="formee-req">*</em>' : ''), 'googleplus', ((!empty($post_array['googleplus'])) ? $post_array['googleplus'] : ''));
            }

            if ($this->check_option('linkedin')) {
                $val_r[] = 'linkedin: {url:true, required: ' . (($this->_options['r_linkedin']) ? 'true' : 'false') . ', maxlength:255}';
                $val_m[] = 'linkedin: {required:"' . __('Please enter your company linkedin url', 'wpecbd') . '",url:"' . __('Please enter a valid linkedin url', 'wpecbd') . '"}';
                printf($pattern, __('LinkedIn Url', 'wpecbd'), (($this->_options['r_linkedin']) ? '<em class="formee-req">*</em>' : ''), 'linkedin', ((!empty($post_array['linkedin'])) ? $post_array['linkedin'] : ''));
            }

            if ($this->check_option('rss')) {
                $val_r[] = 'rss: {required: ' . (($this->_options['r_rss']) ? 'true' : 'false') . ', maxlength:255}';
                $val_m[] = 'rss: {required:"' . __('Please enter your company rss url', 'wpecbd') . '"}';
                printf($pattern, __('Rss Url', 'wpecbd'), (($this->_options['r_rss']) ? '<em class="formee-req">*</em>' : ''), 'rss', ((!empty($post_array['rss'])) ? $post_array['rss'] : ''));
            }

            if ($this->check_option('skype')) {
                $val_r[] = 'skype: {required: ' . (($this->_options['r_skype']) ? 'true' : 'false') . ',maxlength:32, minlength:6}';
                $val_m[] = 'skype: {required:"' . __('Please enter your skype username', 'wpecbd') . '"}';
                printf($pattern, __('Skype UserName', 'wpecbd'), (($this->_options['r_skype']) ? '<em class="formee-req">*</em>' : ''), 'skype', ((!empty($post_array['skype'])) ? $post_array['skype'] : ''));
            }
            echo '</fieldset>';
        }

        /**
         * Prepares and Prints Captcha Section For New/Edit Company Popup Page
         * @global array $val_r
         * @global array $val_m 
         */
        function user_captcha() {
            global $val_r, $val_m;
            $val_r[] = 'recaptcha_response_field: {required: true}';
            $val_m[] = 'recaptcha_response_field: {required:"' . __('Please enter Captcha', 'wpecbd') . '"}';

            echo '<fieldset>
                <legend>' . __('Captcha', 'wpecbd') . '</legend>';

            if ($this->_options['captcha'] == 2) {
                require_once(WPECBD_PLUGIN_DIR . 'inc/recaptchalib.php');
                echo '<div class="grid-7-12 right">';
                echo recaptcha_get_html($this->_options['captcha_public_key']);
                echo '</div>';
            } else {
                $url = 'http://www.google.com/recaptcha/api/challenge?k=' . $this->_options['captcha_public_key'] . '&ajax=1';
                $str = file_get_contents($url);
                $left = "challenge : '";
                $right = "'";

                $str = substr(stristr($str, $left), strlen($left));
                $leftLen = strlen(stristr($str, $right));
                $leftLen = $leftLen ? - ($leftLen) : strlen($str);
                $c = substr($str, 0, $leftLen);

                $img_url = $this->http_protocol() . 'www.google.com/recaptcha/api/image?c=' . $c;
                ?>
                <div class="grid-6-12 left"><img class="captcha_img" src="<?php echo $img_url; ?>"></img></div>
                <div class="grid-6-12 captcha_str right">
                    <label id="captcha_label" for="captcha_response"><?php _e('Enter the code shown on the left', 'wpecbd'); ?> <em class="formee-req">*</em></label>
                    <input name="recaptcha_response_field" id="captcha_response" type="text" value="" /></div>
                <input name="recaptcha_challenge_field" type="hidden" value="<?php echo $c; ?>" />
                <?php
            }
            echo '</fieldset>';
        }

        /**
         * Prepares VCARD
         * @global object $wpdb
         * @global object $post
         * @param string $post_name
         * @return mixed 
         */
        function prepare_vcard($post_name = NULL) {
            if ($post_name) {
                global $wpdb;
                $post = $wpdb->get_row("SELECT " . $wpdb->prefix . "wpecbd.*, " . $wpdb->posts . ".* FROM " . $wpdb->posts . " INNER JOIN " . $wpdb->prefix . "wpecbd ON " . $wpdb->posts . ".ID=" . $wpdb->prefix . "wpecbd.post_id  WHERE post_name='" . $post_name . "'");
            } else {
                global $post;
            }
            /*
              See : http://tools.ietf.org/html/rfc2426.html
             */
            $l = "\r\n";
            $vc = '';
            $vc.='BEGIN:VCARD' . $l;
            $vc.='VERSION:3.0' . $l;
            $vc.='N:;;;;' . $l;

            //NAME
            $title = stripslashes($post->post_title);
            $vc.='ORG:' . $title . $l;
            $vc.='FN:' . $title . $l;

            //LOGO
            if (has_post_thumbnail()) {
                $logo_id = get_post_thumbnail_id($post->ID);
                $logo_url = wp_get_attachment_image_src($logo_id, 'thumbnail');
                $vc.='LOGO;VALUE=uri:' . $logo_url[0] . $l;
            }

            //TELEPHONE
            if (!empty($post->phone)) {
                $vc.='TEL;TYPE=work,voice,pref:' . $post->phone . $l;
            }

            //FAX
            if (!empty($post->fax)) {
                $vc.='TEL;TYPE=work,fax:' . $post->phone . $l;
            }

            //EMAIL
            if (!empty($post->email)) {
                $vc.='EMAIL;TYPE=internet,pref:' . $post->email . $l;
            }

            //ADDRESS
            if (!empty($post->address)) {
                $address_buff = '';
                $address_buff.=$post->address;
                $address_buff.=(!empty($post->city)) ? ";" . $post->city : '';
                $address_buff.=(!empty($post->state)) ? ';' . $post->state : '';
                $address_buff.=(!empty($post->country)) ? ';' . $post->country : '';
                $vc.='ADR;type=work:;;' . stripslashes($address_buff) . $l;
            }

            //URL
            if (!empty($post->web_site)) {
                $vc.='URL:' . esc_url($post->web_site) . $l;
            }

            //MAP
            if (!empty($post->lat)) {
                $vc.='GEO:' . $post->lat . ';' . $post->lng . $l;
            }

            $vc.='REV:' . date(DATE_ISO8601) . $l;
            $vc.='CLASS:PUBLIC' . $l;
            $vc.='END:VCARD' . $l;
            if ($post_name) {
                header('Content-Disposition: attachment; filename="' . $post_name . '.vcf"');
                header('Content-Type: text/x-vcard; charset=utf-8; name="' . $post_name . '.vcf"');
                echo $vc;
            } else {
                return $vc;
            }
        }

        function content_filter($content) {
            global $post;
            if (!$this->_options['sql_fix']) {
                global $wpdb;
                $post = $wpdb->get_row("SELECT " . $wpdb->prefix . "wpecbd.*, " . $wpdb->posts . ".* FROM " . $wpdb->posts . " INNER JOIN " . $wpdb->prefix . "wpecbd ON " . $wpdb->posts . ".ID=" . $wpdb->prefix . "wpecbd.post_id  WHERE ID=" . $post->ID);
            }

            //Check For Premium
            $GLOBALS['wpecbd_is_premium'] = $this->is_premium();

            if ((is_category() || is_archive() || is_search()) AND !is_single() AND (isset($post) AND $post->post_type == 'wpecbd')) {
                //INLOOP
                return $this->render_template(get_option('wpecbd_tpl_inloop', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/inloop.html')), $content);
            } elseif (!is_singular('wpecbd')) {
                //NOT COMPANY, PASS!
                return $content;
            } else {
                //SINGLE PAGE
                return $this->render_template(get_option('wpecbd_tpl_singlepage', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/singlepage.html')), $content);
            }
        }

        function feed_content_filter($content) {
            global $post;

            if (!$this->_options['sql_fix']) {
                global $wpdb;
                $post = $wpdb->get_row("SELECT " . $wpdb->prefix . "wpecbd.*, " . $wpdb->posts . ".* FROM " . $wpdb->posts . " INNER JOIN " . $wpdb->prefix . "wpecbd ON " . $wpdb->posts . ".ID=" . $wpdb->prefix . "wpecbd.post_id  WHERE ID=" . $post->ID);
            }

            if (!isset($post) OR $post->post_type != 'wpecbd')
                return $content;
            return $this->render_template(get_option('wpecbd_tpl_feed', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/feed.html')), $content);
        }

        function render_template($template, $content) {
            global $post;

            //Define Logo
            $GLOBALS['wpecbd_logo_id'] = get_post_thumbnail_id($post->ID);
            $GLOBALS['wpecbd_have_logo'] = (is_wp_error($GLOBALS['wpecbd_logo_id']) OR empty($GLOBALS['wpecbd_logo_id'])) ? FALSE : TRUE;
            if ($GLOBALS['wpecbd_have_logo']) {
                $logo = array(
                    'large' => wp_get_attachment_image_src($GLOBALS['wpecbd_logo_id'], 'large'),
                    'page' => wp_get_attachment_image_src($GLOBALS['wpecbd_logo_id'], 'bd_logo'),
                    'inloop' => wp_get_attachment_image_src($GLOBALS['wpecbd_logo_id'], 'bd_logo_inloop')
                );
            }

            $variables = array(
                '%%content%%' => $content,
                '%%insite_url%%' => get_permalink(),
                '%%company_inloop_logo_url%%' => ($GLOBALS['wpecbd_have_logo']) ? $logo['inloop'][0] : '',
                '%%default_inloop_logo_url%%' => '', //BUNU SIL ISTEYEN KENDI YAZABILIR
                '%%company_id%%' => $post->ID,
                '%%company_name%%' => get_the_title(),
                '%%company_excerpt%%' => ((has_excerpt()) ? $post->post_excerpt . '...' : $this->custom_excerpt($post->post_content)),
                '%%company_short_description%%' => $post->post_excerpt,
                '%%company_long_description%%' => $post->post_content,
                '%%company_big_logo_url%%' => ($GLOBALS['wpecbd_have_logo']) ? $logo['large'][0] : '',
                '%%company_page_logo_url%%' => ($GLOBALS['wpecbd_have_logo']) ? $logo['page'][0] : '',
                '%%company_website%%' => $post->web_site,
                '%%company_email%%' => $post->email,
                '%%company_phone%%' => $post->phone,
                '%%company_fax%%' => $post->fax,
                '%%company_address%%' => $post->address,
                '%%company_city%%' => $post->city,
                '%%company_state%%' => $post->state,
                '%%company_country%%' => $post->country,
                '%%company_zip%%' => $post->zip,
                '%%company_map_popup_url%%' => get_bloginfo('wpurl') . '/wpecbd_big_map_popup/' . $post->ID . '.html/',
                '%%company_lat%%' => $post->lat,
                '%%company_lng%%' => $post->lng,
                '%%qr_width%%' => $this->_options['qr_w'],
                '%%qr_height%%' => $this->_options['qr_h'],
                '%%qr_data%%' => $this->get_qr_data(),
                '%%map_zoom%%' => $this->_options['zoom_front'],
                '%%map_type%%' => $this->_options['maptype_front'],
                '%%map_width%%' => $this->_options['map_img_w'],
                '%%map_height%%' => $this->_options['map_img_h'],
            );

            $if_tags = array(
                'is_premium',
                'logo',
                'images',
                'website',
                'email',
                'phone',
                'fax',
                'city',
                'state',
                'country',
                'zip',
                'social',
                'map',
                'qr',
                'shortd'
            );

            $if_pattern = '#\{\{if\:(' . implode('|', $if_tags) . ')\}\}(.*)\{\{endif\}\}#sU';
            //Proccess If Conditions
            $template = preg_replace_callback($if_pattern, array(&$this, 'render_template_ifcheck'), $template);
            //Place Variables
            $template = str_ireplace(array_keys($variables), array_values($variables), $template);

            //Proccess Macros
            $macro_tags = array('social', 'image');
            $macro_pattern = '#\{\{macro\:(' . implode('|', $macro_tags) . ')\}\}(.*)\{\{endmacro\}\}#sU';
            $template = preg_replace_callback($macro_pattern, array(&$this, 'render_template_macrocheck'), $template);
            return $template;
        }

        function render_template_ifcheck($v) {
            global $post, $is_premium;
            $is_premium = $GLOBALS['wpecbd_is_premium'];

            switch ($v[1]) {
                case 'is_premium':
                    return ($is_premium) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);

                case 'logo':
                    return ($this->check_option('logo') AND has_post_thumbnail()) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);

                case 'images':
                    if (!$this->check_option('images'))
                        return $this->render_template_elsecheck($v[2], TRUE);

                    //Get Images
                    $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID);
                    $attachments = get_posts($args);
                    if (is_wp_error($attachments) OR empty($attachments))
                        return $this->render_template_elsecheck($v[2], TRUE);

                    return (count($attachments) > (($GLOBALS['wpecbd_have_logo']) ? 1 : 0)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);

                case 'website':
                    return ($this->check_option('website') AND !empty($post->web_site)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'email':
                    return ($this->check_option('email') AND !empty($post->email)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'phone':
                    return ($this->check_option('phone') AND !empty($post->phone)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'fax':
                    return ($this->check_option('fax') AND !empty($post->fax)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'city':
                    return (!empty($post->city)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'state':
                    return (!empty($post->state)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'country':
                    return (!empty($post->country)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'zip':
                    return (!empty($post->zip)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'social':
                    return ($this->_options['social']) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'map':
                    return ($this->check_option('map') AND !empty($post->lng)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'qr':
                    return ($this->_options['qr']) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                case 'shortd':
                    return ($this->check_option('shortd') AND !empty($post->post_excerpt)) ? $this->render_template_elsecheck($v[2]) : $this->render_template_elsecheck($v[2], TRUE);
                default:
                    return NULL;
            }
        }

        function render_template_elsecheck($v, $else = FALSE) {

            if (stripos($v, '{{else}}') === FALSE) {
                return ($else) ? '' : $v;
            } else {
                $v_parts = explode('{{else}}', $v);
                return ($else) ? $v_parts[1] : $v_parts[0];
            }
        }

        function render_template_macrocheck($v) {
            global $post, $is_premium;
            $is_premium = $GLOBALS['wpecbd_is_premium'];

            switch ($v[1]) {
                case 'image':
                    $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID);
                    $attachments = get_posts($args);
                    if (is_wp_error($attachments) OR empty($attachments))
                        return NULL;
                    $img_out = '';
                    foreach ($attachments as $attachment) {
                        if ($GLOBALS['wpecbd_have_logo'] AND $attachment->ID == $GLOBALS['wpecbd_logo_id']) {
                            continue;
                        }
                        $thumb = wp_get_attachment_image_src($attachment->ID, 'bd_thumb');
                        $img = wp_get_attachment_image_src($attachment->ID, 'bd_img');

                        $img_replace = array(
                            '%%img_thumb_url%%' => $thumb[0],
                            '%%img_large_url%%' => $img[0],
                            '%%img_desc%%' => esc_attr($attachment->post_title),
                            '%%img_id%%' => $attachment->ID
                        );
                        $img_out.= str_ireplace(array_keys($img_replace), array_values($img_replace), $v[2]);
                    }
                    return $img_out;
                case 'social':
                    $buff = array();
                    $buff['Twitter'] = ($this->check_option('twitter') AND !empty($post->twitter)) ? $post->twitter : NULL;
                    $buff['Facebook'] = ($this->check_option('facebook') AND !empty($post->facebook)) ? $post->facebook : NULL;
                    $buff['GooglePlus'] = ($this->check_option('googleplus') AND !empty($post->googleplus)) ? $post->googleplus : NULL;
                    $buff['LinkedIn'] = ($this->check_option('linkedin') AND !empty($post->linkedin)) ? $post->linkedin : NULL;
                    $buff['Rss'] = ($this->check_option('rss') AND !empty($post->rss)) ? $post->rss : NULL;
                    $buff['Skype'] = ($this->check_option('skype') AND !empty($post->skype)) ? $post->skype : NULL;

                    $valid_counter = 0;
                    foreach ($buff as $c) {
                        if (!is_null($c))
                            $valid_counter++;
                    }
                    if ($valid_counter < 1)
                        return NULL;

                    $social_out = '';

                    foreach ($buff as $social_k => $social_v) {
                        if (empty($social_v))
                            continue;
                        $social_replace = array(
                            '%%social_title%%' => $social_k,
                            '%%social_url%%' => $social_v,
                            '%%social_title_class%%' => strtolower($social_k)
                        );
                        if ($social_k == 'Twitter') {
                            $social_replace['%%social_url%%'] = 'http://twitter.com/' . $social_v;
                        } elseif ($social_k == 'Skype') {
                            $social_replace['%%social_url%%'] = 'skype:' . $social_v;
                        }
                        $social_out .= str_ireplace(array_keys($social_replace), array_values($social_replace), $v[2]);
                    }

                    return $social_out;
                default:
                    return NULL;
            }
        }

        /**
         * Popup Map iframe, it open when clicked map where company page
         * @global object $wpdb
         * @param int $id Post ID
         */
        function frontend_big_map_popup($id) {
            global $wpdb;
            if (!is_numeric($id) OR !($com = $wpdb->get_row($wpdb->prepare("SELECT " . $wpdb->prefix . "wpecbd.*, $wpdb->posts.* FROM $wpdb->posts INNER JOIN " . $wpdb->prefix . "wpecbd ON $wpdb->posts.ID=" . $wpdb->prefix . "wpecbd.post_id  WHERE ID=%d", $id)))) {
                exit('ERROR!');
            }
            //Prepare Adress2
            $buff_add2 = array();
            if (!empty($com->city))
                $buff_add2[] = $com->city;
            if (!empty($com->state))
                $buff_add2[] = $com->state;
            if (!empty($com->zip))
                $buff_add2[] = $com->zip;
            if (!empty($com->country))
                $buff_add2[] = $com->country;

            require_once(WPECBD_PLUGIN_DIR . 'inc/views/frontend_map_popup.php');
        }

        /**
         * Returns frontend comapny qrcode
         * @global object $post
         * @return string 
         */
        function get_qr_data() {
            global $post;
            switch ($this->_options['qr_type']) {
                case 'vcard-download':
                    $data = urlencode(get_bloginfo('wpurl') . '/wpecbd_get_vcard/' . $post->post_name . '.vcf');
                    break;
                case 'vcard-embed':
                    $data = urlencode($this->prepare_vcard());
                    break;
                case 'url':
                    $data = get_permalink();
                    break;
                default:
                    return;
            }
            return $data;
        }

        /**
         * Custom Excerpt function for our short description
         * @param type $excerpt
         * @return type 
         */
        function custom_excerpt($excerpt) {
            $out = '';
            if (mb_strlen($excerpt) > $this->_options['shortd_max']) {
                $subex = mb_substr($excerpt, 0, $this->_options['shortd_max'] - 3);
                $exwords = explode(' ', $subex);
                $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
                if ($excut < 0) {
                    $out.= mb_substr($subex, 0, $excut);
                } else {
                    $out.= $subex;
                }
                $out.= '...';
            } else {
                $out.= $excerpt;
            }
            return $out;
        }

        /**
         * Handle ajax queries, not images
         * @global object $wpdb
         * @global string $premium_end
         * @param string $w Action
         */
        function widget_ajax($w) {
            header("Expires: Sun, 19 Nov 1978 05:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            switch ($w):
                case 'search':
                    require_once(WPECBD_PLUGIN_DIR . 'inc/views/search_popup.php');
                    break;

                case 'ajax_search':
                    if (isset($_POST['is_bd_search'])):

                        $results = $this->custom_map_searcher(@$_POST['lat'], @$_POST['lng'], @$_POST['cats'], @$_POST['term'], @$_POST['distance'], @$_POST['unit']);
                        if (is_null($results)) {
                            die(json_encode(array('error' => 'no_found')));
                        } elseif ($results === FALSE) {
                            die(json_encode(array('error' => true)));
                        } else {
                            die(json_encode($results));
                        }
                    else:
                        echo json_encode(array('error' => true));
                    endif;
                    break;

            endswitch;
            die();
        }

        function custom_map_searcher($lat = NULL, $lng = NULL, $categories = array(), $term = NULL, $distance = 500, $unit = 'km', $include = array(), $exclude = array()) {
            global $wpdb, $premium_end;
            $query_r = array();
            $included = FALSE;
            if (!empty($lat) AND !empty($lng) AND is_numeric($lat) AND is_numeric($lng)) {

                $unit_v = (isset($unit)) ? (($unit == 'km') ? 6371 : 3959) : 6371;
                $distance_v = (isset($distance) AND is_numeric($distance) AND $distance < 1001) ? $distance : 1000;
                $query_r_2 = $wpdb->get_results($wpdb->prepare("SELECT post_id,lat,lng, ( %d * acos( cos( radians(%f) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(%f) ) + sin( radians(%f) ) * sin( radians( lat ) ) ) ) AS distance FROM " . $wpdb->prefix . "wpecbd HAVING distance < %d  ORDER BY distance DESC,premium_end DESC LIMIT 500", $unit_v, $lat, $lng, $lat, $distance_v));
                if (!$query_r_2) {
                    return NULL;
                } else {
                    $last_q_d = array();
                    $last_q_i = array();
                    foreach ($query_r_2 as $last_q_v) {
                        $last_q_d[$last_q_v->post_id] = $last_q_v->distance;
                        $last_q_i[] = $last_q_v->post_id;
                    }
                }

                if (!empty($categories) AND is_array($categories)) {
                    //Category Security
                    $cat_buff = array();
                    foreach ($categories as $c_a_v) {
                        if (is_numeric($c_a_v))
                            $cat_buff[] = $c_a_v;
                    }
                    if (empty($cat_buff))
                        return FALSE;

                    $sql_query = '';
                    $sql_query.="SELECT ID as PID FROM $wpdb->posts 
                                    LEFT JOIN $wpdb->term_relationships ON 
                                    ($wpdb->posts.ID = $wpdb->term_relationships.object_id) 
                                    LEFT JOIN $wpdb->term_taxonomy ON 
                                    ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) 
                                    WHERE $wpdb->posts.post_status = 'publish' 
                                    AND $wpdb->term_taxonomy.taxonomy = 'bd_categories' 
                                    AND $wpdb->term_taxonomy.term_id IN (" . implode(', ', $cat_buff) . ") 
                                    AND $wpdb->posts.ID IN (" . implode(',', $last_q_i) . ")";
                    if (!empty($term) AND strlen($term) > 2) {
                        $term = '%' . $term . '%';
                        $sql_query.=$wpdb->prepare(" AND ((($wpdb->posts.post_title LIKE %s) OR ($wpdb->posts.post_content LIKE %s))) ", $term, $term);
                    }
                } elseif (!empty($term) AND strlen($term) > 2) {
                    $sql_query = '';
                    $sql_query.="SELECT ID as PID ";
                    $sql_query.="FROM $wpdb->posts ";
                    if ($this->_options['premium_first']) {
                        $sql_query.=" INNER JOIN " . $wpdb->prefix . "wpecbd ON " . $wpdb->posts . ".ID=" . $wpdb->prefix . "wpecbd.post_id ";
                    }
                    $term = '%' . $term . '%';
                    $sql_query.=$wpdb->prepare("WHERE $wpdb->posts.post_status = 'publish' AND $wpdb->posts.ID IN (" . implode(',', $last_q_i) . ") AND ((($wpdb->posts.post_title LIKE %s) OR ($wpdb->posts.post_content LIKE %s)))", $term, $term);
                } else {
                    return FALSE;
                }
                $query_r = $wpdb->get_col($sql_query);
            } elseif (!empty($include)) {
                $query_r = $include;
                $included = TRUE;
            } else {
                return FALSE;
            }

            if ($included === FALSE AND !empty($include)) {
                array_merge($query_r, $include);
            }

            $query_r = array_unique($query_r);

            if (!$query_r)
                return NULL;

            if (!empty($exclude)) {
                foreach ($exclude as $exclude_value) {
                    $exclude_key_key = NULL;
                    if (($exclude_key_key = array_search($exclude_value, $query_r)) !== FALSE) {
                        unset($query_r[$exclude_key_key]);
                    }
                }
            }

            if (!$query_r)
                return NULL;

            //Sort results array
            sort($query_r);

            //Prepare Output
            $out_buff = array();
            $sql_query = '';
            $sql_query.="SELECT " . $wpdb->prefix . "wpecbd.*, " . $wpdb->posts . ".* FROM " . $wpdb->posts . " INNER JOIN " . $wpdb->prefix . "wpecbd ON " . $wpdb->posts . ".ID=" . $wpdb->prefix . "wpecbd.post_id  WHERE post_id IN (" . implode(', ', $query_r) . ')';
            //Order By?
            if ($this->_options['premium_first']) {
                $sql_query.=" ORDER BY CASE WHEN DATE( " . $wpdb->prefix . "wpecbd.premium_end ) > NOW( ) THEN 1 ELSE 0 END DESC, $wpdb->posts.post_date DESC LIMIT 100";
            } else {
                $sql_query.= " ORDER BY $wpdb->posts.post_date DESC LIMIT 100";
            }
            $o_p = $wpdb->get_results($sql_query);
            foreach ($o_p as $o_p_v) {
                $premium_end = $o_p_v->premium_end;
                //Prepare Logo
                $logo = array();
                if (($logo_id = get_post_thumbnail_id($o_p_v->ID)) AND !isset($logo_id->errors))
                    $logo = wp_get_attachment_image_src($logo_id, 'medium');
                //Prepare Adress
                $address_buff = '';
                $address = '';
                if (!empty($o_p_v->address)) {
                    $address_buff.=$o_p_v->address;
                    $address_buff.=(!empty($o_p_v->city)) ? ' <span class="iw_city">' . $o_p_v->city . '</span>' : '';
                    $address_buff.=(!empty($o_p_v->state)) ? ' <span class="iw_state">' . $o_p_v->state . '</span>' : '';
                    $address_buff.=(!empty($o_p_v->country)) ? ' <span class="iw_country">' . $o_p_v->country . '</span>' : '';
                    $address = stripslashes($address_buff);
                }

                $out_buff[] = array(
                    'id' => $o_p_v->ID,
                    'post_title' => $o_p_v->post_title,
                    'post_excerpt' => (empty($o_p_v->post_excerpt)) ? $this->custom_excerpt($o_p_v->post_content) : $o_p_v->post_excerpt,
                    'url' => get_permalink($o_p_v->ID),
                    'distance' => (isset($last_q_d)) ? round($last_q_d[$o_p_v->ID], 2) : '',
                    'lat' => $o_p_v->lat,
                    'lng' => $o_p_v->lng,
                    'logo_url' => (!empty($logo[0])) ? $logo[0] : '',
                    'address' => (!empty($address)) ? $address : '',
                    'phone' => $o_p_v->phone,
                    'fax' => $o_p_v->fax,
                    'is_premium' => ($this->is_premium(NULL, TRUE)) ? 1 : 0
                );
            }

            $GLOBALS['premium_first_option'] = $this->_options['premium_first'];

            function result_sorter($a, $b) {
                $a_val = 0;
                $b_val = 0;
                if ($GLOBALS['premium_first_option']) {
                    if ($a['is_premium']) {
                        $a_val = $a_val + 2;
                    }
                    if ($b['is_premium']) {
                        $b_val = $b_val + 2;
                    }
                }
                if ($a['distance'] < $b['distance']) {
                    $a_val = $a_val + 1;
                } else {
                    $b_val = $b_val + 1;
                }

                if ($a_val == $b_val)
                    return 0;

                return ($a_val > $b_val) ? -1 : 1;
            }

            usort($out_buff, "result_sorter");

            return $out_buff;
        }

        /**
         * When admin publish a company, adds a post meta,
         * help of thi this way further submissions will be automaticly approved.
         * @param int $post_id 
         */
        function make_post_admin_approved($post_id) {
            if (current_user_can('administrator')) {
                add_post_meta($post_id, 'bd_approved', 1, TRUE) or update_post_meta($post_id, 'bd_approved', 1);
            }
        }

        /**
         * When admin unpublish a company, adds a post meta.
         * @param int $post_id 
         */
        function make_post_admin_rejected($post_id) {
            if (current_user_can('administrator'))
                add_post_meta($post_id, 'bd_approved', 0, TRUE) or update_post_meta($post_id, 'bd_approved', 0);
        }

        /**
         * Filter for template function get_categories()
         * @global object $post
         * @global type $wp_rewrite
         * @param type $cats
         * @return string 
         */
        function template_category_fix($cats) {
            global $post, $wp_rewrite;
            if ('wpecbd' == get_post_type() AND (is_single() OR is_tax('bd_tags') OR is_tax('bd_categories') OR is_search() OR in_the_loop())) {
                $categories = get_the_terms($post->ID, 'bd_categories');
                if (!$categories)
                    $categories = array();

                $categories = array_values($categories);

                foreach (array_keys($categories) as $key) {
                    _make_cat_compat($categories[$key]);
                }
                $rel = ( is_object($wp_rewrite) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';
                $thelist = '';
                $i = 0;
                $separator = ', ';
                foreach ($categories as $category) {
                    if (0 < $i)
                        $thelist .= $separator;
                    $thelist .= '<a href="' . get_term_link($category->slug, 'bd_categories') . '" title="' . esc_attr(sprintf(__("View all posts in %s"), $category->name)) . '" ' . $rel . '>' . $category->name . '</a>';
                    ++$i;
                }
                return $thelist;
            }
            return $cats;
        }

        /**
         * Adds Companies Section to Admin Right Now Dashboard Widget
         */
        function right_now_admin_widget() {
            //Total Companies
            $num_raw = wp_count_posts('wpecbd');
            $num = '<a href="edit.php?post_status=publish&post_type=wpecbd">' . $num_raw->publish . '</a>';
            $text = '<a href="edit.php?post_status=publish&post_type=wpecbd">' . _n('Company', 'Companies', $num_raw->publish, 'wpecbd') . '</a>';
            echo '<tr>';
            echo '<td class="first b b-company">' . $num . '</td>';
            echo '<td class="t tags">' . $text . '</td>';
            echo '</tr>';
            //Pending Companies
            $num = '<a href="edit.php?post_status=pending&post_type=wpecbd">' . $num_raw->pending . '</a>';
            $text = '<a class="waiting" href="edit.php?post_status=pending&post_type=wpecbd">' . _n('Pending Company', 'Pending Companies', $num_raw->pending, 'wpecbd') . '</a>';
            echo '<tr>';
            echo '<td class="b b-waiting_company">' . $num . '</td>';
            echo '<td class="last t">' . $text . '</td>';
            echo '</tr>';
        }

        function seo() {
            global $post;
            //NOINDEX Archive Pages
            if ($this->_options['seo_noindex'] AND is_tax(array('bd_categories', 'bd_tags'))) {
                echo '<meta name="robots" content="noindex,follow" />' . "\n";
            }

            //ADD KEYWORDS
            if ($this->_options['seo_keywords'] AND is_singular('wpecbd') AND isset($post)) {
                $keywords = array();
                $taxonomies = array('bd_categories', 'bd_tags');
                foreach ($taxonomies as $tax) {
                    if (($terms = get_the_term_list($post->ID, $tax, '', ', ', '')) AND !is_wp_error($terms))
                        $keywords[] = $terms;
                }

                if (!empty($keywords)) {
                    $keywords = join(', ', $keywords);
                    $keywords = '<meta name="keywords" content="' . esc_attr(strip_tags($keywords)) . '" />' . "\n";
                    echo $keywords;
                }
            }

            //ADD DESCRIPTION
            if ($this->_options['seo_desc'] AND is_singular('wpecbd')) {
                if (!has_excerpt()) {
                    $excerpt = $this->custom_excerpt($post->post_content);
                } else {
                    $excerpt = stripslashes($post->post_excerpt . '...');
                }
                echo '<meta name="description" content="' . esc_attr(substr($excerpt, 0, 160)) . '" />' . "\n";
            }
        }

        /**
         * Send HTTP POST Request to PayPal
         *
         * @param string The API method name
         * @param array	The POST Message fields in array
         * @return array Parsed HTTP Response body
         */
        function PPHttpPost($methodName_, $nvpArray_) {
            $API_Endpoint = "https://api-3t.paypal.com/nvp";
            if ("sandbox" === $this->_options['paypal']['envi'] || "beta-sandbox" === $this->_options['paypal']['envi']) {
                $API_Endpoint = "https://api-3t." . $this->_options['paypal']['envi'] . ".paypal.com/nvp";
            }

            // setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
            //curl_setopt($ch, CURLOPT_VERBOSE, 1);
            // Set the curl parameters.
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);

            // Set the API operation, version, and API signature in the request.

            $nvpreq = array(
                'METHOD' => $methodName_,
                'VERSION' => '63.0',
                'PWD' => $this->_options['paypal']['pass'],
                'USER' => $this->_options['paypal']['user'],
                'SIGNATURE' => $this->_options['paypal']['sign']
            );
            $buff = '';
            foreach (array_merge($nvpreq, $nvpArray_) as $key => $value) {
                $buff .= $key . '=' . urlencode($value) . '&';
            }
            $buff = rtrim($buff, '&');

            // Set the request as a POST FIELD for curl.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $buff);

            // Get response from the server.
            $httpResponse = curl_exec($ch);

            if (!$httpResponse) {
                exit('$methodName_ failed: ' . curl_error($ch) . '(' . curl_errno($ch) . ')');
            }

            // Extract the response details.
            $httpResponseAr = explode("&", $httpResponse);

            $httpParsedResponseAr = array();
            foreach ($httpResponseAr as $i => $value) {
                $tmpAr = explode("=", $value);
                if (sizeof($tmpAr) > 1) {
                    $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                }
            }

            if ((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
                exit("Invalid HTTP Response for POST request.");
            }

            return $httpParsedResponseAr;
        }

        function http_protocol() {
            return ((is_ssl()) ? 'https://' : 'http://');
        }

        function disable_comments() {
            global $post;
            if (isset($post) AND isset($post->comment_status))
                $post->comment_status = 'closed';
        }

        function wpecbd_max_upload_size() {
            require_once(ABSPATH . 'wp-admin/includes/template.php');
            $server_max = wp_max_upload_size();
            if ($server_max > 2097152) {
                $upload_max = 2097152;
            } else {
                $upload_max = $server_max;
            }

            return apply_filters('wpecbd_max_upload_size', $upload_max, $server_max);
        }

        /*
         * ########## ########## WPECBD REWRITE ########## ##########
         */

        function create_rewrite_rules($rules) {
            global $wp_rewrite;
            $vcard = array('^wpecbd_get_vcard/([^/]*)\.vcf$' => 'index.php?wpecbd_get_vcard=' . $wp_rewrite->preg_index(1));
            $user_edit = array('^wpecbd_user/([^/]*)\.html$' => 'index.php?wpecbd_user=' . $wp_rewrite->preg_index(1));
            $bd_big_map_popup = array('^wpecbd_big_map_popup/([^/]*)\.html$' => 'index.php?wpecbd_big_map_popup=' . $wp_rewrite->preg_index(1));
            $user_ajax_handler = array('^wpecbd_user_ajax_handler.html$' => 'index.php?wpecbd_user_ajax_handler=true');
            return $vcard + $user_edit + $user_ajax_handler + $bd_big_map_popup + $rules;
        }

        function add_query_vars($qvars) {
            $qvars[] = 'wpecbd_get_vcard';
            $qvars[] = 'wpecbd_user';
            $qvars[] = 'wpecbd_user_ajax_handler';
            $qvars[] = 'bd_widget_ajax';
            $qvars[] = 'wpecbd_big_map_popup';
            $qvars[] = 'get_bd_cat_slug_by_id';
            return $qvars;
        }

        function template_redirect_intercept() {
            global $wp_query;
            if ($wp_query->get('bd_widget_ajax')) {
                $this->widget_ajax($wp_query->get('bd_widget_ajax'));
                exit();
            } elseif ($wp_query->get('wpecbd_get_vcard')) {
                $this->prepare_vcard($wp_query->get('wpecbd_get_vcard'));
                exit;
            } elseif ($wp_query->get('wpecbd_user')) {
                $this->user_edit_page($wp_query->get('wpecbd_user'));
                exit;
            } elseif ($wp_query->get('wpecbd_user_ajax_handler')) {
                $this->ajax_admin_image_handler();
                exit;
            } elseif ($wp_query->get('wpecbd_big_map_popup')) {
                $this->frontend_big_map_popup($wp_query->get('wpecbd_big_map_popup'));
                exit;
            } elseif ($wp_query->get('get_bd_cat_slug_by_id')) {
                if (is_numeric($wp_query->get('get_bd_cat_slug_by_id')) AND ($term = get_term($wp_query->get('get_bd_cat_slug_by_id'), 'bd_categories'))) {
                    wp_redirect(home_url() . '?bd_categories=' . $term->slug);
                }
            }
        }

    }

    //Start WpBusinessDirectory
    new wp_business_directory();
    //Add Widgets
    require_once(WPECBD_PLUGIN_DIR . 'inc/widgets.php');
}