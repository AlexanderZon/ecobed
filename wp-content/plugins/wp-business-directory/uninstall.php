<?php

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die('Busted! You should not call this file directly...');
}

function wpecbd_delete_plugin() {
    global $wpdb;

    # Delete Options
    delete_option('wpecbd_options');

    # Delete Folders
    @system("rm -rf " . WP_CONTENT_DIR . '/wp_business_directory');

    # Delete all companies
    $posts = get_posts(array(
        'numberposts' => -1,
        'post_type' => 'wpecbd',
        'post_status' => 'any'));

    foreach ($posts as $post)
        wp_delete_post($post->ID, true);

    # Delete all taxonomy terms
    $all_terms = get_terms(array('bd_categories','bd_tags'), array('hide_empty' => FALSE));
    if(!empty($all_terms)){
        foreach ($all_terms as $term) {
            wp_delete_term( $term->term_id, $term->taxonomy);
        }
    }
    
    # Delete Database Table
    $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . "wpecbd");
}

wpecbd_delete_plugin();
