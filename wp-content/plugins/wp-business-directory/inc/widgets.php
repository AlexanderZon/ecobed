<?php
#Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");

/*
 * ##Search Widget##
 */
if (!class_exists('bd_Company_Search')) {

    class bd_Company_Search extends WP_Widget {

        protected $defaults = array(
            'title' => 'Search',
            'use_cat' => 1,
            'hide_empty_cats' => 1,
            'cats_show_count' => 0,
            'cats_show_childs' => 1,
            'detect_location' => 1,
            'address_search' => 1,
            'show_map' => 0,
            'unit' => 'km',
            'def_distance' => 50,
            'ad_active' => 0,
            'ad_unit' => 'HALF_BANNER',
            'ad_position' => 'RIGHT_BOTTOM'
        );

        public function __construct() {
            parent::__construct(
                    'wpbd_com_search', // Base ID
                    __('Company Search', 'wpecbd'), // Name
                    array('description' => __('Company Search Widget', 'wpecbd')) // Args
            );
        }

        public function form($instance) {
            extract(wp_parse_args($instance, $this->defaults));
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'wpecbd'); ?>:</label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <strong><?php _e('Category', 'wpecbd'); ?></strong><hr />
            <p>
                <input id="<?php echo $this->get_field_id('use_cat'); ?>" name="<?php echo $this->get_field_name('use_cat'); ?>" type="checkbox" value="1" <?php checked($use_cat, 1); ?> />
                <label for="<?php echo $this->get_field_id('use_cat'); ?>"><?php _e('Use Categories', 'wpecbd'); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id('hide_empty_cats'); ?>" name="<?php echo $this->get_field_name('hide_empty_cats'); ?>" type="checkbox" value="1" <?php checked($hide_empty_cats, 1); ?> />
                <label for="<?php echo $this->get_field_id('hide_empty_cats'); ?>"><?php _e('Hide Empty', 'wpecbd'); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id('cats_show_count'); ?>" name="<?php echo $this->get_field_name('cats_show_count'); ?>" type="checkbox" value="1" <?php checked($cats_show_count, 1); ?> />
                <label for="<?php echo $this->get_field_id('cats_show_count'); ?>"><?php _e('Show Count', 'wpecbd'); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id('cats_show_childs'); ?>" name="<?php echo $this->get_field_name('cats_show_childs'); ?>" type="checkbox" value="1" <?php checked($cats_show_childs, 1); ?> />
                <label for="<?php echo $this->get_field_id('cats_show_childs'); ?>"><?php _e('Show Childs', 'wpecbd'); ?></label>
            </p>
            <strong><?php _e('Map', 'wpecbd'); ?></strong><hr />
            <p>
                <input id="<?php echo $this->get_field_id('detect_location'); ?>" name="<?php echo $this->get_field_name('detect_location'); ?>" type="checkbox" value="1" <?php checked($detect_location, 1); ?> />
                <label for="<?php echo $this->get_field_id('detect_location'); ?>"><?php _e('Detect Location', 'wpecbd'); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id('address_search'); ?>" name="<?php echo $this->get_field_name('address_search'); ?>" type="checkbox" value="1" <?php checked($address_search, 1); ?> />
                <label for="<?php echo $this->get_field_id('address_search'); ?>"><?php _e('Show Address Search', 'wpecbd'); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id('show_map'); ?>" name="<?php echo $this->get_field_name('show_map'); ?>" type="checkbox" value="1" <?php checked($show_map, 1); ?> />
                <label for="<?php echo $this->get_field_id('show_map'); ?>"><?php _e('Show Map', 'wpecbd'); ?></label>
                <br />
                <input id="<?php echo $this->get_field_id('def_distance'); ?>" name="<?php echo $this->get_field_name('def_distance'); ?>" class="small-text" type="text" value="<?php echo esc_attr($def_distance); ?>" maxlength="4"/>
                <label for="<?php echo $this->get_field_id('def_distance'); ?>"><?php _e('Default Distance', 'wpecbd'); ?></label>
                <br />
                <select id="<?php echo $this->get_field_id('unit'); ?>" name="<?php echo $this->get_field_name('unit'); ?>">
                    <option value="km" <?php selected($unit, 'km'); ?>>Km</option>
                    <option value="mi" <?php selected($unit, 'mi'); ?>>Mile</option>
                </select>
                <label for="<?php echo $this->get_field_id('unit'); ?>"><?php _e('Measurement Unit', 'wpecbd'); ?></label>
            </p>
            <strong><?php _e('Google Adsense', 'wpecbd'); ?></strong><hr />

            <p>
                <input id="<?php echo $this->get_field_id('ad_active'); ?>" name="<?php echo $this->get_field_name('ad_active'); ?>" type="checkbox" value="1" <?php checked($ad_active, 1); ?> />
                <label for="<?php echo $this->get_field_id('ad_active'); ?>"><?php _e('Active', 'wpecbd'); ?></label>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('ad_unit'); ?>"><?php _e('Ad Unit Format', 'wpecbd'); ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('ad_unit'); ?>" name="<?php echo $this->get_field_name('ad_unit'); ?>">
                    <option value="LEADERBOARD" <?php selected($ad_unit, 'LEADERBOARD'); ?>>LEADERBOARD</option>
                    <option value="BANNER" <?php selected($ad_unit, 'BANNER'); ?>>BANNER</option>
                    <option value="HALF_BANNER" <?php selected($ad_unit, 'HALF_BANNER'); ?>>HALF BANNER</option>
                    <option value="SKYSCRAPER" <?php selected($ad_unit, 'SKYSCRAPER'); ?>>SKYSCRAPER</option>
                    <option value="WIDE_SKYSCRAPER" <?php selected($ad_unit, 'WIDE_SKYSCRAPER'); ?>>WIDE SKYSCRAPER</option>
                    <option value="VERTICAL_BANNER" <?php selected($ad_unit, 'VERTICAL_BANNER'); ?>>VERTICAL BANNER</option>
                    <option value="BUTTON" <?php selected($ad_unit, 'BUTTON'); ?>>BUTTON</option>
                    <option value="SMALL_SQUARE" <?php selected($ad_unit, 'SMALL_SQUARE'); ?>>SMALL SQUARE</option>
                    <option value="SQUARE" <?php selected($ad_unit, 'SQUARE'); ?>>SQUARE</option>
                    <option value="SMALL_RECTANGLE" <?php selected($ad_unit, 'SMALL_RECTANGLE'); ?>>SMALL RECTANGLE</option>
                    <option value="MEDIUM_RECTANGLE" <?php selected($ad_unit, 'MEDIUM_RECTANGLE'); ?>>MEDIUM RECTANGLE</option>
                    <option value="LARGE_RECTANGLE" <?php selected($ad_unit, 'LARGE_RECTANGLE'); ?>>LARGE RECTANGLE</option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('ad_position'); ?>"><?php _e('Ad Position', 'wpecbd'); ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('ad_position'); ?>" name="<?php echo $this->get_field_name('ad_position'); ?>">
                    <option value="TOP_CENTER" <?php selected($ad_position, 'TOP_CENTER'); ?>>TOP CENTER</option>
                    <option value="TOP_LEFT" <?php selected($ad_position, 'TOP_LEFT'); ?>>TOP LEFT</option>
                    <option value="TOP_RIGHT" <?php selected($ad_position, 'TOP_RIGHT'); ?>>TOP RIGHT</option>
                    <option value="LEFT_TOP" <?php selected($ad_position, 'LEFT_TOP'); ?>>LEFT TOP</option>
                    <option value="RIGHT_TOP" <?php selected($ad_position, 'RIGHT_TOP'); ?>>RIGHT TOP</option>
                    <option value="LEFT_CENTER" <?php selected($ad_position, 'LEFT_CENTER'); ?>>LEFT CENTER</option>
                    <option value="RIGHT_CENTER" <?php selected($ad_position, 'RIGHT_CENTER'); ?>>RIGHT CENTER</option>
                    <option value="LEFT_BOTTOM" <?php selected($ad_position, 'LEFT_BOTTOM'); ?>>LEFT BOTTOM</option>
                    <option value="RIGHT_BOTTOM" <?php selected($ad_position, 'RIGHT_BOTTOM'); ?>>RIGHT BOTTOM</option>
                    <option value="BOTTOM_CENTER" <?php selected($ad_position, 'BOTTOM_CENTER'); ?>>BOTTOM CENTER</option>
                    <option value="BOTTOM_LEFT" <?php selected($ad_position, 'BOTTOM_LEFT'); ?>>BOTTOM LEFT</option>
                    <option value="BOTTOM_RIGHT" <?php selected($ad_position, 'BOTTOM_RIGHT'); ?>>BOTTOM RIGHT</option>
                </select>
            </p>
            <?php
        }

        public function update($new_instance, $old_instance) {

            $buff = array();

            foreach ($this->defaults as $key => $value) {
                if (isset($new_instance[$key])) {
                    $buff[$key] = trim($new_instance[$key]);
                } else {
                    $buff[$key] = 0;
                }
            }

            return $buff;
        }

        public function widget($args, $instance) {
            global $print_js;
            $print_js = '';
            extract(wp_parse_args($instance, $this->defaults));
            extract($args);

            echo $before_widget;
            $title = apply_filters('widget_title', $instance['title']);
            if (!empty($title))
                echo $before_title . $title . $after_title;
            ?>
            <form id="<?php echo $this->get_field_id('bd_searchform'); ?>" action="#">
                <div class="widget-wpbd_com_search-term_div">
                    <label id="bd_popup_search_term_label-<?php echo $args['widget_id']; ?>" for="bd_popup_search_term-<?php echo $args['widget_id']; ?>"><?php _ex('Search Term', 'Company Search Widget', 'wpecbd'); ?></label>
                    <input name="bd_popup_search_term-<?php echo $args['widget_id']; ?>" id="<?php echo $this->get_field_id('term'); ?>" class="widget-wpbd_com_search-term" type="text" onclick="this.value='';" value="" />
                </div>
                <?php
                if ($address_search)
                    $this->prepare_location($show_map);

                if ($use_cat)
                    $this->prepare_categories($hide_empty_cats, $cats_show_childs, $cats_show_count);

                //Echo widget id
                if (!isset($args['widget_id']))
                    $args['widget_id'] = $this->id;
                $w_id_p = strpos($args['widget_id'], '-');
                $w_id_i = substr($args['widget_id'], $w_id_p + 1);
                ?>
                <input id="<?php echo $this->get_field_id('button'); ?>" type="submit" value="<?php _e('Search', 'wpecbd'); ?>" />
            </form>
            <script>
                /* <![CDATA[ */
                jQuery(document).ready(function() {
                    jQuery("#<?php echo $this->get_field_id('bd_searchform'); ?>").submit(function(){
                        bd_widget_search_submit_form({
                            url:"<?php echo site_url('?bd_widget_ajax=search'); ?>",
                            detect_location:<?php echo $detect_location; ?>,
                            widget_id:<?php echo $w_id_i; ?>,
                            element:{
                                form:"<?php echo $this->get_field_id('bd_searchform'); ?>",
                                term:"<?php echo $this->get_field_id('term'); ?>",
                                cats:"<?php echo $this->get_field_id('use_cat'); ?>",
                                lat:"<?php echo $this->get_field_id('lat'); ?>",
                                lng:"<?php echo $this->get_field_id('lng'); ?>",
                                address:"<?php echo $this->get_field_id('addresspicker'); ?>"
                            }
                        });
                        return false;
                    });
            <?php echo $print_js; ?>
                });
                /* ]]> */
            </script>
            <?php
            echo $after_widget;
        }

        protected function prepare_categories($hide_empty_cats, $cats_show_childs, $cats_show_count) {
            global $print_js;
            $print_js .= 'jQuery("#' . $this->get_field_id('use_cat') . '").multiselect({header: false,noneSelectedText:"' . __('Select Categories', 'wpecbd') . '",selectedText:"' . __('# selected', 'wpecbd') . '"});' . "\n";
            $terms_args = array();
            $terms_args['hide_empty'] = $hide_empty_cats;

            $all_cats = get_terms('bd_categories', $terms_args);

            $cat_buff = array();
            foreach ($all_cats as $cat1 => $cat2) {
                if ($cat2->parent == 0) {
                    $cat_buff[$cat2->term_id]['name'] = $cat2->name;
                    $cat_buff[$cat2->term_id]['count'] = $cat2->count;
                } else {
                    $cat_buff[$cat2->parent]['child'][$cat2->term_id]['name'] = $cat2->name;
                    $cat_buff[$cat2->parent]['child'][$cat2->term_id]['count'] = $cat2->count;
                }
            }

            if (empty($cat_buff)) {
                _e('Please Add Some Category!', 'wpecbd');
                return;
            }

            echo '<select name="bd_categories[]" id="' . $this->get_field_id('use_cat') . '" multiple="multiple">';

            if ($cats_show_childs) {
                foreach ($cat_buff as $catea1 => $catea2) {
                    echo '<optgroup label="' . $catea2['name'] . '">';
                    if (empty($catea2['child'])) {
                        echo '<option value="' . $catea1 . '" >' . $catea2['name'] . '</option>';
                    } else {
                        foreach ($catea2['child'] as $cateb1 => $cateb2) {
                            echo '<option value="' . $cateb1 . '">' . $cateb2['name'] . (($cats_show_count) ? ' (' . $catea2['count'] . ')' : '') . '</option>';
                        }
                    }
                    echo '</optgroup>';
                }
            } else {
                foreach ($cat_buff as $catea1 => $catea2) {
                    echo '<option value="' . $catea1 . '">' . $catea2['name'] . (($cats_show_count) ? ' (' . $catea2['count'] . ')' : '') . '</option>';
                }
            }
            echo '</select>';
        }

        protected function prepare_location($show_map) {
            global $print_js;
            $bd_options = get_option("wpecbd_options");
            ?>
            <div>
                <label id="bd_popup_search_location_label-<?php echo $this->id; ?>" for="bd_popup_location-<?php echo $this->id; ?>"><?php _ex('Location', 'Company Search Widget', 'wpecbd'); ?></label>
                <input id="<?php echo $this->get_field_id('addresspicker'); ?>" type="text" name="bd_popup_location-<?php echo $this->id; ?>" class="widget-wpbd_com_search-adress" onclick="this.value='';" value="" />
            </div>
            <?php if ($show_map): ?>
                <div id="<?php echo $this->get_field_id('map'); ?>" class="widget-wpbd_com_search-map"></div>
            <?php endif; ?>
            <input id="<?php echo $this->get_field_id('lat'); ?>" name="bd_w_search[lat]" type="hidden" value="<?php echo $bd_options['def_lat']; ?>" />
            <input id="<?php echo $this->get_field_id('lng'); ?>" name="bd_w_search[lng]" type="hidden" value="<?php echo $bd_options['def_lng']; ?>" />
            <?php
            $print_js.='bd_widget_search_map({show_map:' . (($show_map) ? 'true' : 'false') . ',lat:' . $bd_options['def_lat'] . ',lng:' . $bd_options['def_lng'] . ',element:{searcher:"' . $this->get_field_id('addresspicker') . '",map:"' . $this->get_field_id('map') . '",lat:"' . $this->get_field_id('lat') . '",lng:"' . $this->get_field_id('lng') . '"}});' . "\n";
        }

    }

    add_action('widgets_init', create_function('', 'register_widget("bd_Company_Search");'));
}


/*
 * ##Categories Widget##
 */
if (!class_exists('bd_Categories_Widget')) {

    class bd_Categories_Widget extends WP_Widget {

        function __construct() {
            $widget_ops = array('classname' => 'bd_Categories_Widget', 'description' => __("A list or dropdown of company directory categories"));
            parent::__construct('wpbd_com_categories', __('Business Directory Categories', 'wpecbd'), $widget_ops);
        }

        function widget($args, $instance) {
            extract($args);

            $c = !empty($instance['count']) ? '1' : '0';
            $h = !empty($instance['hierarchical']) ? '1' : '0';
            $d = !empty($instance['dropdown']) ? '1' : '0';

            echo $before_widget;
            $title = apply_filters('widget_title', $instance['title']);
            if (!empty($title))
                echo $before_title . $title . $after_title;

            $cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h, 'taxonomy' => 'bd_categories');

            if ($d) {
                $cat_args['show_option_none'] = __('Select Category');
                wp_dropdown_categories(apply_filters('widget_bd_categories_dropdown_args', $cat_args));
                ?>

                <script type='text/javascript'>
                    /* <![CDATA[ */
                    var dropdown = document.getElementById("cat");
                    function onCatChange() {
                        if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                            location.href = "<?php echo home_url(); ?>/?get_bd_cat_slug_by_id="+dropdown.options[dropdown.selectedIndex].value;
                        }
                    }
                    dropdown.onchange = onCatChange;
                    /* ]]> */
                </script>

                <?php
            } else {
                ?>
                <ul>
                    <?php
                    $cat_args['title_li'] = '';
                    wp_list_categories(apply_filters('widget_bd_categories_args', $cat_args));
                    ?>
                </ul>
                <?php
            }

            echo $after_widget;
        }

        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['count'] = !empty($new_instance['count']) ? 1 : 0;
            $instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
            $instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;

            return $instance;
        }

        function form($instance) {
            $instance = wp_parse_args((array) $instance, array('title' => ''));
            $title = esc_attr($instance['title']);
            $count = isset($instance['count']) ? (bool) $instance['count'] : false;
            $hierarchical = isset($instance['hierarchical']) ? (bool) $instance['hierarchical'] : false;
            $dropdown = isset($instance['dropdown']) ? (bool) $instance['dropdown'] : false;
            ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

            <p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('dropdown'); ?>" name="<?php echo $this->get_field_name('dropdown'); ?>"<?php checked($dropdown); ?> />
                <label for="<?php echo $this->get_field_id('dropdown'); ?>"><?php _e('Display as dropdown'); ?></label><br />

                <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked($count); ?> />
                <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Show post counts'); ?></label><br />

                <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked($hierarchical); ?> />
                <label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e('Show hierarchy'); ?></label></p>
            <?php
        }

    }

    add_action('widgets_init', create_function('', 'register_widget("bd_Categories_Widget");'));
}

/*
 * ##Recent Companies Widget##
 */
if (!class_exists('bd_Recent_Companies_Widget')) {

    class bd_Recent_Companies_Widget extends WP_Widget {

        function __construct() {
            $widget_ops = array('classname' => 'bd_Recent_Companies_Widget', 'description' => __("The most recent companies on your business directory", 'wpecbd'));
            parent::__construct('wpbd_com_recent_companies', __('Recent Companies', 'wpecbd'), $widget_ops);
            $this->alt_option_name = 'bd_Recent_Companies_Widget';

            add_action('save_post', array(&$this, 'flush_widget_cache'));
            add_action('deleted_post', array(&$this, 'flush_widget_cache'));
            add_action('switch_theme', array(&$this, 'flush_widget_cache'));
        }

        function widget($args, $instance) {
            $cache = wp_cache_get('widget_bd_recent_companies', 'widget');

            if (!is_array($cache))
                $cache = array();

            if (!isset($args['widget_id']))
                $args['widget_id'] = $this->id;

            if (isset($cache[$args['widget_id']])) {
                echo $cache[$args['widget_id']];
                return;
            }

            ob_start();
            extract($args);

            if (empty($instance['number']) || !$number = absint($instance['number']))
                $number = 10;

            $r = new WP_Query(apply_filters('widget_bd_posts_args', array('posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'post_type' => 'wpecbd')));
            if ($r->have_posts()) :
                ?>
                <?php echo $before_widget; ?>
                <?php
                $title = apply_filters('widget_title', $instance['title']);
                if (!empty($title))
                    echo $before_title . $title . $after_title;
                ?>
                <ul>
                    <?php while ($r->have_posts()) : $r->the_post(); ?>
                        <li><a href="<?php the_permalink() ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>"><?php
                    if (get_the_title())
                        the_title();
                    else
                        the_ID();
                        ?></a></li>
                    <?php endwhile; ?>
                </ul>
                <?php echo $after_widget; ?>
                <?php
                // Reset the global $the_post as this query will have stomped on it
                wp_reset_postdata();

            endif;

            $cache[$args['widget_id']] = ob_get_flush();
            wp_cache_set('widget_bd_recent_companies', $cache, 'widget');
        }

        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['number'] = (int) $new_instance['number'];
            $this->flush_widget_cache();

            $alloptions = wp_cache_get('alloptions', 'options');
            if (isset($alloptions['bd_Recent_Companies_Widget']))
                delete_option('bd_Recent_Companies_Widget');
            return $instance;
        }

        function flush_widget_cache() {
            wp_cache_delete('widget_bd_recent_companies', 'widget');
        }

        function form($instance) {
            $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
            $number = isset($instance['number']) ? absint($instance['number']) : 5;
            ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

            <p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of companies to show:', 'wpecbd'); ?></label>
                <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
            <?php
        }

    }

    add_action('widgets_init', create_function('', 'register_widget("bd_Recent_Companies_Widget");'));
}

/*
 * ##Tag Cloud Widget##
 */
if (!class_exists('bd_tag_cloud_Widget')) {

    class bd_tag_cloud_Widget extends WP_Widget {

        protected $defaults = array(
            'title' => 'Business Tag Cloud',
            'smallest' => 8,
            'largest' => 22,
            'number' => 45,
            'format' => 'flat',
            'orderby' => 'name',
            'order' => 'ASC',
            'link' => 'view',
        );

        function __construct() {
            $widget_ops = array('classname' => 'bd_tag_cloud_Widget', 'description' => __("Your most used company tags in cloud format", 'wpecbd'));
            parent::__construct('wpbd_com_tag_cloud', __('Business Directory Tag Cloud', 'wpecbd'), $widget_ops);
        }

        function widget($args, $instance) {
            extract(wp_parse_args($instance, $this->defaults));
            extract($args);

            echo $before_widget;
            $title = apply_filters('widget_title', $instance['title']);
            if (!empty($title))
                echo $before_title . $title . $after_title;
            echo '<div class="tagcloud">';
            wp_tag_cloud(apply_filters('widget_bd_tag_cloud_args', array('taxonomy' => 'bd_tags',
                        'smallest' => $smallest,
                        'largest' => $largest,
                        'number' => $number,
                        'format' => $format,
                        'orderby' => $orderby,
                        'order' => $order)));
            echo "</div>\n";
            echo $after_widget;
        }

        public function update($new_instance, $old_instance) {

            $buff = array();

            foreach ($this->defaults as $key => $value) {

                if (isset($new_instance[$key])) {
                    $buff[$key] = $new_instance[$key];
                } else {
                    $buff[$key] = 0;
                }
            }

            return $buff;
        }

        function form($instance) {
            extract(wp_parse_args($instance, $this->defaults));
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('smallest'); ?>"><?php _e('Smallest', 'wpecbd') ?>:</label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('smallest'); ?>" name="<?php echo $this->get_field_name('smallest'); ?>" value="<?php echo esc_attr($smallest); ?>" />  
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('largest'); ?>"><?php _e('Largest', 'wpecbd') ?>:</label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('largest'); ?>" name="<?php echo $this->get_field_name('largest'); ?>" value="<?php echo esc_attr($largest); ?>" />  
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number', 'wpecbd') ?>:</label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo esc_attr($number); ?>" />  
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('format'); ?>"><?php _e('Format', 'wpecbd') ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('format'); ?>" name="<?php echo $this->get_field_name('format'); ?>">
                    <option value="flat" <?php selected('flat', $format); ?>><?php _e('Flat', 'wpecbd'); ?></option>
                    <option value="list" <?php selected('list', $format); ?>><?php _e('List', 'wpecbd'); ?></option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Orderby', 'wpecbd') ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>">
                    <option value="name" <?php selected('name', $orderby); ?>><?php _e('Name', 'wpecbd'); ?></option>
                    <option value="count" <?php selected('count', $orderby); ?>><?php _e('Count', 'wpecbd'); ?></option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Order', 'wpecbd') ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>">
                    <option value="ASC" <?php selected('ASC', $order); ?>><?php _e('Ascending', 'wpecbd'); ?></option>
                    <option value="DESC" <?php selected('DESC', $order); ?>><?php _e('Descending', 'wpecbd'); ?></option>
                    <option value="RAND" <?php selected('RAND', $order); ?>><?php _e('Random', 'wpecbd'); ?></option>
                </select>
            </p>
            <?php
        }

    }

    add_action('widgets_init', create_function('', 'register_widget("bd_tag_cloud_Widget");'));
}

/*
 * ##Premium Companies Widget##
 */
if (!class_exists('bd_premium_companies_Widget')) {

    class bd_premium_companies_Widget extends WP_Widget {

        protected $defaults = array(
            'title' => 'Premium Companies',
            'number' => 5,
            'show_logo' => 1,
            'orderby' => 'rand',
            'order' => 'DESC'
        );

        function __construct() {
            $widget_ops = array('classname' => 'bd_premium_companies_Widget', 'description' => __("A list of your premium companies", 'wpecbd'));
            parent::__construct('wpbd_com_premium_companies', __('Premium Companies', 'wpecbd'), $widget_ops);
            $this->alt_option_name = 'bd_premium_companies_Widget';
            add_action('save_post', array(&$this, 'flush_widget_cache'));
            add_action('deleted_post', array(&$this, 'flush_widget_cache'));
            add_action('switch_theme', array(&$this, 'flush_widget_cache'));
        }

        function form($instance) {
            extract(wp_parse_args($instance, $this->defaults));
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number', 'wpecbd') ?>:</label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo esc_attr($number); ?>" />  
            </p>
            <p>
                <input type="checkbox" id="<?php echo $this->get_field_id('show_logo'); ?>" value="1" name="<?php echo $this->get_field_name('show_logo'); ?>" <?php checked(1, $show_logo); ?> /> 
                <label for="<?php echo $this->get_field_id('show_logo'); ?>"><?php _e('Show Logo', 'wpecbd') ?></label>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Orderby', 'wpecbd') ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>">
                    <option value="post_date" <?php selected('post_date', $orderby); ?>><?php _e('Post Date', 'wpecbd'); ?></option>
                    <option value="premium_end" <?php selected('premium_end', $orderby); ?>><?php _e('Premium End', 'wpecbd'); ?></option>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Order', 'wpecbd') ?>:</label>
                <select class="widefat" id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>">
                    <option value="asc" <?php selected('asc', $order); ?>><?php _e('Ascending', 'wpecbd'); ?></option>
                    <option value="desc" <?php selected('desc', $order); ?>><?php _e('Descending', 'wpecbd'); ?></option>
                    <option value="rand" <?php selected('rand', $order); ?>><?php _e('Random', 'wpecbd'); ?></option>
                </select>
            </p>
            <?php
        }

        function widget($args, $instance) {
            global $wpdb, $post;
            $cache = wp_cache_get('widget_bd_premium_companies', 'widget');

            if (!is_array($cache))
                $cache = array();

            if (!isset($args['widget_id']))
                $args['widget_id'] = $this->id;

            if (isset($cache[$args['widget_id']])) {
                echo $cache[$args['widget_id']];
                return;
            }

            ob_start();
            extract(wp_parse_args($instance, $this->defaults));
            extract($args);

            $sql_query = '';
            $sql_query.="SELECT $wpdb->posts.*," . $wpdb->prefix . "wpecbd.* ";
            $sql_query.="FROM $wpdb->posts INNER JOIN " . $wpdb->prefix . "wpecbd ON $wpdb->posts.ID = " . $wpdb->prefix . "wpecbd.post_id ";
            $sql_query.="WHERE $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'wpecbd' ";
            $sql_query.="AND " . $wpdb->prefix . "wpecbd.premium_end > '" . gmdate('Y-m-d') . "' ";
            $sql_query.="ORDER BY " . (($orderby == 'post_date') ? $wpdb->posts . '.post_date' : (($order == 'rand') ? 'RAND()' : $wpdb->prefix . "wpecbd.premium_end")) . ' ';
            $sql_query.=($order == 'rand') ? '' : (($order == 'asc') ? 'ASC ' : 'DESC ');
            $sql_query.=$wpdb->prepare("LIMIT %d", $number);
            $query = $wpdb->get_results($sql_query);

            $old_post = $post;

            if ($query):
                echo $before_widget;
                $title = apply_filters('widget_title', $instance['title']);
                if (!empty($title))
                    echo $before_title . $title . $after_title;

                echo '<div class="bd_widget_premium_container">';
                foreach ($query as $post) {
                    ?>
                    <div class="bd_widget_premium_com_container">
                        <?php
                        if ($show_logo AND has_post_thumbnail()) {
                            ?>
                            <div class="bd_prem_com_logo"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a></div>
                            <?php
                        }
                        ?>
                        <div><a href="<? the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
                        <br class="bd_prem_com_logo_br" />
                    </div>
                    <?php
                }
                echo '</div>';
                echo $after_widget;
            endif;

            $post = $old_post;

            $cache[$args['widget_id']] = ob_get_flush();
            wp_cache_set('widget_bd_premium_companies', $cache, 'widget');
        }

        function flush_widget_cache() {
            wp_cache_delete('widget_bd_premium_companies', 'widget');
        }

        public function update($new_instance, $old_instance) {

            $buff = array();

            foreach ($this->defaults as $key => $value) {

                if (isset($new_instance[$key])) {
                    $buff[$key] = $new_instance[$key];
                } else {
                    $buff[$key] = 0;
                }
            }
            $this->flush_widget_cache();

            $alloptions = wp_cache_get('alloptions', 'options');
            if (isset($alloptions['bd_premium_companies_Widget']))
                delete_option('bd_premium_companies_Widget');

            return $buff;
        }

    }

    add_action('widgets_init', create_function('', 'register_widget("bd_premium_companies_Widget");'));
}