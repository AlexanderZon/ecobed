<?php
#Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");
defined('WPBD_CSV_UPLOAD_DIR') or define('WPBD_CSV_UPLOAD_DIR', WP_CONTENT_DIR . '/wp_business_directory' . '/upload');

if (!class_exists('wpecbd_csv_importer')) {

    class wpecbd_csv_importer {

        public static $action;

        function view() {

            //Define Action
            if (!empty($_GET['wpbd_action'])) {
                switch ($_GET['wpbd_action']) {

                    case 'preparation':
                        self::$action = 'preparation';
                        break;

                    case 'last_controls':
                        self::$action = 'last_controls';
                        break;

                    case 'process':
                        self::$action = 'process';
                        break;

                    case 'upload':
                    default :
                        self::$action = 'upload';
                        break;
                }
            } else {
                self::$action = 'upload';
            }
            ?>

            <div class="wrap">

                <div id="icon-tools" class="icon32"></div>
                <h2><?php _e('WP Business Directory - CSV Company Importer', 'wpecbd'); ?></h2>

                <h2 class="nav-tab-wrapper">
                    <div class="nav-tab <?php self::tab_activate('upload'); ?>"><?php _e('Step 1 - Upload', 'wpecbd'); ?></div>
                    <div class="nav-tab <?php self::tab_activate('preparation'); ?>"><?php _e('Step 2 - Preparation', 'wpecbd'); ?></div>
                    <div class="nav-tab <?php self::tab_activate('last_controls'); ?>"><?php _e('Step 3 - Last Controls', 'wpecbd'); ?></div>
                    <div class="nav-tab <?php self::tab_activate('process'); ?>"><?php _e('Step 4 - Process', 'wpecbd'); ?></div>
                </h2>
                <div class="wrap">
                    <?php
                    //Security
                    current_user_can('administrator') or die('Permission Denied! You have to admin!');

                    switch (self::$action) {
                        case 'preparation':
                            self::step_two_preparation();
                            break;

                        case 'preparation':
                            self::step_two_preparation();
                            break;

                        case 'last_controls':
                            self::step_three_last_controls();
                            break;

                        case 'process':
                            self::step_four_process();
                            break;

                        case 'upload':
                        default:
                            self::step_one_upload();
                            break;
                    }
                    ?>
                </div> <!-- .wrap -->
            </div> <!-- .wrap -->

            <?php
        }

        function step_one_upload() {
            ?>
            <form id="file_selection_form" method="POST" action="#">
                <table class="widefat">
                    <tr>
                        <td class="row-title"><?php _e('Please Select a File or Upload', 'wpecbd'); ?></td>
                    </tr>
                    <tr>
                        <td>
                            <ul id="file_list">
                                <?php
                                if (($files = self::prepare_folders_files())):
                                    foreach ($files as $file) {
                                        echo '<li file_name="' . esc_attr($file) . '"><label><input type="radio" name="file" value="' . esc_attr($file) . '" /> <span>' . $file . '</span></label> - <a href="#" class="delete_file" file_name="' . esc_attr($file) . '"><em>Delete</em></a></li>';
                                    } elseif (is_null($files)):
                                    echo '<li id="no_file_li">' . __('No files found!', 'wpecbd') . '</li>';
                                endif;
                                ?>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td><div id="wpecbd_csv_uploader"></div><br class="clear" /><div id="uploading_file"></div></td>
                    </tr>
                </table>
                <br />
                <input class="button-primary" type="submit" value="<?php esc_attr_e("Let's Start", 'wpecbd'); ?>" />
            </form>
            <script>
                jQuery(document).ready(function($) {
                    var wpecbd_csv_uploader = new qq.FileUploader({
                        element: document.getElementById('wpecbd_csv_uploader'),
                        action: ajaxurl+'?action=wpecbd_csv_uploader',
                        allowedExtensions: [],
                        params: {
                            nonce_admin: '<?php echo wp_create_nonce('wpecbd_csv_upload'); ?>'
                        },
                        sizeLimit: <?php echo wp_max_upload_size(); ?>,
                        messages: {
                            typeError: "<?php _e('{file} has invalid extension. Only {extensions} are allowed.', 'wpecbd'); ?>",
                            sizeError: "<?php _e('{file} is too large, maximum file size is {sizeLimit}.', 'wpecbd'); ?>",
                            minSizeError: "<?php _e('{file} is too small, minimum file size is {minSizeLimit}.', 'wpecbd'); ?>",
                            emptyError: "<?php _e('{file} is empty, please select files again without it.', 'wpecbd'); ?>",
                            onLeave: "<?php _e('The files are being uploaded, if you leave now the upload will be cancelled.', 'wpecbd'); ?>"
                        },
                        onSubmit: function(id, fileName){
                            $('#uploading_file').text(fileName+' '+'<?php _e('Uploading, please wait...', 'wpecbd'); ?>').fadeIn();
                        },
                        onComplete: function(id, fileName, responseJSON){
                            $('#file_list').append('<li file_name="'+responseJSON.file_name+'"><label><input type="radio" name="file" value="'+responseJSON.file_name+'" /> <span>'+responseJSON.file_name+'</span></label> - <a href="#" class="delete_file" file_name="'+responseJSON.file_name+'"><em>Delete</em></a></li>');
                            $('#no_file_li').hide();
                            $('#uploading_file').hide();
                        }
                    });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                    jQuery(document).on("click", '.delete_file', function(){
                        var file_name_val=$(this).attr('file_name');
                        $.ajax({
                            url: ajaxurl+'?action=wpecbd_csv_uploader',
                            type: "GET",
                            data: { nonce_admin: "<?php echo wp_create_nonce('wpecbd_csv_upload'); ?>", file_name: file_name_val,delete_file:true }
                        }).done(function ( data ) {
                            $('li[file_name="'+file_name_val+'"]').remove();
                            if($('#file_list li').size() < 1){
                                $('#file_list').append('<li id="no_file_li"><?php _e('No files found!', 'wpecbd'); ?></li>');
                            }
                        });
                        return false;
                    });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                    $('#file_selection_form').submit(function(){
                        if($('[name="file"]:checked').size() > 0){
                            window.location.href = "<?php echo admin_url('tools.php?page=wpecbd_csv_importer&wpbd_action=preparation'); ?>&file="+encodeURI($('[name="file"]:checked').val());
                            return false;
                        }else{
                            alert('<?php _e('Please select a file...', 'wpecbd'); ?>');
                            return false;
                        }
                    });
                });
            </script>
            <style>
                .wpecbd-upload-drop-area, .wpecbd-upload-button{
                    background-color: #F2F2F2;
                    width: 160px;
                    text-align: center;
                    padding: 8px;
                    font-size: 20px;
                    border: 1px solid black;
                }
                .wpecbd-upload-list{
                    display: none;
                }
                #uploading_file{
                    background-image: url('<?php echo WPECBD_PLUGIN_URL . 'img/loading.gif' ?>');
                    background-repeat: no-repeat;
                    padding-left: 20px;
                }
            </style>
            <?php
        }

        function step_two_preparation() {
            //If PHP is not properly recognizing the line endings when reading 
            //files either on or created by a Macintosh computer, enabling the 
            //auto_detect_line_endings run-time configuration option may help 
            //resolve the problem.
            ini_set("auto_detect_line_endings", true);

            is_file(WPBD_CSV_UPLOAD_DIR . '/' . $_GET['file'] . '.csv') or die('File missing... ' . WPBD_CSV_UPLOAD_DIR . '/' . $_GET['file'] . '.csv');

            $row = 0;
            $lines = array();
            $max_column = array();
            if (!($handle = fopen(WPBD_CSV_UPLOAD_DIR . '/' . $_GET['file'] . '.csv', "r")) !== FALSE) {
                die('File missing... ' . WPBD_CSV_UPLOAD_DIR . '/' . $_GET['file'] . '.csv');
            }

            $delimiter = ((empty($_GET['bd_delimiter'])) ? ',' : stripslashes($_GET['bd_delimiter']));
            $enclosure = ((empty($_GET['bd_enclosure'])) ? '"' : stripslashes($_GET['bd_enclosure']));
            $category_delimiter = ((empty($_GET['bd_category_delimiter'])) ? '\\' : stripslashes($_GET['bd_category_delimiter']));
            $tag_delimiter = ((empty($_GET['bd_tag_delimiter'])) ? ',' : stripslashes($_GET['bd_tag_delimiter']));

            while (($data = fgetcsv($handle, 0, $delimiter, $enclosure)) !== FALSE) {
                $lines[] = $data;
                $max_column[] = count($data);
                $row++;
                if ($row > 8) {
                    break;
                }
            }
            fclose($handle);
            echo '<form id="preparation_form" method="POST" action="' . admin_url('tools.php?page=wpecbd_csv_importer&wpbd_action=last_controls') . '">';
            echo '<p><strong>' . __('Check the table below, if the table displaying not properly, change the options (delimiter,enclosure) according to your file and try again...<br /> If everything is normal, set your columns and click Ready!', 'wpecbd') . '</strong></p>';
            echo '<div style="width:100% !important;overflow: auto;"><table class="widefat">';
            echo '<tr>';

            for ($column_n = 0; $column_n < max($max_column); $column_n++) {
                ?>
                <th>
                    <select id="line_select-<?php echo $column_n; ?>" name="lines[<?php echo $column_n; ?>]">
                        <?php self::option_lists(); ?>
                    </select>
                </th>
                <?php
            }

            echo '</tr>';

            foreach ($lines as $key => $value) {
                echo '<tr' . (($key % 2 == 0) ? ' class="alternate"' : '') . '>';
                foreach ($value as $key_c => $value_c) {
                    echo '<td>';
                    echo $value_c;
                    echo '</td>';
                }
                echo '</tr>';
            }

            echo '</table></div>';
            ?>

            <table class="widefat">
                <tr>
                    <th class="row-title"><?php _e('Options', 'wpecbd'); ?></th>
                    <th><?php _e('Values', 'wpecbd'); ?></th>
                    <th><?php _e('Description', 'wpecbd'); ?></th>
                </tr>
                <tr>
                <tr>
                    <td class="row-title"><label for="tablecell"><?php _e('Delimiter', 'wpecbd'); ?></label></td>
                    <td><input id="delimiter" class="small-text" type="text" name="bd_delimiter" maxlength="1" value="<?php echo esc_attr($delimiter); ?>" /></td>
                    <td><?php _e('Set the field delimiter (one character only)', 'wpecbd'); ?></td>
                </tr>
                <tr class="alternate">
                    <td class="row-title"><label for="tablecell"><?php _e('Enclosure', 'wpecbd'); ?></label></td>
                    <td><input id="enclosure" class="small-text" type="text" name="bd_enclosure" maxlength="1" value="<?php echo esc_attr($enclosure); ?>" /></td>
                    <td><?php _e('Set the field enclosure character (one character only)', 'wpecbd'); ?></td>
                </tr>
                <tr>
                    <td class="row-title"><label for="tablecell"><?php _e('Category Delimiter', 'wpecbd'); ?></label></td>
                    <td><input id="category_delimiter" class="small-text" type="text" name="bd_category_delimiter" maxlength="1" value="<?php echo esc_attr($category_delimiter); ?>" /></td>
                    <td><?php _e('Set the category delimiter (one character only)', 'wpecbd'); ?></td>
                </tr>
                <tr class="alternate">
                    <td class="row-title"><label for="tablecell"><?php _e('Tag Delimiter', 'wpecbd'); ?></label></td>
                    <td><input id="tag_delimiter" class="small-text" type="text" name="bd_tag_delimiter" maxlength="1" value="<?php echo esc_attr($tag_delimiter); ?>" /></td>
                    <td><?php _e('Set the tag delimiter (one character only)', 'wpecbd'); ?></td>
                </tr>
            </table>
            <br class="clear" />
            <input id="file" type="hidden" name="file" value="<?php echo esc_attr($_GET['file']); ?>" />
            <input id="ready_button" class="button-primary" type="submit" value="<?php _e('Ready!', 'wpecbd'); ?>" />
            <input id="try_button" class="button-secondary" type="submit" value="<?php _e('Try This Settings', 'wpecbd'); ?>" /> 
            </form>
            <script>
                jQuery(document).ready(function($) {
                    $('#try_button').click(function(){
                        window.location.href = "<?php echo admin_url('tools.php?page=wpecbd_csv_importer&wpbd_action=preparation'); ?>"+encodeURI('&bd_delimiter='+$('#delimiter').val()+'&bd_enclosure='+$('#enclosure').val()+'&file='+$('#file').val());
                        return false;
                    });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                    $('#ready_button').click(function(){
                        $('#preparation_form').submit();      
                    }); 
                });    
            </script>
            <?php
        }

        function step_three_last_controls() {
            global $category_delimiter, $tag_delimiter;

            ini_set("auto_detect_line_endings", true);

            is_file(WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv') or die('File missing... ' . WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv');

            $row = 0;
            $lines = array();
            if (!($handle = fopen(WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv', "r")) !== FALSE) {
                die('File missing... ' . WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv');
            }

            if (!in_array('name', $_POST['lines'])) {
                die('<p><strong>You must set the Name column!</strong></p>');
            }

            $delimiter = ((empty($_POST['bd_delimiter'])) ? ',' : stripslashes($_POST['bd_delimiter']));
            $enclosure = ((empty($_POST['bd_enclosure'])) ? '"' : stripslashes($_POST['bd_enclosure']));
            $category_delimiter = ((empty($_POST['bd_category_delimiter'])) ? '\\' : stripslashes($_POST['bd_category_delimiter']));
            $tag_delimiter = ((empty($_POST['bd_tag_delimiter'])) ? ',' : stripslashes($_POST['bd_tag_delimiter']));
            ?>
            <div class="wrap">
                <form method="POST" action="<?php echo admin_url('tools.php?page=wpecbd_csv_importer&wpbd_action=process&bd_process_nonce=' . wp_create_nonce('bd_process')); ?>">
                    <p><strong><?php _e('Default values, if empty'); ?></strong></p>
                    <table class="widefat">
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('City', 'wpecbd'); ?></td>
                            <td><input type="text" name="default_values[city]" value="" /></td>
                        </tr>
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('State', 'wpecbd'); ?></td>
                            <td><input type="text" name="default_values[state]" value="" /></td>
                        </tr>
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('Country', 'wpecbd'); ?></td>
                            <td><input type="text" name="default_values[country]" value="" /></td>
                        </tr>
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('Tags', 'wpecbd'); ?></td>
                            <td><input type="text" name="default_values[tags]" value="" />
                                <?php
                                _e('Delimiter Character Is => ');
                                echo $tag_delimiter;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('Categories', 'wpecbd'); ?></td>
                            <td><input type="text" name="default_values[categories]" value="" />
                                <?php
                                _e('Delimiter Character Is => ');
                                echo $category_delimiter;
                                ?>
                            </td>
                        </tr>
                    </table>

                    <p><strong><?php _e('Options'); ?></strong></p>
                    <table class="widefat">
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('Skip First # Lines', 'wpecbd'); ?></td>
                            <td><input type="text" name="skip_lines" value="1" /></td>
                        </tr>
                        <?php
                        /*
                         * * FOR NEXT VERSION
                          <tr>
                          <td class="row-title" style="width: 20%;"><?php _e('Create Users', 'wpecbd'); ?></td>
                          <td><input type="checkbox" name="create_users" value="1" /> <em><?php _e('If you selected Contact name and Contact Email columns, this option will create user accounts for contacts.', 'wpecbd'); ?></em></td>
                          </tr>
                         * 
                         */
                        ?>
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('Auto Find Map Location', 'wpecbd'); ?></td>
                            <td><input type="checkbox" name="autofind_lat_lng" value="1" /> <em><?php _e('If latitude/longitude columns are empty, this option try to find map location automatically.', 'wpecbd'); ?></em></td>
                        </tr>
                        <tr>
                            <td class="row-title" style="width: 20%;"><?php _e('Publish', 'wpecbd'); ?></td>
                            <td><input type="checkbox" name="make_published" value="1" /> <em><?php _e('Make imported companies published.', 'wpecbd'); ?></em></td>
                        </tr>
                    </table>
                    <br />
                    <input id="ready_button" class="button-primary" type="submit" value="<?php _e('Process!', 'wpecbd'); ?>" />
                    <input name="bd_delimiter" type="hidden" value="<?php echo esc_attr($delimiter); ?>" />
                    <input name="bd_enclosure" type="hidden" value="<?php echo esc_attr($enclosure); ?>" />
                    <input name="bd_category_delimiter" type="hidden" value="<?php echo esc_attr($category_delimiter); ?>" />
                    <input name="bd_tag_delimiter" type="hidden" value="<?php echo esc_attr($tag_delimiter); ?>" />
                    <input name="file" type="hidden" value="<?php echo esc_attr($_POST['file']); ?>" />
                    <?php
                    foreach ($_POST['lines'] as $key => $value) {
                        echo '<input type="hidden" name="lines[' . $key . ']" value="' . $value . '" />';
                    }
                    ?>
                </form>
                <p><strong><?php _e('Please check the values below last time...'); ?></strong></p>



                <?php
                while (($data = fgetcsv($handle, 0, $delimiter, $enclosure)) !== FALSE) {
                    $lines[] = $data;
                    $row++;
                    if ($row > 7) {
                        break;
                    }
                }
                fclose($handle);


                foreach ($lines as $com) {
                    self::echo_last_control_companies($com, $_POST['lines']);
                }

                echo '</div>';
            }

            function step_four_process() {
                if (!wp_verify_nonce($_GET['bd_process_nonce'], 'bd_process'))
                    die('Security Check Failed!');
                global $category_delimiter, $tag_delimiter;
                ini_set("auto_detect_line_endings", true);

                is_file(WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv') or die('File missing... ' . WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv');


                if (!($handle = fopen(WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv', "r")) !== FALSE) {
                    die('File missing... ' . WPBD_CSV_UPLOAD_DIR . '/' . $_POST['file'] . '.csv');
                }


                $delimiter = ((empty($_POST['bd_delimiter'])) ? ',' : stripslashes($_POST['bd_delimiter']));
                $enclosure = ((empty($_POST['bd_enclosure'])) ? '"' : stripslashes($_POST['bd_enclosure']));
                $category_delimiter = ((empty($_POST['bd_category_delimiter'])) ? '\\' : stripslashes($_POST['bd_category_delimiter']));
                $tag_delimiter = ((empty($_POST['bd_tag_delimiter'])) ? ',' : stripslashes($_POST['bd_tag_delimiter']));

                echo '<p><strong>Import Log</strong></p>';
                echo '<pre>';
                $row = -1;
                while (($data = fgetcsv($handle, 0, $delimiter, $enclosure)) !== FALSE) {
                    $row++;
                    if ($_POST['skip_lines'] > $row) {
                        continue;
                    }
                    self::process_data($data, $_POST['lines']);
                }
                fclose($handle);
                echo '</pre>';
            }

            function process_data($data, $lines) {
                global $wpdb, $category_delimiter, $tag_delimiter;

                set_time_limit(45);

                $images = array();
                $categories = array();
                $category_ids = array();
                $tags = array();
                $name = $shortd = $longd = $address = $city = $state = $zip = $country = $lat = $lng = $phone = $fax = $website = $email = $twitter = $facebook = $googleplus = $linkedin = $rss = $logo_url = $skype = '';
                $contact_name = $contact_surname = $contact_email = '';

                foreach ($lines as $key => $value) {

                    switch ($value) {
                        case 'name':
                            $name = $data[$key];
                            break;
                        case 'shortd':
                            $shortd = $data[$key];
                            break;
                        case 'longd':
                            $longd = $data[$key];
                            break;
                        case 'address':
                            $address = $data[$key];
                            break;
                        case 'city':
                            $city = $data[$key];
                            break;
                        case 'state':
                            $state = $data[$key];
                            break;
                        case 'zip':
                            $zip = $data[$key];
                            break;
                        case 'country':
                            $country = $data[$key];
                            break;
                        case 'lat':
                            $lat = $data[$key];
                            break;
                        case 'lng':
                            $lng = $data[$key];
                            break;
                        case 'phone':
                            $phone = $data[$key];
                            break;
                        case 'fax':
                            $fax = $data[$key];
                            break;
                        case 'website':
                            $website = $data[$key];
                            break;
                        case 'email':
                            $email = $data[$key];
                            break;
                        case 'twitter':
                            $twitter = $data[$key];
                            break;
                        case 'facebook':
                            $facebook = $data[$key];
                            break;
                        case 'googleplus':
                            $googleplus = $data[$key];
                            break;
                        case 'linkedin':
                            $linkedin = $data[$key];
                            break;
                        case 'rss':
                            $rss = $data[$key];
                            break;
                        case 'skype':
                            $skype = $data[$key];
                            break;
                        case 'tags':
                            if (!empty($data[$key])) {
                                if (!empty($tag_delimiter)) {
                                    $cat_buff = array();
                                    $cat_buff = @explode($tag_delimiter, $data[$key]);
                                    foreach ($cat_buff as $tag_vals) {
                                        $tags[] = trim($tag_vals);
                                    }
                                } else {
                                    $tags[] = trim($data[$key]);
                                }
                            }
                            break;
                        case 'categories':
                            if (!empty($data[$key])) {
                                if (!empty($category_delimiter)) {
                                    $cat_buff = array();
                                    $cat_buff = @explode($category_delimiter, $data[$key]);
                                    foreach ($cat_buff as $cat_vals) {
                                        $categories[] = trim($cat_vals);
                                    }
                                } else {
                                    $categories[] = trim($data[$key]);
                                }
                            }

                            //Check Exist
                            break;
                        case 'logo_url':
                            $logo_url = esc_url($data[$key]);
                            break;
                        case 'img_url':
                            $images[] = esc_url($data[$key]);
                            break;
                        case 'contact_name':
                            $contact_name = $data[$key];
                            break;
                        case 'contact_surname':
                            $contact_surname = $data[$key];
                            break;
                        case 'contact_email':
                            $contact_email = $data[$key];
                            break;
                        default:
                            break;
                    }
                }

                $save_data = array(
                    'web_site' => !empty($website) ? esc_url($website) : '',
                    'email' => !empty($email) ? sanitize_email($email) : '',
                    'phone' => !empty($phone) ? $phone : '',
                    'fax' => !empty($fax) ? $fax : '',
                    'address' => !empty($address) ? $address : '',
                    'city' => !empty($city) ? $city : $_POST['default_values']['city'],
                    'state' => !empty($state) ? $state : $_POST['default_values']['state'],
                    'country' => !empty($country) ? $country : $_POST['default_values']['country'],
                    'zip' => !empty($zip) ? $zip : '',
                    'lat' => !empty($lat) ? $lat : '',
                    'lng' => !empty($lng) ? $lng : '',
                    'twitter' => !empty($twitter) ? $twitter : '',
                    'facebook' => !empty($facebook) ? esc_url($facebook) : '',
                    'googleplus' => !empty($googleplus) ? $googleplus : '',
                    'linkedin' => !empty($linkedin) ? esc_url($linkedin) : '',
                    'rss' => !empty($rss) ? esc_url($rss) : '',
                    'skype' => !empty($skype) ? $skype : ''
                );

                //Try to find lat lng
                if (isset($_POST['autofind_lat_lng']) AND (empty($lat) OR empty($lng))) {
                    $address_for_geocoding = '';
                    $address_for_geocoding .= $name;
                    $address_for_geocoding .= (!empty($save_data['address'])) ? ', ' . $save_data['address'] : '';
                    $address_for_geocoding .= (!empty($save_data['city'])) ? ', ' . $save_data['city'] : '';
                    $address_for_geocoding .= (!empty($save_data['state'])) ? ', ' . $save_data['state'] : '';
                    $address_for_geocoding .= (!empty($save_data['zip'])) ? ', ' . $save_data['zip'] : '';
                    $address_for_geocoding .= (!empty($save_data['country'])) ? ', ' . $save_data['country'] : '';

                    if (($auto_lat_lng = self::geocoding($address_for_geocoding))) {
                        $save_data['lat'] = $auto_lat_lng['lat'];
                        $save_data['lng'] = $auto_lat_lng['lng'];
                    }
                }


                $save_data_format = array(
                    '%s', //web_site
                    '%s', //email
                    '%s', //phone
                    '%s', //fax
                    '%s', //address
                    '%s', //city
                    '%s', //state
                    '%s', //country
                    '%s', //zip
                    '%f', //lat
                    '%f', //lng
                    '%s', //twitter
                    '%s', //facebook
                    '%s', //googleplus
                    '%s', //linkedin
                    '%s', //rss
                    '%s', //skype
                    '%s', //premium_end
                );

                $insert_post_data = array(
                    'post_excerpt' => $shortd,
                    'post_title' => $name,
                    'post_type' => 'wpecbd',
                    'post_content' => $longd,
                    'post_status' => (!empty($_POST['make_published']) AND $_POST['make_published']) ? 'publish' : 'pending'
                );

                if (empty($insert_post_data['post_title'])) {
                    self::log('<span style="color:red;">NAME FIELD EMPTY, SKIPING!...</span>', NULL, TRUE);
                }

                //Check For Company Exist
                $is_new = TRUE;
                if (($com_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_title=%s AND post_type='wpecbd'", $insert_post_data['post_title']))) AND !is_wp_error($com_id)) {
                    $is_new = FALSE;
                    self::log($insert_post_data['post_title'] . ' already exist. Updating...', NULL, TRUE);
                    self::log('Post ID: ' . '<a href="' . admin_url('post.php?post=' . $com_id . '&action=edit') . '">' . $com_id . '</a>');
                } else {
                    self::log($insert_post_data['post_title'] . ' Adding...', NULL, TRUE);
                }
                //Save<->Update & Get Post ID
                if ($is_new === TRUE) {
                    //Create New Post
                    if (!($com_id = wp_insert_post($insert_post_data)) OR is_wp_error($com_id)) {
                        self::log('wp_insert_post error!!!', TRUE);
                        return FALSE;
                    } else {
                        self::log('Added! Post ID: ' . $com_id);
                    }
                    $save_data['premium_end'] = gmdate('Y-m-d');
                    $save_data = array('post_id' => $com_id) + $save_data;
                    array_unshift(&$save_data_format, '%d');
                    if (is_wp_error($wpdb->insert($wpdb->prefix . 'wpecbd', $save_data, $save_data_format))) {
                        self::log('Database error!!!', TRUE);
                        return FALSE;
                    }
                } else {
                    if (is_wp_error($wpdb->update($wpdb->prefix . 'wpecbd', $save_data, array('post_id' => $com_id), $save_data_format, array('%d')))) {
                        self::log('Database error!!!', TRUE);
                        return FALSE;
                    }
                    $insert_post_data['ID'] = $com_id;
                    if (is_wp_error(wp_update_post($insert_post_data))) {
                        self::log('Post update error!!!', TRUE);
                        return FALSE;
                    }
                }

                //Default Values For TAG&Category
                if (!empty($_POST['default_values']['categories'])) {
                    if (!empty($category_delimiter)) {
                        $cat_buff = array();
                        $cat_buff = @explode($category_delimiter, $_POST['default_values']['categories']);
                        foreach ($cat_buff as $cat_vals) {
                            $categories[] = trim($cat_vals);
                        }
                    } else {
                        $categories[] = trim($_POST['default_values']['categories']);
                    }
                }

                if (!empty($_POST['default_values']['tags'])) {
                    if (!empty($tag_delimiter)) {
                        $cat_buff = array();
                        $cat_buff = @explode($tag_delimiter, $_POST['default_values']['tags']);
                        foreach ($cat_buff as $tag_vals) {
                            $tags[] = trim($tag_vals);
                        }
                    } else {
                        $tags[] = trim($_POST['default_values']['tags']);
                    }
                }



                //Add Tags&Categories
                if (!empty($categories)) {
                    //Check All Categories
                    $all_categories = array();
                    $all_categories_buff = get_terms('bd_categories', array('hide_empty' => FALSE));
                    if ($all_categories_buff) {
                        foreach ($all_categories_buff as $cat_buf) {
                            $all_categories[$cat_buf->term_id] = $cat_buf->name;
                        }
                    }

                    foreach (array_unique($categories) as $cat_test) {
                        if (($founded_id = array_search($cat_test, $all_categories))) {
                            $category_ids[] = $founded_id;
                        } else {
                            if (is_wp_error($is_inserted_cat = wp_insert_term($cat_test, 'bd_categories'))) {
                                self::log('Category adding error: ' . $cat_test, TRUE);
                                continue;
                            }
                            if ($is_inserted_cat) {
                                $category_ids[] = $is_inserted_cat['term_id'];
                            }
                        }
                    }

                    if (!empty($category_ids))
                        if (is_wp_error(wp_set_post_terms($com_id, $category_ids, 'bd_categories', FALSE))) {
                            self::log('wp_set_post_terms error!', TRUE);
                        }
                }

                //Update Tags
                if (!empty($tags))
                    if (is_wp_error(wp_set_post_terms($com_id, array_unique($tags), 'bd_tags', FALSE))) {
                        self::log('wp_set_post_terms error!', TRUE);
                    }
                //IF Exist Remove Images&Logo
                if (!$is_new) {
                    $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $com_id);
                    $attachments = get_posts($args);
                    if ($attachments) {
                        foreach ($attachments as $image_post) {
                            if (is_wp_error(wp_delete_attachment($image_post->ID, TRUE))) {
                                self::log('wp_delete_attachment error! : ' . $image_post->ID, TRUE);
                            }
                        }
                    }
                }
                //Add Images
                if (!empty($images)) {
                    foreach ($images as $image_url) {
                        if(empty($image_url))
                            continue;
                        if (!self::save_img($com_id, esc_url($image_url), FALSE)) {
                            self::log('Save image error: ' . $image_url, TRUE);
                        }
                    }
                }
                if (!empty($logo_url)) {
                    if (!self::save_img($com_id, esc_url($logo_url), TRUE)) {
                        self::log('Save logo error: ' . $logo_url, TRUE);
                    }
                }

                if ($_POST['create_users']) {
                    $USER = self::create_user($contact_email, $contact_name, $contact_surname);
                    if ($USER !== FALSE) {
                        if (is_array($USER)) {
                            $user_id = $USER['id'];
                            //Send Email
                            self::send_email($com_id, $user_id);
                        } else {
                            $user_id = $USER;
                        }
                        wp_update_post(array('ID' => $com_id, 'post_author' => $user_id));
                    }
                }

                //RETURN
                self::log('=== DONE! ===' . "\r\n");
                return $com_id;
            }

            function create_user($email, $name = NULL, $surname = NULL) {
                $name = @trim($name);
                $surname = @trim($surname);
                $email = @trim($email);
                $username = '';

                if (empty($email)) {
                    self::log('email field is empty, pass', TRUE);
                    return FALSE;
                }

                if (!is_email($email)) {
                    self::log('email is not valid : ' . $email, TRUE);
                    return FALSE;
                }

                if ($id = email_exists($email)) {
                    self::log('email Already registred : ' . $email);
                    return $id;
                }

                //Create username
                $valid_username_found = FALSE;

                if (empty($name) OR empty($surname)) {
                    $mail_parts = explode('@', $email);
                    $username = $mail_parts[0];
                    $valid_username_found = (strlen($mail_parts[0]) > 3 AND !username_exists($mail_parts[0]) AND validate_username($mail_parts[0])) ? TRUE : FALSE;
                }

                if (!$valid_username_found AND !empty($name)) {
                    $test_username_from_name = $name;
                    while (username_exists($test_username_from_name) OR !validate_username($test_username_from_name)) {
                        $test_username_from_name = $name . rand(0, 9999);
                    }
                    $username = $test_username_from_name;
                    $valid_username_found = TRUE;
                } elseif (!$valid_username_found AND !empty($surname)) {
                    $test_username_from_surname = $surname;
                    while (username_exists($test_username_from_surname) OR !validate_username($test_username_from_surname)) {
                        $test_username_from_surname = $surname . rand(0, 9999);
                    }
                    $username = $test_username_from_surname;
                    $valid_username_found = TRUE;
                }

                if (!$valid_username_found) {
                    self::log('I couldn\'t find a valid username to create user...', TRUE);
                    return FALSE;
                }
                $random_password = wp_generate_password(12, false);
                $user_id = wp_create_user($username, $random_password, $email);
                wp_update_user(array('ID' => $user_id, 'first_name' => $name, 'last_name' => $surname));
                return array('id' => $user_id, 'pass' => $random_password);
            }

            function send_email($com_id, $user_id) {
                $variables = array();
            }

            function geocoding($address) {
                $r = wp_remote_get('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                if (!is_wp_error($r) AND $r['response']['code'] == 200) {
                    $j = json_decode($r['body']);
                } else {
                    return FALSE;
                }

                switch ($j->status) {
                    case 'ZERO_RESULTS':
                        self::log('Google Geocoding Error : ZERO_RESULTS', TRUE);
                        return FALSE;
                        break;
                    case 'OVER_QUERY_LIMIT':
                        self::log('Google Geocoding Error : OVER_QUERY_LIMIT', TRUE);
                        return FALSE;
                        break;
                    case 'REQUEST_DENIED':
                        self::log('Google Geocoding Error : REQUEST_DENIED', TRUE);
                        return FALSE;
                        break;
                    case 'INVALID_REQUEST':
                        self::log('Google Geocoding Error : INVALID_REQUEST', TRUE);
                        return FALSE;
                        break;
                    case 'OK':
                        $out = array();
                        $out['lat'] = $j->results[0]->geometry->location->lat;
                        $out['lng'] = $j->results[0]->geometry->location->lng;
                        return $out;
                        break;
                    default:
                        return FALSE;
                }
            }

            function log($e, $error = FALSE, $strong = FALSE) {

                if ($error) {
                    echo "<span style=\"color:red;\"><strong>$e</strong></span> \r\n";
                } elseif ($strong) {
                    echo "\r\n<strong>$e</strong> \r\n";
                } else {
                    echo "$e \r\n";
                }
            }

            function get_file($url, $name = FALSE, $folder = FALSE) {

                if (!$name) {
                    $name = basename($url);
                }

                if (!$folder) {
                    $folder = sys_get_temp_dir();
                }

                if (!is_writable($folder)) {
                    return FALSE;
                }

                $tmpfname = $folder . '/' . wp_unique_filename($folder, $name);

                $response = wp_remote_get($url, array('timeout' => 300, 'stream' => true, 'filename' => $tmpfname));

                if (is_wp_error($response)) {
                    unlink($tmpfname);
                    return FALSE;
                }

                if (200 != wp_remote_retrieve_response_code($response)) {
                    unlink($tmpfname);
                    return FALSE;
                }

                return array('tmp' => $tmpfname, 'name' => $name);
            }

            function save_img($com_id, $url = NULL, $is_logo = FALSE, $file_name = NULL) {
                if (empty($url)) {
                    return FALSE;
                }

                if (!($img = self::get_file($url)))
                    return FALSE;

                $wp_filetype = wp_check_filetype($img['tmp']);
                $uploads = wp_upload_dir();
                $unique_name = wp_unique_filename($uploads['path'], $img['name'] . "." . $wp_filetype['ext']);
                $filename = $uploads['path'] . "/" . $unique_name;
                rename($img['tmp'], $filename);
                $attachment = array(
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                    'post_content' => '',
                    'post_status' => 'inherit',
                    'guid' => $uploads['url'] . "/" . $unique_name
                );
                $attach_id = wp_insert_attachment($attachment, $filename, $com_id);
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);
                if ($is_logo)
                    add_post_meta($com_id, '_thumbnail_id', $attach_id);

                return $attach_id;
            }

            function echo_last_control_companies($com, $fields) {
                global $category_delimiter, $tag_delimiter;
                $name_key = array_search('name', $fields);
                //Skip nameless
                if (empty($com[$name_key]))
                    return NULL;
                ?>
                <table class="widefat">
                    <tr>
                        <td class="row-title" style="width: 20%;"><label for="tablecell"><?php _e('Name', 'wpecbd'); ?></label></td>
                        <td><?php echo $com[$name_key] ?></td>
                    </tr>
                    <?php
                    foreach ($fields as $key => $value) {
                        if ($value == 'name' OR $value == 'none')
                            continue;
                        ?>
                        <tr>
                            <td class="row-title"><label for="tablecell"><?php echo ucfirst($value); ?></label></td>
                            <?php
                            switch ($value) {
                                case 'img_url':
                                case 'logo_url':
                                    $e_val = '<img src="' . esc_url($com[$key]) . '" style="max-height:150px;"/>';
                                    break;
                                case 'website':
                                    $e_val = esc_url($com[$key]);
                                    break;
                                case 'tags':
                                    $tag_buff = array();
                                    $tag_buff = @explode($tag_delimiter, $com[$key]);
                                    $tag_buff2[] = array();
                                    foreach ($tag_buff as $tag) {
                                        $tag_buff2[] = trim($tag);
                                    }
                                    $e_val = @implode(', ', $tag_buff2);
                                    break;
                                case 'categories':
                                    $cat_buff = array();
                                    $cat_buff = @explode($category_delimiter, $com[$key]);
                                    $cat_buff2 = array();
                                    foreach ($cat_buff as $cat) {
                                        $cat_buff2[] = trim($cat);
                                    }
                                    $e_val = @implode(', ', $cat_buff2);
                                    break;
                                default:
                                    $e_val = $com[$key];
                                    break;
                            }

                            if (empty($e_val))
                                $e_val = 'N/A';
                            ?>
                            <td><?php echo $e_val; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <?php
            }

            function prepare_folders_files() {
                $error = FALSE;

                if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory'))
                    mkdir(WP_CONTENT_DIR . '/wp_business_directory');

                if (!is_dir(WPBD_CSV_UPLOAD_DIR)) {
                    if (!mkdir(WPBD_CSV_UPLOAD_DIR, 0775)) {
                        echo 'ERROR! You must create directory ' . WPBD_CSV_UPLOAD_DIR . ' and chmod 775';
                        $error = TRUE;
                    }
                }

                $files = array();

                if (!$error AND ($folder = opendir(WPBD_CSV_UPLOAD_DIR))) {
                    while (false !== ($file = readdir($folder))) {
                        $path_parts = pathinfo($file);

                        if ($path_parts['extension'] == 'csv') {
                            $files[] = $path_parts['filename'];
                        }
                    }
                    closedir($folder);
                }

                if ($error) {
                    return FALSE;
                } elseif (empty($files)) {
                    return NULL;
                } else {
                    return $files;
                }
            }

            function option_lists() {
                ?>
                <option value="none"><?php _e('None', 'wpecbd'); ?></option>
                <option value="name"><?php _e('Business Name', 'wpecbd'); ?></option>
                <option value="shortd"><?php _e('Short Description', 'wpecbd'); ?></option>
                <option value="longd"><?php _e('Long Description', 'wpecbd'); ?></option>
                <option value="address"><?php _e('Address', 'wpecbd'); ?></option>
                <option value="city"><?php _e('City', 'wpecbd'); ?></option>
                <option value="state"><?php _e('State', 'wpecbd'); ?></option>
                <option value="zip"><?php _e('Zip', 'wpecbd'); ?></option>
                <option value="country"><?php _e('Country', 'wpecbd'); ?></option>
                <option value="lat"><?php _e('Latitude', 'wpecbd'); ?></option>
                <option value="lng"><?php _e('Longitude', 'wpecbd'); ?></option>
                <option value="phone"><?php _e('Phone', 'wpecbd'); ?></option>
                <option value="fax"><?php _e('Fax', 'wpecbd'); ?></option>
                <option value="website"><?php _e('Web Site', 'wpecbd'); ?></option>
                <option value="email"><?php _e('eMail', 'wpecbd'); ?></option>
                <option value="logo_url"><?php _e('Logo Url', 'wpecbd'); ?></option>
                <option value="img_url"><?php _e('Image Url', 'wpecbd'); ?></option>
                <option value="twitter"><?php _e('Twitter', 'wpecbd'); ?></option>
                <option value="facebook"><?php _e('Facebook', 'wpecbd'); ?></option>
                <option value="googleplus"><?php _e('GooglePlus', 'wpecbd'); ?></option>
                <option value="linkedin"><?php _e('LinkedIn', 'wpecbd'); ?></option>
                <option value="rss"><?php _e('Rss', 'wpecbd'); ?></option>
                <option value="skype"><?php _e('Skype', 'wpecbd'); ?></option>
                <option value="tags"><?php _e('Tags', 'wpecbd'); ?></option>
                <option value="categories"><?php _e('Categories', 'wpecbd'); ?></option>
                <?php
                /*
                 * * FOR NEXT VERSION
                  <option value="contact_name"><?php _e('Contact Name', 'wpecbd'); ?></option>
                  <option value="contact_surname"><?php _e('Contact Surname', 'wpecbd'); ?></option>
                  <option value="contact_email"><?php _e('Contact email', 'wpecbd'); ?></option>
                 * 
                 */
                ?>

                <?php
            }

            function tab_activate($tab) {
                if ($tab == self::$action)
                    echo 'nav-tab-active';
            }

            function handle_upload() {

                if (!current_user_can('update_core') OR !wp_verify_nonce($_GET['nonce_admin'], 'wpecbd_csv_upload'))
                    die(json_encode(array('success' => 'false', 'error' => 'Permission Denied!!!')));

                if (isset($_GET['delete_file'])) {
                    unlink(WPBD_CSV_UPLOAD_DIR . '/' . $_GET['file_name'] . '.csv');
                    die(json_encode(array('success' => 'true')));
                }

                if ($_FILES["wpecbd_file"]["error"] > 0) {
                    die(json_encode(array('success' => 'false', 'error' => 'Error:' . $_FILES["wpecbd_file"]["error"])));
                } else {
                    $file = $_FILES["wpecbd_file"]["name"];
                    $path_parts = pathinfo(sanitize_file_name($file));
                    if (move_uploaded_file($_FILES["wpecbd_file"]["tmp_name"], WPBD_CSV_UPLOAD_DIR . '/' . $path_parts['filename'] . '.csv')) {
                        die(json_encode(array('success' => 'true', 'file_name' => $path_parts['filename'])));
                    } else {
                        die(json_encode(array('success' => 'false', 'error' => 'Can\'t move file!')));
                    }
                }
            }

        }

    }