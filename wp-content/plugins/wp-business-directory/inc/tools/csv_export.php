<?php
#Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");
defined('WPBD_CSV_DOWNLOAD_DIR') or define('WPBD_CSV_DOWNLOAD_DIR', WP_CONTENT_DIR . '/wp_business_directory' . '/upload');
defined('WPBD_CSV_DOWNLOAD_URL') or define('WPBD_CSV_DOWNLOAD_URL', WP_CONTENT_URL . '/wp_business_directory' . '/upload');

if (!class_exists('wpecbd_csv_exporter')) {

    class wpecbd_csv_exporter {

        function get_companies_from_db($walk = 20) {
            global $wpdb;
            $limit = '';
            static $walk_val = 0;
            if ((bool) $walk && is_integer($walk)) {
                $limit = " LIMIT {$walk_val},";
                $walk_val = $walk_val + $walk;
                $limit .=$walk_val;
            }

            return $wpdb->get_results("SELECT * FROM {$wpdb->prefix}wpecbd INNER JOIN {$wpdb->posts} ON {$wpdb->posts}.ID = {$wpdb->prefix}wpecbd.post_id WHERE {$wpdb->posts}.post_type = 'wpecbd' {$limit}", ARRAY_A);
        }

        function get_company_images($company_id) {
            $out = array();
            //Find Logo
            if (($logo_id = get_post_thumbnail_id($company_id)) && !is_wp_error($logo_id)) {
                $logo = wp_get_attachment_image_src($logo_id, 'large');
                $out['logo'] = $logo[0];
            } else {
                $out['logo'] = '';
            }

            //Fild images except logo
            $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $company_id);
            $attachments = get_posts($args);
            if (is_wp_error($attachments) OR empty($attachments))
                return $out;

            $counter = 0;
            foreach ($attachments as $attachment) {
                if ($attachment->ID == $logo_id) {
                    continue;
                }
                $img = wp_get_attachment_image_src($attachment->ID, 'large');

                $out['image' . $counter] = $img[0];
                $counter++;
            }

            return $out;
        }

        function get_company_taxonomy($company_id) {
            $out = array('categories' => '', 'tags' => '');

            //Get Categories
            $get_categories = wp_get_object_terms($company_id, 'bd_categories');

            if (!is_wp_error($get_categories) && !empty($get_categories)) {
                $cat_buff = array();
                foreach ($get_categories as $category) {
                    $cat_buff[] = $category->name;
                }
                if (!empty($cat_buff)) {
                    $out['categories'] = implode('\\', $cat_buff);
                }
            }

            //Get Tags
            $get_tags = wp_get_object_terms($company_id, 'bd_tags');

            if (!is_wp_error($get_tags) && !empty($get_tags)) {
                $tag_buff = array();
                foreach ($get_tags as $tag) {
                    $tag_buff[] = $tag->name;
                }
                if (!empty($tag_buff)) {
                    $out['tags'] = implode(',', $tag_buff);
                }
            }
            return $out;
        }

        function progress() {
            $default_options = array(
                'delimiter' => ',',
                'enclosure' => '"',
                'auto_detect_line_endings' => 1
            );

            ini_set('auto_detect_line_endings', $default_options['auto_detect_line_endings']);
            //Open file for write
            $file_name = gmdate('M_d_Y-H_i_s') . '-business_directory_export.csv';
            $fp = fopen(WPBD_CSV_DOWNLOAD_DIR . '/' . $file_name, 'w');

            if (!$fp) {
                die("Exporter can't create files. Mission aborted.");
            }

            $headers_writed = FALSE;

            //Get companies
            while (($coms = self::get_companies_from_db())) {
                $coms = array_map(array('wpecbd_csv_exporter', 'clean_unused'), $coms);
                //Write Headers
                if (!$headers_writed) {
                    $images_headers = array('logo' => '');
                    $options = get_option('wpecbd_options');
                    $max_image = max($options['f_maximg'], $options['p_maximg']);
                    for ($index = 1; $index <= $max_image; $index++) {
                        $images_headers['image-' . $index] = '';
                    }

                    //Write Current Line
                    fputcsv($fp, array_keys(array_merge($coms[0], array('categories' => '', 'tags' => ''), $images_headers)), $default_options['delimiter'], $default_options['enclosure']);
                    $headers_writed = TRUE;
                }
                foreach ($coms as $com) {
                    $images = self::get_company_images($com['post_id']);
                    $taxonomies = self::get_company_taxonomy($com['post_id']);
                    fputcsv($fp, array_values($com+$taxonomies+$images));
                }
            }

            fclose($fp);

            return $file_name;
        }

        function clean_unused($com = array()) {
            static $except_list = array(
        'post_author',
        'post_date',
        'post_date_gmt',
        'post_status',
        'comment_status',
        'ping_status',
        'post_password',
        'to_ping',
        'pinged',
        'post_modified',
        'post_modified_gmt',
        'post_content_filtered',
        'post_parent',
        'guid',
        'menu_order',
        'post_mime_type',
        'comment_count',
        'post_type',
        'post_name',
        'ID');

            foreach ($com as $k_com => $v_com) {
                if (in_array($k_com, $except_list)) {
                    unset($com[$k_com]);
                }
            }
            return $com;
        }

        public function view() {
            ?>
            <div class="wrap">

                <div id="icon-tools" class="icon32"></div>
                <h2><?php _e('WP Business Directory - CSV Company Importer', 'wpecbd'); ?></h2>

                <div id="poststuff">
                    <div id="post-body" class="metabox-holder">
                        <!-- main content -->
                        <div id="post-body-content">
                            <div class="postbox">
                                <div class="inside">
                                    <?php
                                    $result = self::progress();
                                    ?>
                                    <p>
                                        Your export file is prepared and saved to "<?php echo WPBD_CSV_DOWNLOAD_DIR . '/' . $result; ?>"
                                    </p>
                                    <p>
                                        Also you can download from <a href="<?php echo WPBD_CSV_DOWNLOAD_URL . '/' . $result; ?>"><?php echo WPBD_CSV_DOWNLOAD_URL . '/' . $result; ?></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php
        }

    }

}