<?php

// Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");

function wpecbd_update() {

    if (!get_option('wpecbd_tpl_inloop'))
        add_option('wpecbd_tpl_inloop', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/inloop.html'));


    if (!get_option('wpecbd_tpl_singlepage'))
        add_option('wpecbd_tpl_singlepage', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/singlepage.html'));
    
    if (!get_option('wpecbd_tpl_feed'))
        add_option('wpecbd_tpl_feed', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/feed.html'));

    if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory'))
        mkdir(WP_CONTENT_DIR . '/wp_business_directory');

    if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory' . '/languages'))
        mkdir(WP_CONTENT_DIR . '/wp_business_directory' . '/languages');

    if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory' . '/upload'))
        mkdir(WP_CONTENT_DIR . '/wp_business_directory' . '/upload');

    //Copy language files
    $lang_files = array();

    if (($folder = opendir(WP_CONTENT_DIR . '/wp_business_directory' . '/languages'))) {
        while (false !== ($file = readdir($folder))) {
            $path_parts = pathinfo($file);

            if ($path_parts['extension'] == 'mo' OR $path_parts['extension'] == 'po') {
                $lang_files[] = $path_parts['basename'];
            }
        }
        closedir($folder);
    }

    if (!empty($lang_files)) {
        foreach ($lang_files as $lang_file) {
            copy(WP_CONTENT_DIR . '/wp_business_directory' . '/languages/' . $lang_file, WPECBD_PLUGIN_DIR . 'languages/' . $lang_file);
        }
    }
}