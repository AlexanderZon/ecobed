<?php
// Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");

//Check options saving?
if (isset($_GET['wpecbd_do']) AND (($_GET['wpecbd_do'] == 'save_options' AND !empty($_POST['bd'])) OR $_GET['wpecbd_do'] == 'reset_options')) {
    //Security Check
    if (!wp_verify_nonce($_REQUEST['_wpnonce'], 'wpecbd_save_options') AND !current_user_can('administrator'))
        die('Security check failed!!!');

    if ($_GET['wpecbd_do'] == 'reset_options') {
        delete_option('wpecbd_options');
        $this->LoadOptions();
        echo '<div class="updated"><p><strong>' . __('Options has been successfully reset!', 'wpecbd') . '</strong></p></div>';
    } elseif ($_GET['wpecbd_do'] == 'save_options') {
        //Save Options
        $this->LoadOptions($_POST['bd']);
        echo '<div class="updated"><p><strong>' . __('Options successfully saved!', 'wpecbd') . '</strong></p></div>';
    }

    //Flush rewrite rules
    flush_rewrite_rules();
}
?>
<div class="wrap">

    <div id="icon-options-general" class="icon32"></div>
    <h2>WP Business Directory - <?php _e('Options', 'wpecbd'); ?></h2>
    <form name="wpecbd_options" method="post" action="<?php echo admin_url('edit.php?post_type=wpecbd&page=options&wpecbd_do=save_options'); ?>">

        <div class="metabox-holder has-right-sidebar">
            <div class="inner-sidebar">
                <div class="postbox">
                    <h3><span><?php _e('Purchase Code', 'wpecbd'); ?></span></h3>
                    <div class="inside">
                        <input name="bd[purchase_code]" id="purchase_code" type="text" value="<?php echo (strlen($this->_options['purchase_code']) > 10) ? esc_attr($this->_options['purchase_code']) : ''; ?>" class="widefat" placeholder="<?php _e('Please enter your Purchse Code', 'wpecbd'); ?>" />
                    </div>
                </div>


                <div class="postbox">
                    <h3><span><?php _e('Slugs', 'wpecbd'); ?></span></h3>
                    <div class="inside">
                        <table class="widefat">
                            <tr>
                                <td>
                                    <strong><?php _e('Directory Slug', 'wpecbd'); ?></strong>
                                    <span style="float: right;">
                                        <input type="checkbox" name="bd[dir_slug_with_front]" id="dir_slug_with_front" value="1" <?php checked($this->_options['dir_slug_with_front'], 1); ?> />
                                        <label for="dir_slug_with_front"><?php _e('With Front', 'wpecbd'); ?></label>
                                    </span>
                                </td>
                            </tr> 
                            <tr class="alternate">
                                <td>
                                    <input name="bd[dir_slug]" type="text" value="<?php echo esc_attr($this->_options['dir_slug']); ?>" class="widefat" />
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    <strong><?php _e('Category Slug', 'wpecbd'); ?></strong>
                                    <span style="float: right;">
                                        <input type="checkbox" name="bd[cat_slug_with_front]" id="cat_slug_with_front" value="1" <?php checked($this->_options['cat_slug_with_front'], 1); ?> />
                                        <label for="cat_slug_with_front"><?php _e('With Front', 'wpecbd'); ?></label>
                                    </span>
                                </td>
                            </tr> 
                            <tr class="alternate">
                                <td>
                                    <input name="bd[cat_slug]" type="text" value="<?php echo esc_attr($this->_options['cat_slug']); ?>" class="widefat" />
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    <strong><?php _e('Tag Slug', 'wpecbd'); ?></strong>
                                    <span style="float: right;">
                                        <input type="checkbox" name="bd[tag_slug_with_front]" id="tag_slug_with_front" value="1" <?php checked($this->_options['tag_slug_with_front'], 1); ?> />
                                        <label for="tag_slug_with_front"><?php _e('With Front', 'wpecbd'); ?></label>
                                    </span>
                                </td>
                            </tr> 
                            <tr class="alternate">
                                <td>
                                    <input name="bd[tag_slug]" type="text" value="<?php echo esc_attr($this->_options['tag_slug']); ?>" class="widefat" />
                                </td>
                            </tr> 
                        </table>
                    </div>
                </div>

                <div class="postbox">
                    <h3><span><?php _e('Features', 'wpecbd'); ?></span></h3>
                    <div class="inside">
                        <table class="widefat">
                            <tr>
                                <th class="row-title"><?php _e('Feature Name', 'wpecbd'); ?></th>
                                <th><?php _e('Active', 'wpecbd'); ?></th>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Comments', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[comments]" id="">
                                        <option value="1" <?php selected($this->_options['comments'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['comments'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Premium', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[premium]" id="">
                                        <option value="1" <?php selected($this->_options['premium'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['premium'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Category', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[category]" id="">
                                        <option value="1" <?php selected($this->_options['category'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['category'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Tag', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[tag]" id="">
                                        <option value="1" <?php selected($this->_options['tag'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['tag'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Social', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[social]" id="">
                                        <option value="1" <?php selected($this->_options['social'], TRUE); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['social'], FALSE); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('QR Code', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[qr]" id="">
                                        <option value="1" <?php selected($this->_options['qr'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['qr'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('QR Action', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[qr_type]" id="">
                                        <option value="vcard-download" <?php selected($this->_options['qr_type'], 'vcard-download'); ?>><?php _e('Download', 'wpecbd'); ?></option>
                                        <option value="vcard-embed" <?php selected($this->_options['qr_type'], 'vcard-embed'); ?>><?php _e('Embed', 'wpecbd'); ?></option>
                                        <option value="url" <?php selected($this->_options['qr_type'], 'url'); ?>><?php _e('Url', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="row-title"><?php _e('Auto Locality', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[auto_locality]" id="">
                                        <option value="1" <?php selected($this->_options['auto_locality'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['auto_locality'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Sql Fix', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[sql_fix]" id="">
                                        <option value="1" <?php selected($this->_options['sql_fix'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['sql_fix'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Premium First', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[premium_first]" id="">
                                        <option value="1" <?php selected($this->_options['premium_first'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['premium_first'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Alphabetical Order', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[alphabetical_order]" id="">
                                        <option value="1" <?php selected($this->_options['alphabetical_order'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['alphabetical_order'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _ex('Captcha', 'admin area captcha', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[captcha]" id="">
                                        <option value="2" <?php selected($this->_options['captcha'], 2); ?>><?php _e('Yes - Classic', 'wpecbd'); ?></option>
                                        <option value="1" <?php selected($this->_options['captcha'], 1); ?>><?php _e('Yes - Only Image', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['captcha'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Currency', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[currency]" id="">
                                        <option value="AUD" <?php selected($this->_options['currency'], 'AUD'); ?>>Australian Dollar</option>
                                        <option value="CAD" <?php selected($this->_options['currency'], 'CAD'); ?>>Canadian Dollar</option>
                                        <option value="CZK" <?php selected($this->_options['currency'], 'CZK'); ?>>Czech Koruna</option>
                                        <option value="DKK" <?php selected($this->_options['currency'], 'DKK'); ?>>Danish Krone</option>
                                        <option value="EUR" <?php selected($this->_options['currency'], 'EUR'); ?>>Euro</option>
                                        <option value="HUF" <?php selected($this->_options['currency'], 'HUF'); ?>>Hungarian Forint</option>
                                        <option value="JPY" <?php selected($this->_options['currency'], 'JPY'); ?>>Japanese Yen</option>
                                        <option value="NOK" <?php selected($this->_options['currency'], 'NOK'); ?>>Norwegian Krone</option>
                                        <option value="NZD" <?php selected($this->_options['currency'], 'NZD'); ?>>New Zealand Dollar</option>
                                        <option value="PLN" <?php selected($this->_options['currency'], 'PLN'); ?>>Polish Zloty</option>
                                        <option value="GBP" <?php selected($this->_options['currency'], 'GBP'); ?>>Pound Sterling</option>
                                        <option value="SGD" <?php selected($this->_options['currency'], 'SGD'); ?>>Singapore Dollar</option>
                                        <option value="SEK" <?php selected($this->_options['currency'], 'SEK'); ?>>Swedish Krona</option>
                                        <option value="CHF" <?php selected($this->_options['currency'], 'CHF'); ?>>Swiss Franc</option>
                                        <option value="USD" <?php selected($this->_options['currency'], 'USD'); ?>>U.S. Dollar</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="postbox">
                    <h3><span><?php _e('Masked Input', 'wpecbd'); ?></span></h3>
                    <div class="inside">
                        <table class="widefat">
                            <tr>
                                <td class="row-title"><?php _e('Acvive', 'wpecbd'); ?>?</td>
                                <td>
                                    <select name="bd[mask_actv]" id="">
                                        <option value="1" <?php selected($this->_options['mask_actv'], 1); ?>><?php _e('Yes', 'wpecbd'); ?></option>
                                        <option value="0" <?php selected($this->_options['mask_actv'], 0); ?>><?php _e('No', 'wpecbd'); ?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Telephone', 'wpecbd'); ?></td>
                                <td><input name="bd[mask_tel]" id="bd_mask_tel" value="<?php echo esc_attr($this->_options['mask_tel']); ?>" type="text" /></td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Fax', 'wpecbd'); ?></td>
                                <td><input name="bd[mask_fax]" id="bd_mask_fax" value="<?php echo esc_attr($this->_options['mask_fax']); ?>" type="text" /></td>
                            </tr>
                        </table>
                    </div>
                </div>


                <div class="postbox">
                    <script>
                        /* <![CDATA[ */
                        jQuery(function() {
                            var addresspickerMap = jQuery( "#addresspicker_map" ).addresspicker({
                                elements: {
                                    map:      "#map",
                                    lat:      "#def_lat",
                                    lng:      "#def_lng"
                                },
                                mapOptions: {
                                    zoom: <?php echo $this->_options['zoom_edit']; ?>,
                                    center: new google.maps.LatLng(<?php echo $this->_options['def_lat'] . ', ' . $this->_options['def_lng']; ?>),
                                    scrollwheel: false,
                                    mapTypeId: google.maps.MapTypeId.<?php echo $this->_options['maptype_edit']; ?>
                                }
                            });
                            var gmarker = addresspickerMap.addresspicker( "marker");
                            gmarker.setVisible(true);
                            addresspickerMap.addresspicker( "updatePosition");
                        });
                        /* ]]> */
                    </script>
                    <h3><span>MAP Options</span></h3>
                    <div class="inside">
                        <table class="widefat">
                            <tr>
                                <td class="row-title" colspan="2"><?php _e('Default Map Location', 'wpecbd'); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input id="addresspicker_map" placeholder="<?php _e('Search', 'wpecbd'); ?>" type="text" style="width: 244px;"/></td>
                            </tr>
                            <tr>
                                <td colspan="2"><div id="map" style="height: 200px;width: 100%;"></div></td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Default Lat', 'wpecbd'); ?></td>
                                <td><input name="bd[def_lat]" id="def_lat" type="text" value="<?php echo $this->_options['def_lat']; ?>" size="14" readonly="true" /></td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Default Lng', 'wpecbd'); ?></td>
                                <td><input name="bd[def_lng]" id="def_lng" type="text" value="<?php echo $this->_options['def_lng']; ?>" size="14" readonly="true" /></td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Edit MapType', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[maptype_edit]">
                                        <option value="HYBRID" <?php selected($this->_options['maptype_edit'], 'HYBRID'); ?>>HYBRID</option>
                                        <option value="ROADMAP" <?php selected($this->_options['maptype_edit'], 'ROADMAP'); ?>>ROADMAP</option>
                                        <option value="SATELLITE" <?php selected($this->_options['maptype_edit'], 'SATELLITE'); ?>>SATELLITE</option>
                                        <option value="TERRAIN" <?php selected($this->_options['maptype_edit'], 'TERRAIN'); ?>>TERRAIN</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Front MapType', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[maptype_front]">
                                        <option value="HYBRID" <?php selected($this->_options['maptype_front'], 'HYBRID'); ?>>HYBRID</option>
                                        <option value="ROADMAP" <?php selected($this->_options['maptype_front'], 'ROADMAP'); ?>>ROADMAP</option>
                                        <option value="SATELLITE" <?php selected($this->_options['maptype_front'], 'SATELLITE'); ?>>SATELLITE</option>
                                        <option value="TERRAIN" <?php selected($this->_options['maptype_front'], 'TERRAIN'); ?>>TERRAIN</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Map Api Key', 'wpecbd'); ?></td>
                                <td><input name="bd[map_api_key]" id="map_api_key" type="text" value="<?php echo esc_attr($this->_options['map_api_key']); ?>" size="14" /></td>
                            </tr>

                            <tr class="alternate">
                                <td class="row-title"><?php _e('Adsense ID', 'wpecbd'); ?></td>
                                <td><input name="bd[ad_pub_id]" id="ad_pub_id" type="text" value="<?php echo esc_attr($this->_options['ad_pub_id']); ?>" size="14" /></td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('Ad Type', 'wpecbd'); ?></td>
                                <td>
                                    <select style="width: 130px;" id="fe_p_ad_type" name="bd[fe_p_ad_type]">
                                        <option value="0" <?php selected($this->_options['fe_p_ad_type'], 0); ?>><?php _e('DISABLED', 'wpecbd'); ?>!</option>
                                        <option value="LEADERBOARD" <?php selected($this->_options['fe_p_ad_type'], 'LEADERBOARD'); ?>>LEADERBOARD</option>
                                        <option value="BANNER" <?php selected($this->_options['fe_p_ad_type'], 'BANNER'); ?>>BANNER</option>
                                        <option value="HALF_BANNER" <?php selected($this->_options['fe_p_ad_type'], 'HALF_BANNER'); ?>>HALF BANNER</option>
                                        <option value="SKYSCRAPER" <?php selected($this->_options['fe_p_ad_type'], 'SKYSCRAPER'); ?>>SKYSCRAPER</option>
                                        <option value="WIDE_SKYSCRAPER" <?php selected($this->_options['fe_p_ad_type'], 'WIDE_SKYSCRAPER'); ?>>WIDE SKYSCRAPER</option>
                                        <option value="VERTICAL_BANNER" <?php selected($this->_options['fe_p_ad_type'], 'VERTICAL_BANNER'); ?>>VERTICAL BANNER</option>
                                        <option value="BUTTON" <?php selected($this->_options['fe_p_ad_type'], 'BUTTON'); ?>>BUTTON</option>
                                        <option value="SMALL_SQUARE" <?php selected($this->_options['fe_p_ad_type'], 'SMALL_SQUARE'); ?>>SMALL SQUARE</option>
                                        <option value="SQUARE" <?php selected($this->_options['fe_p_ad_type'], 'SQUARE'); ?>>SQUARE</option>
                                        <option value="SMALL_RECTANGLE" <?php selected($this->_options['fe_p_ad_type'], 'SMALL_RECTANGLE'); ?>>SMALL RECTANGLE</option>
                                        <option value="MEDIUM_RECTANGLE" <?php selected($this->_options['fe_p_ad_type'], 'MEDIUM_RECTANGLE'); ?>>MEDIUM RECTANGLE</option>
                                        <option value="LARGE_RECTANGLE" <?php selected($this->_options['fe_p_ad_type'], 'LARGE_RECTANGLE'); ?>>LARGE RECTANGLE</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="alternate">
                                <td class="row-title"><?php _e('Edit Zoom', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[zoom_edit]">
                                        <?php
                                        for ($zoom_edit = 0; $zoom_edit < 21; $zoom_edit++) {
                                            echo '<option value="' . $zoom_edit . '" ' . selected($this->_options['zoom_edit'], $zoom_edit) . '>' . $zoom_edit . '</option>' . "\n";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="row-title"><?php _e('FrontEnd Zoom', 'wpecbd'); ?></td>
                                <td>
                                    <select name="bd[zoom_front]">
                                        <?php
                                        for ($zoom_front = 0; $zoom_front < 21; $zoom_front++) {
                                            echo '<option value="' . $zoom_front . '" ' . selected($this->_options['zoom_front'], $zoom_front) . '>' . $zoom_front . '</option>' . "\n";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div> <!-- .inner-sidebar -->

            <div id="post-body">
                <div id="post-body-content">
                    <div class="postbox">
                        <h3><span><?php _e('Free-Premium Form Fields, Features And Requirements', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <table class="widefat">
                                <tr>
                                    <th class="row-title"><?php _e('Feature Name', 'wpecbd'); ?></th>
                                    <th><?php _e('Free', 'wpecbd'); ?></th>
                                    <th><?php _e('Premium', 'wpecbd'); ?></th>
                                    <th><?php _e('Required', 'wpecbd'); ?></th>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Logo', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_logo]" id="bd_f_logo" type="checkbox" value="1" <?php checked($this->_options['f_logo'], 1) ?> /></td>
                                    <td><input name="bd[p_logo]" id="bd_p_logo" type="checkbox" value="1" <?php checked($this->_options['p_logo'], 1) ?> /></td>
                                    <td><input id="bd_r_logo" type="checkbox" value="0" disabled="disabled" /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Images', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_images]" id="bd_f_images" type="checkbox" value="1" <?php checked($this->_options['f_images'], 1) ?> /></td>
                                    <td><input name="bd[p_images]" id="bd_p_images" type="checkbox" value="1" <?php checked($this->_options['p_images'], 1) ?> /></td>
                                    <td><input id="bd_r_images" type="checkbox" value="0" disabled="disabled" /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Map', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_map]" id="bd_f_map" type="checkbox" value="1" <?php checked($this->_options['f_map'], 1) ?> /></td>
                                    <td><input name="bd[p_map]" id="bd_p_map" type="checkbox" value="1" <?php checked($this->_options['p_map'], 1) ?> /></td>
                                    <td><input name="bd[r_map]" id="bd_r_map" type="checkbox" value="1" <?php checked($this->_options['r_map'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Adress', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_address]" id="bd_f_address" type="checkbox" value="1" <?php checked($this->_options['f_address'], 1) ?> /></td>
                                    <td><input name="bd[p_address]" id="bd_p_address" type="checkbox" value="1" <?php checked($this->_options['p_address'], 1) ?> /></td>
                                    <td><input name="bd[r_address]" id="bd_r_address" type="checkbox" value="1" <?php checked($this->_options['r_address'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('City', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_city]" id="bd_f_city" type="checkbox" value="1" <?php checked($this->_options['f_city'], 1) ?> /></td>
                                    <td><input name="bd[p_city]" id="bd_p_city" type="checkbox" value="1" <?php checked($this->_options['p_city'], 1) ?> /></td>
                                    <td><input name="bd[r_city]" id="bd_r_city" type="checkbox" value="1" <?php checked($this->_options['r_city'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('State', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_state]" id="bd_f_state" type="checkbox" value="1" <?php checked($this->_options['f_state'], 1) ?> /></td>
                                    <td><input name="bd[p_state]" id="bd_p_state" type="checkbox" value="1" <?php checked($this->_options['p_state'], 1) ?> /></td>
                                    <td><input name="bd[r_state]" id="bd_r_state" type="checkbox" value="1" <?php checked($this->_options['r_state'], 1) ?> /></td>
                                </tr><tr class="alternate">
                                    <td class="row-title"><?php _e('Country', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_country]" id="bd_f_country" type="checkbox" value="1" <?php checked($this->_options['f_country'], 1) ?> /></td>
                                    <td><input name="bd[p_country]" id="bd_p_country" type="checkbox" value="1" <?php checked($this->_options['p_country'], 1) ?> /></td>
                                    <td><input name="bd[r_country]" id="bd_r_country" type="checkbox" value="1" <?php checked($this->_options['r_country'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Zip', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_zip]" id="bd_f_zip" type="checkbox" value="1" <?php checked($this->_options['f_zip'], 1) ?> /></td>
                                    <td><input name="bd[p_zip]" id="bd_p_zip" type="checkbox" value="1" <?php checked($this->_options['p_zip'], 1) ?> /></td>
                                    <td><input name="bd[r_zip]" id="bd_r_zip" type="checkbox" value="1" <?php checked($this->_options['r_zip'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Short Description', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_shortd]" id="bd_f_shortd" type="checkbox" value="1" <?php checked($this->_options['f_shortd'], 1) ?> /></td>
                                    <td><input name="bd[p_shortd]" id="bd_p_shortd" type="checkbox" value="1" <?php checked($this->_options['p_shortd'], 1) ?> /></td>
                                    <td><input name="bd[r_shortd]" id="bd_r_shortd" type="checkbox" value="1" <?php checked($this->_options['r_shortd'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Long Description', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_longd]" id="bd_f_longd" type="checkbox" value="1" <?php checked($this->_options['f_longd'], 1) ?> /></td>
                                    <td><input name="bd[p_longd]" id="bd_p_longd" type="checkbox" value="1" <?php checked($this->_options['p_longd'], 1) ?> /></td>
                                    <td><input name="bd[r_longd]" id="bd_r_longd" type="checkbox" value="1" disabled="disabled" checked="checked" /></td>
                                </tr><tr class="alternate">
                                    <td class="row-title"><?php _e('Phone', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_phone]" id="bd_f_phone" type="checkbox" value="1" <?php checked($this->_options['f_phone'], 1) ?> /></td>
                                    <td><input name="bd[p_phone]" id="bd_p_phone" type="checkbox" value="1" <?php checked($this->_options['p_phone'], 1) ?> /></td>
                                    <td><input name="bd[r_phone]" id="bd_r_phone" type="checkbox" value="1" <?php checked($this->_options['r_phone'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Fax', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_fax]" id="bd_f_fax" type="checkbox" value="1" <?php checked($this->_options['f_fax'], 1) ?> /></td>
                                    <td><input name="bd[p_fax]" id="bd_p_fax" type="checkbox" value="1" <?php checked($this->_options['p_fax'], 1) ?> /></td>
                                    <td><input name="bd[r_fax]" id="bd_r_fax" type="checkbox" value="1" <?php checked($this->_options['r_fax'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Web Site', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_website]" id="bd_f_website" type="checkbox" value="1" <?php checked($this->_options['f_website'], 1) ?> /></td>
                                    <td><input name="bd[p_website]" id="bd_p_website" type="checkbox" value="1" <?php checked($this->_options['p_website'], 1) ?> /></td>
                                    <td><input name="bd[r_website]" id="bd_r_website" type="checkbox" value="1" <?php checked($this->_options['r_website'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('eMail', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_email]" id="bd_f_email" type="checkbox" value="1" <?php checked($this->_options['f_email'], 1) ?> /></td>
                                    <td><input name="bd[p_email]" id="bd_p_email" type="checkbox" value="1" <?php checked($this->_options['p_email'], 1) ?> /></td>
                                    <td><input name="bd[r_email]" id="bd_r_email" type="checkbox" value="1" <?php checked($this->_options['r_email'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Twitter', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_twitter]" id="bd_f_twitter" type="checkbox" value="1" <?php checked($this->_options['f_twitter'], 1) ?> /></td>
                                    <td><input name="bd[p_twitter]" id="bd_p_twitter" type="checkbox" value="1" <?php checked($this->_options['p_twitter'], 1) ?> /></td>
                                    <td><input name="bd[r_twitter]" id="bd_r_twitter" type="checkbox" value="1" <?php checked($this->_options['r_twitter'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Facebook', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_facebook]" id="bd_f_facebook" type="checkbox" value="1" <?php checked($this->_options['f_facebook'], 1) ?> /></td>
                                    <td><input name="bd[p_facebook]" id="bd_p_facebook" type="checkbox" value="1" <?php checked($this->_options['p_facebook'], 1) ?> /></td>
                                    <td><input name="bd[r_facebook]" id="bd_r_facebook" type="checkbox" value="1" <?php checked($this->_options['r_facebook'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('GooglePlus', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_googleplus]" id="bd_f_googleplus" type="checkbox" value="1" <?php checked($this->_options['f_googleplus'], 1) ?> /></td>
                                    <td><input name="bd[p_googleplus]" id="bd_p_googleplus" type="checkbox" value="1" <?php checked($this->_options['p_googleplus'], 1) ?> /></td>
                                    <td><input name="bd[r_googleplus]" id="bd_r_googleplus" type="checkbox" value="1" <?php checked($this->_options['r_googleplus'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('LinkedIn', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_linkedin]" id="bd_f_linkedin" type="checkbox" value="1" <?php checked($this->_options['f_linkedin'], 1) ?> /></td>
                                    <td><input name="bd[p_linkedin]" id="bd_p_linkedin" type="checkbox" value="1" <?php checked($this->_options['p_linkedin'], 1) ?> /></td>
                                    <td><input name="bd[r_linkedin]" id="bd_r_linkedin" type="checkbox" value="1" <?php checked($this->_options['r_linkedin'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Rss', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_rss]" id="bd_f_rss" type="checkbox" value="1" <?php checked($this->_options['f_rss'], 1) ?> /></td>
                                    <td><input name="bd[p_rss]" id="bd_p_rss" type="checkbox" value="1" <?php checked($this->_options['p_rss'], 1) ?> /></td>
                                    <td><input name="bd[r_rss]" id="bd_r_rss" type="checkbox" value="1" <?php checked($this->_options['r_rss'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Skype', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_skype]" id="bd_f_skype" type="checkbox" value="1" <?php checked($this->_options['f_skype'], 1) ?> /></td>
                                    <td><input name="bd[p_skype]" id="bd_p_skype" type="checkbox" value="1" <?php checked($this->_options['p_skype'], 1) ?> /></td>
                                    <td><input name="bd[r_skype]" id="bd_r_skype" type="checkbox" value="1" <?php checked($this->_options['r_skype'], 1) ?> /></td>
                                </tr>
                            </table>
                        </div> <!-- .inside -->
                    </div> <!-- /Free-Premium Form Fields, Features And Requirements -->

                    <div class="postbox">
                        <h3><span>Image Sizes</span></h3>
                        <div class="inside">
                            <table class="widefat">
                                <tr>
                                    <th class="row-title"><?php _e('Image Name', 'wpecbd'); ?></th>
                                    <th><?php _e('Max Width', 'wpecbd'); ?></th>
                                    <th><?php _e('Max Height', 'wpecbd'); ?></th>
                                    <th><?php _e('Crop', 'wpecbd'); ?></th>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Logo', 'wpecbd'); ?></td>
                                    <td><input name="bd[img_logo_w]" id="bd_img_logo_w" type="text" value="<?php echo (int) $this->_options['img_logo_w']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_logo_h]" id="bd_img_logo_h" type="text" value="<?php echo (int) $this->_options['img_logo_h']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_logo_c]" id="bd_img_logo_c" type="checkbox" value="1" <?php checked($this->_options['img_logo_c'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Logo In Loop', 'wpecbd'); ?></td>
                                    <td><input name="bd[img_logo_loop_w]" id="bd_img_logo_loop_w" type="text" value="<?php echo (int) $this->_options['img_logo_loop_w']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_logo_loop_h]" id="bd_img_logo_loop_h" type="text" value="<?php echo (int) $this->_options['img_logo_loop_h']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_logo_loop_c]" id="bd_img_logo_loop_c" type="checkbox" value="1" <?php checked($this->_options['img_logo_loop_c'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Images - Thumb', 'wpecbd'); ?></td>
                                    <td><input name="bd[img_thumb_w]" id="bd_img_thumb_w" type="text" value="<?php echo (int) $this->_options['img_thumb_w']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_thumb_h]" id="bd_img_thumb_h" type="text" value="<?php echo (int) $this->_options['img_thumb_h']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_thumb_c]" id="bd_img_thumb_c" type="checkbox" value="1" <?php checked($this->_options['img_thumb_c'], 1) ?> /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Images - Large', 'wpecbd'); ?></td>
                                    <td><input name="bd[img_w]" id="bd_img_w" type="text" value="<?php echo (int) $this->_options['img_w']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_h]" id="bd_img_h" type="text" value="<?php echo (int) $this->_options['img_h']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[img_c]" id="bd_img_c" type="checkbox" value="1" <?php checked($this->_options['img_c'], 1) ?> /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('FrontEnd Map Image', 'wpecbd'); ?></td>
                                    <td><input name="bd[map_img_w]" id="bd_map_img_w" type="text" value="<?php echo (int) $this->_options['map_img_w']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[map_img_h]" id="bd_map_img_h" type="text" value="<?php echo (int) $this->_options['map_img_h']; ?>" class="small-text" /> px</td>
                                    <td> </td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('QR', 'wpecbd'); ?></td>
                                    <td><input name="bd[qr_w]" id="bd_qr_w" type="text" value="<?php echo (int) $this->_options['qr_w']; ?>" class="small-text" /> px</td>
                                    <td><input name="bd[qr_h]" id="bd_qr_h" type="text" value="<?php echo (int) $this->_options['qr_h']; ?>" class="small-text" /> px</td>
                                    <td> </td>
                                </tr>
                            </table>
                        </div> <!-- .inside -->
                    </div> <!-- /Image Sizes -->

                    <div class="postbox">
                        <h3><span>PayPal <?php _e('Options', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <table class="widefat">
                                <tr>
                                    <th colspan="2" class="row-title">PayPal <?php _e('Options', 'wpecbd'); ?></th>
                                    <th class="row-title"><?php _e('Premium Plans', 'wpecbd'); ?></th>
                                    <th><?php _e('Price', 'wpecbd'); ?></th>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('API Username', 'wpecbd'); ?></td>
                                    <td><input name="bd[paypal][user]" id="bd_paypal_user" type="text" value="<?php echo esc_attr($this->_options['paypal']['user']); ?>" ></td>

                                    <td class="row-title">1 <?php _e('Month', 'wpecbd'); ?></td>
                                    <td>
                                        <input name="bd[prem_plans][1 Month][price]" id="bd_prem_plans_1_Month" type="text" value="<?php echo $this->_options['prem_plans']['1 Month']['price']; ?>" />
                                        <input name="bd[prem_plans][1 Month][str]" type="hidden" value="+1 month" />
                                    </td>
                                </tr>

                                <tr class="alternate">
                                    <td class="row-title"><?php _e('API Password', 'wpecbd'); ?></td>
                                    <td><input name="bd[paypal][pass]" id="bd_paypal_pass" type="text" value="<?php echo esc_attr($this->_options['paypal']['pass']); ?>" ></td>

                                    <td class="row-title">3 <?php _e('Months', 'wpecbd'); ?></td>
                                    <td>
                                        <input name="bd[prem_plans][3 Months][price]" id="bd_prem_plans_3_Months" type="text" value="<?php echo $this->_options['prem_plans']['3 Months']['price']; ?>" />
                                        <input name="bd[prem_plans][3 Months][str]" type="hidden" value="+3 months" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="row-title"><?php _e('API Signature', 'wpecbd'); ?></td>
                                    <td><input name="bd[paypal][sign]" id="bd_paypal_sign" type="text" value="<?php echo esc_attr($this->_options['paypal']['sign']); ?>" ></td>

                                    <td class="row-title">6 <?php _e('Months', 'wpecbd'); ?></td>
                                    <td>
                                        <input name="bd[prem_plans][6 Months][price]" id="bd_prem_plans_6_Months" type="text" value="<?php echo $this->_options['prem_plans']['6 Months']['price']; ?>" />
                                        <input name="bd[prem_plans][6 Months][str]" type="hidden" value="+6 months" />
                                    </td>
                                </tr>

                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Local Page', 'wpecbd'); ?></td>
                                    <td>
                                        <select name="bd[paypal][loca]">
                                            <option value="AU" <?php selected($this->_options['paypal']['loca'], 'AU'); ?>>Australia</option>
                                            <option value="AT" <?php selected($this->_options['paypal']['loca'], 'AT'); ?>>Austria</option>
                                            <option value="BE" <?php selected($this->_options['paypal']['loca'], 'BE'); ?>>Belgium</option>
                                            <option value="BR" <?php selected($this->_options['paypal']['loca'], 'BR'); ?>>Brazil</option>
                                            <option value="CA" <?php selected($this->_options['paypal']['loca'], 'CA'); ?>>Canada</option>
                                            <option value="CH" <?php selected($this->_options['paypal']['loca'], 'CH'); ?>>Switzerland</option>
                                            <option value="CN" <?php selected($this->_options['paypal']['loca'], 'CN'); ?>>China</option>
                                            <option value="DE" <?php selected($this->_options['paypal']['loca'], 'DE'); ?>>Germany</option>
                                            <option value="ES" <?php selected($this->_options['paypal']['loca'], 'ES'); ?>>Spain</option>
                                            <option value="GB" <?php selected($this->_options['paypal']['loca'], 'GB'); ?>>United Kingdom</option>
                                            <option value="FR" <?php selected($this->_options['paypal']['loca'], 'FR'); ?>>France</option>
                                            <option value="IT" <?php selected($this->_options['paypal']['loca'], 'IT'); ?>>Italy</option>
                                            <option value="NL" <?php selected($this->_options['paypal']['loca'], 'NL'); ?>>Netherlands</option>
                                            <option value="PL" <?php selected($this->_options['paypal']['loca'], 'PL'); ?>>Poland</option>
                                            <option value="PT" <?php selected($this->_options['paypal']['loca'], 'PT'); ?>>Portugal</option>
                                            <option value="RU" <?php selected($this->_options['paypal']['loca'], 'RU'); ?>>Russia</option>
                                            <option value="US" <?php selected($this->_options['paypal']['loca'], 'US'); ?>>United States</option>
                                            <option value="da_DK" <?php selected($this->_options['paypal']['loca'], 'da_DK'); ?>>Danish (for Denmark only)</option>
                                            <option value="he_IL" <?php selected($this->_options['paypal']['loca'], 'he_IL'); ?>>Hebrew (all)</option>
                                            <option value="id_ID" <?php selected($this->_options['paypal']['loca'], 'id_ID'); ?>>Indonesian (for Indonesia only)</option>
                                            <option value="jp_JP" <?php selected($this->_options['paypal']['loca'], 'jp_JP'); ?>>Japanese (for Japan only)</option>
                                            <option value="no_NO" <?php selected($this->_options['paypal']['loca'], 'no_NO'); ?>>Norwegian (for Norway only)</option>
                                            <option value="pt_BR" <?php selected($this->_options['paypal']['loca'], 'pt_BR'); ?>>Brazilian Portuguese (for Portugal and Brazil only)</option>
                                            <option value="ru_RU" <?php selected($this->_options['paypal']['loca'], 'ru_RU'); ?>>Russian (for Lithuania, Latvia, and Ukraine only)</option>
                                            <option value="sv_SE" <?php selected($this->_options['paypal']['loca'], 'sv_SE'); ?>>Sweedish (for Sweden only)</option>
                                            <option value="th_TH" <?php selected($this->_options['paypal']['loca'], 'th_TH'); ?>>Thai (for Thailand only)</option>
                                            <option value="tr_TR" <?php selected($this->_options['paypal']['loca'], 'tr_TR'); ?>>Turkish (for Turkey only)</option>
                                            <option value="zh_CN" <?php selected($this->_options['paypal']['loca'], 'zh_CN'); ?>>Simplified Chinese (for China only)</option>
                                            <option value="zh_HK" <?php selected($this->_options['paypal']['loca'], 'zh_HK'); ?>>Traditional Chinese (for Hong Kong only)</option>
                                            <option value="zh_TW" <?php selected($this->_options['paypal']['loca'], 'zh_TW'); ?>>Traditional Chinese (for Taiwan only)</option>
                                        </select>
                                    </td>

                                    <td class="row-title">1 <?php _e('Year', 'wpecbd'); ?></td>
                                    <td>
                                        <input name="bd[prem_plans][1 Year][price]" id="bd_prem_plans_1_Year" type="text" value="<?php echo $this->_options['prem_plans']['1 Year']['price']; ?>" />
                                        <input name="bd[prem_plans][1 Year][str]" type="hidden" value="+1 year" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="row-title"><?php _e('Testing?', 'wpecbd'); ?></td>
                                    <td>
                                        <select name="bd[paypal][envi]">
                                            <option value="normal" <?php selected($this->_options['paypal']['envi'], 'normal'); ?>><?php _e('NO!, Normal', 'wpecbd'); ?></option>
                                            <option value="sandbox" <?php selected($this->_options['paypal']['envi'], 'sandbox'); ?>><?php _e('YES! Sandbox', 'wpecbd'); ?></option>
                                        </select>
                                    </td>

                                    <td class="row-title">2 <?php _e('Years', 'wpecbd'); ?></td>
                                    <td>
                                        <input name="bd[prem_plans][2 Years][price]" id="bd_prem_plans_2_Years" type="text" value="<?php echo $this->_options['prem_plans']['2 Years']['price']; ?>" />
                                        <input name="bd[prem_plans][2 Years][str]" type="hidden" value="+2 years" />
                                    </td>
                                </tr>

                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Store Logo', 'wpecbd'); ?></td>
                                    <td><input name="bd[paypal][img]" id="bd_paypal_img" type="text" value="<?php echo $this->_options['paypal']['img']; ?>" ></td>

                                    <td class="row-title">3 <?php _e('Years', 'wpecbd'); ?></td>
                                    <td>
                                        <input name="bd[prem_plans][3 Years][price]" id="bd_prem_plans_3_Years" type="text" value="<?php echo $this->_options['prem_plans']['3 Years']['price']; ?>" />
                                        <input name="bd[prem_plans][3 Years][str]" type="hidden" value="+3 years" />
                                    </td>
                                </tr>
                            </table>
                        </div> <!-- .inside -->
                    </div> <!-- /PayPal Options -->

                    <div class="postbox">
                        <h3><span><?php _e('Limits', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <table class="widefat">
                                <tr>
                                    <th class="row-title"><?php _e('Case', 'wpecbd'); ?></th>
                                    <th class="row-title"><?php _e('Limit', 'wpecbd'); ?></th>
                                    <th class="row-title"><?php _e('Case', 'wpecbd'); ?></th>
                                    <th class="row-title"><?php _e('Limit', 'wpecbd'); ?></th>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Max Category', 'wpecbd'); ?></td>
                                    <td><input name="bd[max_cat]" type="text" id="bd_max_cat" value="<?php echo (int) $this->_options['max_cat']; ?>" /></td>
                                    <td class="row-title"><?php _e('Max Tag', 'wpecbd'); ?></td>
                                    <td><input name="bd[max_tag]" type="text" id="bd_max_tag" value="<?php echo (int) $this->_options['max_tag']; ?>" /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Max Short Desc.', 'wpecbd'); ?></td>
                                    <td><input name="bd[shortd_max]" type="text" id="bd_shortd_max" value="<?php echo (int) $this->_options['shortd_max']; ?>" /></td>
                                    <td class="row-title"><?php _e('Max Long Desc.', 'wpecbd'); ?></td>
                                    <td><input name="bd[longd_max]" type="text" id="bd_longd_max" value="<?php echo (int) $this->_options['longd_max']; ?>" /></td>
                                </tr>
                                <tr>
                                    <td class="row-title"><?php _e('Min Short Desc.', 'wpecbd'); ?></td>
                                    <td><input name="bd[shortd_min]" type="text" id="bd_shortd_min" value="<?php echo (int) $this->_options['shortd_min']; ?>" /></td>
                                    <td class="row-title"><?php _e('Min Long Desc.', 'wpecbd'); ?></td>
                                    <td><input name="bd[longd_min]" type="text" id="bd_longd_min" value="<?php echo (int) $this->_options['longd_min']; ?>" /></td>
                                </tr>
                                <tr class="alternate">
                                    <td class="row-title"><?php _e('Free Image Upload', 'wpecbd'); ?></td>
                                    <td><input name="bd[f_maximg]" type="text" id="bd_f_maximg" value="<?php echo (int) $this->_options['f_maximg']; ?>" /></td>
                                    <td class="row-title"><?php _e('Premium Image Upload', 'wpecbd'); ?></td>
                                    <td><input name="bd[p_maximg]" type="text" id="bd_p_maximg" value="<?php echo (int) $this->_options['p_maximg']; ?>" /></td>
                                </tr>
                            </table>
                        </div> <!-- .inside -->
                    </div> <!-- /PayPal Options -->

                    <div class="postbox">
                        <h3><span><?php _e('Seo', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <table class="widefat">
                                <tr>
                                    <td>
                                        <input name="bd[seo_desc]" type="checkbox" id="seo_desc" value="1" <?php checked($this->_options['seo_desc'], 1); ?> />
                                        <label for="seo_desc"><?php _e('Autogenerate Descriptions', 'wpecbd'); ?></label>
                                    </td>
                                    <td>
                                        <input name="bd[seo_keywords]" type="checkbox" id="seo_keywords" value="1" <?php checked($this->_options['seo_keywords'], 1); ?> />
                                        <label for="seo_keywords"><?php _e('Use Categories and Tags for META keywords', 'wpecbd'); ?></label>
                                    </td>
                                    <td>
                                        <input name="bd[seo_noindex]" type="checkbox" id="seo_noindex" value="1" <?php checked($this->_options['seo_noindex'], 1); ?> />
                                        <label for="seo_noindex"><?php _e('Use noindex for Categories and Tags', 'wpecbd'); ?></label>
                                    </td>
                                </tr>
                            </table>
                        </div> <!-- .inside -->
                    </div> <!-- /Seo Options -->

                    <div class="postbox">
                        <h3><span><?php _ex('Captcha', 'admin area captcha', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <table class="widefat">
                                <tr>
                                    <td class="row-title"><?php _e('Public Key', 'wpecbd'); ?></td>
                                    <td><input name="bd[captcha_public_key]" type="text" id="bd_captcha_public_key" value="<?php echo esc_attr($this->_options['captcha_public_key']); ?>" /></td>
                                    <td class="row-title"><?php _e('Private Key', 'wpecbd'); ?></td>
                                    <td><input name="bd[captcha_private_key]" type="text" id="bd_captcha_private_key" value="<?php echo esc_attr($this->_options['captcha_private_key']); ?>" /></td>
                                </tr>
                            </table>
                        </div> <!-- .inside -->
                    </div> <!-- /Captcha -->

                    <input class="button-primary" type="submit" name="Save Options" value="<?php _e('Save Options', 'wpecbd'); ?>" />
                    <a id="bd_reset_options" class="button-secondary" href="#" title="<?php _e('Reset Options', 'wpecbd'); ?>"><?php _e('Reset Options', 'wpecbd'); ?></a>
                    <a id="bd_view_documentation" target="_blank" style="float: right;" class="button-secondary" href="http://www.wpbusinessdirectory.info/documentation/" title="<?php _e('Documentation', 'wpecbd'); ?>"><?php _e('Documentation', 'wpecbd'); ?></a>
                    <script>
                        /* <![CDATA[ */
                        jQuery('#bd_reset_options').click(function(){ 
                            if(window.confirm("<?php echo str_replace('"', '\"', __('Do you really want to reset all options?', 'wpecbd')); ?>"))
                            window.location.href = "<?php echo admin_url('edit.php?post_type=wpecbd&page=options&wpecbd_do=reset_options&_wpnonce=' . $nonce); ?>";
                        });
                        /* ]]> */
                    </script>
                </div> <!-- #post-body-content -->
            </div> <!-- #post-body -->

        </div> <!-- .metabox-holder -->
        <input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo $nonce; ?>" />
        <input type="hidden" id="current_version" name="bd[current_version]" value="<?php echo $this->version; ?>" />
    </form>

</div> <!-- .wrap -->