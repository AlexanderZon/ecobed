<?php
// Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style type="text/css">
            html, body, #mapcontainer {
                padding: 0;
                margin: 0;
                width: 100%;
                height: 100%;
                overflow: hidden;
                font-family:Arial,sans-serif;
                font-size:14px;
            }
            .me-secondary-link {
                color:#7777CC;
                cursor:pointer;
                font-weight:normal;
                text-decoration:underline;
            }
            .me-title {
                font-weight: bold;
            }
            .me-label {
                color:#676767;
                margin-right:4px;
            }
        </style>
    </head>
    <body onload="initialize()">
        <div id="mapcontainer"></div>
        <script src="//maps.google.com/maps/api/js?sensor=false<?php echo (($this->_options['fe_p_ad_type']) ? '&libraries=adsense' : '') . ((!empty($this->_options['map_api_key'])) ? '&key=' . $this->_options['map_api_key'] : ''); ?>"></script>
        <script type="text/javascript">
            /* <![CDATA[ */
           var map,marker,lat,lng,point,v,iw;
            lat=<?php echo $com->lat; ?>;
            lng=<?php echo $com->lng; ?>;
            v={
                mtitle:"<?php echo esc_attr($com->post_title); ?>",
                maddress1:"<?php echo str_replace("\r\n", ' ', esc_attr($com->address)); ?>",
                maddress2:"<?php echo esc_attr(implode(', ', $buff_add2)); ?>"
            };
            point=new google.maps.LatLng(lat, lng);
            var mapOptions = {
                center: point,
                zoom: <?php echo $this->_options['zoom_front']; ?>,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("mapcontainer"),mapOptions);
            marker = new google.maps.Marker({
                map: map,
                draggable: false,
                position: point,
                animation: google.maps.Animation.DROP
            });
            
            
            
            function showInfoWindow(v) {
                return function(place) {
                    if (iw) {
                        iw.close();
                        iw = null;
                    }

                    iw = new google.maps.InfoWindow({
                        content: getIWContent(v)
                    });
                    iw.open(map, marker);
                }
            }

            function getIWContent(v) {
                var container = document.createElement('div');
                var titleContainer = document.createElement('div');
                titleContainer.className = 'me-title';

                var title = document.createTextNode(v.mtitle);
                titleContainer.appendChild(title);
                container.appendChild(titleContainer);

                var addressContainer = document.createElement('div');
                addressContainer.className = 'me-address-lines';

                var address1 = document.createElement('div');
                address1.className = 'me-address-line';
                address1.appendChild(document.createTextNode(v.maddress1));
                addressContainer.appendChild(address1);
                if (v.maddress2) {
                    var address2 = document.createElement('div');
                    address2.className = 'me-address-line';
                    address2.appendChild(document.createTextNode(v.maddress2));
                    addressContainer.appendChild(address2);
                }

                container.appendChild(addressContainer);

                return container;
            }
<?php if ($this->_options['fe_p_ad_type']): ?>
            var adUnitDiv = document.createElement('div');
            var adUnitOptions = {
                format: google.maps.adsense.AdFormat["<?php echo $this->_options['fe_p_ad_type']; ?>"],
                position: google.maps.ControlPosition.RIGHT_BOTTOM,
                map: map,
                visible: true,
                publisherId: "<?php echo $this->_options['ad_pub_id']; ?>"
            }
            adUnit = new google.maps.adsense.AdUnit(adUnitDiv, adUnitOptions);
<?php endif; ?>

    google.maps.event.addListener(marker, 'click', showInfoWindow(v));
    
    google.maps.event.trigger(marker, 'click')
    /* ]]> */
        </script>
    </body>
    <?php echo $this->_options['fe_p_ad_type']; ?>
</html>