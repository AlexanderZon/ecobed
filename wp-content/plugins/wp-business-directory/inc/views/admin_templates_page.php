<?php
// Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");

if (isset($_GET['wpecbd_do']) AND (($_GET['wpecbd_do'] == 'save_templates') OR $_GET['wpecbd_do'] == 'reset_templates')) {
    //Security Check
    if (!wp_verify_nonce($_REQUEST['_wpnonce'], 'wpecbd_save_templates') AND !current_user_can('administrator'))
        die('Security check failed!!!');

    if ($_GET['wpecbd_do'] == 'reset_templates') {
        //Reset Templates
        update_option('wpecbd_tpl_inloop', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/inloop.html'));
        update_option('wpecbd_tpl_singlepage', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/singlepage.html'));
        update_option('wpecbd_tpl_feed', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/feed.html'));
        echo '<div class="updated"><p><strong>' . __('Templates has been successfully reset!', 'wpecbd') . '</strong></p></div>';
    } elseif ($_GET['wpecbd_do'] == 'save_templates') {
        //Save Templates
        update_option('wpecbd_tpl_inloop', stripslashes($_POST['inloop_tpl']));
        update_option('wpecbd_tpl_singlepage', stripslashes($_POST['single_tpl']));
        update_option('wpecbd_tpl_feed', stripslashes($_POST['feed_tpl']));
        echo '<div class="updated"><p><strong>' . __('Templates successfully saved!', 'wpecbd') . '</strong></p></div>';
    }
}
?>
<link rel="stylesheet" href="<?php echo WPECBD_PLUGIN_URL . 'css/codemirror.css'; ?>">
<script src="<?php echo WPECBD_PLUGIN_URL . 'js/codemirror/codemirror.js'; ?>"></script>
<script src="<?php echo WPECBD_PLUGIN_URL . 'js/codemirror/overlay.js'; ?>"></script>
<script src="<?php echo WPECBD_PLUGIN_URL . 'js/codemirror/xml.js' ?>"></script>
<script src="<?php echo WPECBD_PLUGIN_URL . 'js/codemirror/javascript.js'; ?>"></script>
<script src="<?php echo WPECBD_PLUGIN_URL . 'js/codemirror/css.js'; ?>"></script>
<script src="<?php echo WPECBD_PLUGIN_URL . 'js/codemirror/htmlmixed.js'; ?>"></script>
<style type="text/css">
    .CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black;}
    .cm-bdvariable {color: #0ca;}
</style>
<div class="wrap">
    <div id="icon-options-general" class="icon32"></div>
    <h2>WP Business Directory - <?php _e('Template Editor', 'wpecbd'); ?></h2>
    <form name="wpecbd_options" method="post" action="<?php echo admin_url('edit.php?post_type=wpecbd&page=templates&wpecbd_do=save_templates&_wpnonce=' . $nonce); ?>">

        <div class="metabox-holder">
            <div id="post-body">
                <div id="post-body-content">
                    <div class="postbox">
                        <h3><span><?php _e('Single Page', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <textarea id="single_tpl" name="single_tpl"><?php echo get_option('wpecbd_tpl_singlepage', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/singlepage.html')); ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="metabox-holder">
            <div id="post-body">
                <div id="post-body-content">
                    <div class="postbox">
                        <h3><span><?php _e('Inloop', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <textarea id="inloop_tpl" name="inloop_tpl"><?php echo get_option('wpecbd_tpl_inloop', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/inloop.html')); ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="metabox-holder">
            <div id="post-body">
                <div id="post-body-content">
                    <div class="postbox">
                        <h3><span><?php _e('RSS Feed', 'wpecbd'); ?></span></h3>
                        <div class="inside">
                            <textarea id="feed_tpl" name="feed_tpl"><?php echo get_option('wpecbd_tpl_feed', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/feed.html')); ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input class="button-primary" type="submit" name="Save Templates" value="<?php _e('Save Templates', 'wpecbd'); ?>" />
        <a id="bd_reset_templates" class="button-secondary" href="#" title="<?php _e('Reset Templates', 'wpecbd'); ?>"><?php _e('Reset Templates', 'wpecbd'); ?></a>
    </form>

    <script>
        /* <![CDATA[ */
        jQuery('#bd_reset_templates').click(function(){ 
            if(window.confirm("<?php echo str_replace('"', '\"', __('Do you really want to reset all templates?', 'wpecbd')); ?>"))
            window.location.href = "<?php echo admin_url('edit.php?post_type=wpecbd&page=templates&wpecbd_do=reset_templates&_wpnonce=' . $nonce); ?>";
        });
        
        CodeMirror.defineMode("bdvariable", function(config, parserConfig) {
            var bdvariableOverlay = {
                token: function(stream, state) {
                    var ch;
                    if (stream.match("%%")) {
                        while ((ch = stream.next()) != null)
                            if (ch == "%" && stream.next() == "%") break;
                        return "bdvariable";
                    }
                    while (stream.next() != null && !stream.match("%%", false)) {}
                    return null;
                }
            };
            return CodeMirror.overlayMode(CodeMirror.getMode(config, parserConfig.backdrop || "text/html"), bdvariableOverlay);
        });        
        var options = {mode: "bdvariable" , tabMode: "indent"};
        var inloop_tpl = CodeMirror.fromTextArea(document.getElementById("inloop_tpl"), options);
        var single_tpl = CodeMirror.fromTextArea(document.getElementById("single_tpl"), options);
        var feed_tpl = CodeMirror.fromTextArea(document.getElementById("feed_tpl"), options);
        /* ]]> */
    </script>

</div>