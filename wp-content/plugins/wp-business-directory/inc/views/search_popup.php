<?php
// Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");

//Check Widgets Options
$widget_options = get_option('widget_wpbd_com_search');
if (isset($widget_options[@$_GET['widget_id']])) {
    $options = $widget_options[$_GET['widget_id']];
} else {
    $options = array(
        'use_cat' => 1,
        'hide_empty_cats' => 1,
        'cats_show_count' => 0,
        'cats_show_childs' => 1,
        'unit' => 'km',
        'def_distance' => 50,
        'ad_active' => 0,
        'publisherId' => 'YOUR_PUBLISHER_ID',
        'ad_unit' => 'HALF_BANNER',
        'ad_position' => 'RIGHT_BOTTOM'
    );
}
?>
<!DOCTYPE html>
<html>
    <head>
        <script src="//maps.google.com/maps/api/js?sensor=false<?php echo (($options['ad_active']) ? '&libraries=adsense' : '') . ((!empty($this->_options['map_api_key'])) ? '&key=' . $this->_options['map_api_key'] : ''); ?>"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
        <script src="<?php echo WPECBD_PLUGIN_URL . 'js/jquery.multiselect.js'; ?>"></script>
        <script src="<?php echo WPECBD_PLUGIN_URL . 'js/wpecbd-popup_search.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo WPECBD_PLUGIN_URL . 'css/reset.css'; ?>" />
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/<?php echo apply_filters('bd_search_popup_theme', 'smoothness'); ?>/jquery-ui.css" />
        <link rel="stylesheet" href="<?php echo WPECBD_PLUGIN_URL . 'css/search_popup.css'; ?>" />
        <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script>
            /* <![CDATA[ */
            var ajax_search_url="<?php echo site_url('?bd_widget_ajax=ajax_search'); ?>";
            var plugin_img_folder="<?php echo WPECBD_PLUGIN_URL . 'img/' ?>";
            var measurement_unit="<?php echo $options['unit']; ?>";
            var detect_location=<?php echo (isset($_GET['detect_location']) AND $_GET['detect_location'] == 0) ? 'false' : 'true'; ?>;
            var address_setted=<?php echo (isset($_GET['lat']) AND $_GET['lat'] != $this->_options['def_lat']) ? 'true' : 'false' ?>;
            var adsense={
                active:<?php echo ($options['ad_active']) ? 'true' : 'false'; ?>,
                publisherId:"<?php echo $this->_options['ad_pub_id']; ?>",
                ad_unit:"<?php echo $options['ad_unit']; ?>",
                ad_position:"<?php echo $options['ad_position']; ?>"
            };
            var def_location={
                lat:<?php echo $this->_options['def_lat']; ?>,
                lng:<?php echo $this->_options['def_lng'] ?>
            };
            var lang={
              position:{
                  unable_to_find:"<?php echo esc_attr(__('Unable to find your location', 'wpecbd')); ?>",
                  permisson_denied:"<?php echo esc_attr(__('Permission denied in finding your location', 'wpecbd')); ?>",
                  unknown:"<?php echo esc_attr(__('Your location is currently unknown', 'wpecbd')); ?>",
                  too_long:"<?php echo esc_attr(__('Attempt to find location took too long', 'wpecbd')); ?>",
                  no_browser_support:"<?php echo esc_attr(__('Location detection not supported in browser', 'wpecbd')); ?>",
                  on_progress:"<?php echo esc_attr(__('Detecting your location... Please Wait...', 'wpecbd')); ?>",
                  success:"<?php echo esc_attr(__('Your location is successfully obtained!', 'wpecbd')); ?>"
              },
              map:{
                  d_marker_title:"<?php echo esc_attr(__('You Are Here!', 'wpecbd')); ?>",
                  more_info:"<?php echo esc_attr(__('More Info', 'wpecbd')); ?>",
                  get_direction:"<?php echo esc_attr(__('Get Direction', 'wpecbd')); ?>",
                  visit_company_page:"<?php echo esc_attr(__('Visit Company Page', 'wpecbd')); ?>"
              },
              search:{
                  searching:"<?php echo esc_attr(__('Searching please wait...', 'wpecbd')); ?>",
                  error:"<?php echo esc_attr(__('Something unexpected has happened... Search Error!', 'wpecbd')); ?>",
                  no_found:"<?php echo esc_attr(__('Sorry! No companies found.')); ?>",
                  xp_found:"<?php echo esc_attr(__('# Companies found!', 'wpecbd')); ?>",
                  one_found:"<?php echo esc_attr(__('1 Company found!', 'wpecbd')); ?>",
                  required:"<?php echo esc_attr(__('Search terms must be at least 3 characters or you must select at least one category.', 'wpecbd')); ?>"
              },
              cat:{
                  noneSelectedText:"<?php echo esc_attr(__('Select Categories', 'wpecbd')); ?>",
                  selectedText:"<?php echo esc_attr(__('# selected', 'wpecbd')); ?>"
              },
              directions:{
                  error:"<?php echo esc_attr(__('Something unexpected has happened... Directions Error!', 'wpecbd')); ?>"
              },
              iw:{
                  address:"<?php echo esc_attr(__('Address', 'wpecbd')); ?>",
                  phone:"<?php echo esc_attr(__('Phone', 'wpecbd')); ?>"
              }
            };
            /* ]]> */
        </script>
    </head>
    <body onload="initialize()">
        <div id="main">
            <div id="places_main">
                <div id="directions_main">
                    <div class="ui-state-default ui-corner-all directions_close"><span class="ui-icon ui-icon-closethick"></span><span class="directions_close_text"><?php _e('Close Direction Panel', 'wpecbd'); ?></span><br class="clear" /></div>
                    <div id="directions_panel"></div>
                    <div class="ui-state-default ui-corner-all directions_close"><span class="ui-icon ui-icon-closethick"></span><span class="directions_close_text"><?php _e('Close Direction Panel', 'wpecbd'); ?></span><br class="clear" /></div>
                </div>
                <div id="places">
                    <div id="places_title"><?php _e('Search Results', 'wpecbd'); ?></div>
                    <ul id="places_ul">
                    </ul>
                </div>
            </div>
            <div id="map_main">
                <div class="ui-state-default ui-corner-all search_open" title="<?php _e('Open Search Box', 'wpecbd'); ?>"><span class="ui-icon ui-icon-extlink"></span></div>
                <div id="search_base">
                    <form id="bd_com_search_form" action="#">
                        <label id="distance_label" for="distance"><?php _e('Distance', 'wpecbd'); ?>:</label>
                        <span id="unit_text"><?php echo (($options['unit'] == 'km') ? 'km' : 'mi') ?></span>
                        <input id="distance" type="text" name="distance" value="<?php echo $options['def_distance']; ?>" maxlength="4"/>
                        <label for="term"><?php _e('Search Term', 'wpecbd'); ?></label>
                        <input id="term" type="text" name="term" value="<?php echo esc_attr(@$_GET['term']); ?>" />
                        <label for="autocomplete_adress"><?php _e('Location', 'wpecbd'); ?></label>
                        <input id="autocomplete_adress" type="text" name="term" value="<?php echo esc_attr(@$_GET['address']); ?>" />
                        <input id="lat" type="hidden" name="lat" value="<?php echo esc_attr(@$_GET['lat']); ?>" />
                        <input id="lng" type="hidden" name="lng" value="<?php echo esc_attr(@$_GET['lng']); ?>" />
                        <?php
                        if ($options['use_cat'])
                            prepare_categories((bool) $options['hide_empty_cats'], (bool) $options['cats_show_childs'], (bool) $options['cats_show_count']);
                        ?>
                        <input id="search_button" type="submit" value="<?php _e('Search', 'wpecbd'); ?>" />
                    </form>
                    <div class="ui-state-default ui-corner-all search_close" title="<?php _e('Close Search Box', 'wpecbd'); ?>"><span class="ui-icon ui-icon-extlink"></span></div>
                </div>
                <div id="info"></div>
                <div id="map_canvas"></div>
            </div>
        </div>
    </body>
</html>
<?php

function prepare_categories($hide_empty_cats=TRUE, $cats_show_childs=TRUE, $cats_show_count=FALSE, $selected=array()) {
    $terms_args = array();
    $terms_args['hide_empty'] = $hide_empty_cats;

    $selected_cats = array();
    if (isset($_GET['cats']) AND is_array($_GET['cats'])) {
        foreach ($_GET['cats'] as $s_c_v) {
            if (is_numeric($s_c_v))
                $selected_cats[] = $s_c_v;
        }
    }

    $all_cats = get_terms('bd_categories', $terms_args);

    $cat_buff = array();
    foreach ($all_cats as $cat1 => $cat2) {
        if ($cat2->parent == 0) {
            $cat_buff[$cat2->term_id]['name'] = $cat2->name;
            $cat_buff[$cat2->term_id]['count'] = $cat2->count;
        } else {
            $cat_buff[$cat2->parent]['child'][$cat2->term_id]['name'] = $cat2->name;
            $cat_buff[$cat2->parent]['child'][$cat2->term_id]['count'] = $cat2->count;
        }
    }

    if (empty($cat_buff)) {
        echo 'Please Add Some Category!';
        return;
    }

    echo '<select name="bd_categories[]" id="cat_selector" multiple="multiple">';

    if ($cats_show_childs) {
        foreach ($cat_buff as $catea1 => $catea2) {
            echo '<optgroup label="' . $catea2['name'] . '">';
            if (empty($catea2['child'])) {
                echo '<option value="' . $catea1 . '" >' . $catea2['name'] . '</option>';
            } else {
                foreach ($catea2['child'] as $cateb1 => $cateb2) {
                    echo '<option value="' . $cateb1 . '" ' . ((in_array($cateb1, $selected_cats)) ? 'selected="selected"' : '') . '>' . $cateb2['name'] . (($cats_show_count) ? ' (' . $catea2['count'] . ')' : '') . '</option>';
                }
            }
            echo '</optgroup>';
        }
    } else {
        foreach ($cat_buff as $catea1 => $catea2) {
            echo '<option value="' . $catea1 . '" ' . ((in_array($catea1, $selected_cats)) ? 'selected="selected"' : '') . '>' . $catea2['name'] . (($cats_show_count) ? ' (' . $catea2['count'] . ')' : '') . '</option>';
        }
    }
    echo '</select>';
}
