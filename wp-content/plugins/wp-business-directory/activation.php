<?php

if (!file_exists(WPECBD_PLUGIN_DIR . 'db.sql') OR !$wpdb->query(str_replace('%%TABLE%%', $wpdb->prefix . 'wpecbd', file_get_contents(WPECBD_PLUGIN_DIR . 'db.sql'))))
    die('SQL File Missing! or DB isn\'t reachable');

/*
 * Create Folders
 */
if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory'))
    mkdir(WP_CONTENT_DIR . '/wp_business_directory');

if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory' . '/languages'))
    mkdir(WP_CONTENT_DIR . '/wp_business_directory' . '/languages');

if (!is_dir(WP_CONTENT_DIR . '/wp_business_directory' . '/upload'))
    mkdir(WP_CONTENT_DIR . '/wp_business_directory' . '/upload');

if (!get_option('wpecbd_tpl_inloop'))
    add_option('wpecbd_tpl_inloop', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/inloop.html'));


if (!get_option('wpecbd_tpl_singlepage'))
    add_option('wpecbd_tpl_singlepage', file_get_contents(WPECBD_PLUGIN_DIR . 'inc/default_templates/singlepage.html'));
/*
 * Flushing Rewrites
 */
flush_rewrite_rules();