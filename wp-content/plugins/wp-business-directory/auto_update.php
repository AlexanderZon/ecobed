<?php

// Stop direct call
defined('ABSPATH') or die("Cannot access pages directly.");

if (!class_exists('wp_business_directory_auto_update')) {

    class wp_business_directory_auto_update {

        /**
         * The plugin current version
         * @var string
         */
        public $current_version;

        /**
         * The plugin remote update path
         * @var string
         */
        public $update_path='http://update.wpbusinessdirectory.info/index.php';

        /**
         * Plugin Slug (plugin_directory/plugin_file.php)
         * @var string
         */
        public $plugin_slug;

        /**
         * Plugin name (plugin_file)
         * @var string
         */
        public $slug;

        /**
         * Purchase Code
         * @var string 
         */
        public $purchase_code;

        /**
         * Initialize a new instance of the WordPress Auto-Update class
         * @param string $current_version
         * @param string $plugin_slug
         */
        function __construct($current_version, $plugin_slug, $purchase_code) {
            // Set the class public variables
            $this->current_version = $current_version;
            $this->plugin_slug = $plugin_slug;
            $this->purchase_code = $purchase_code;
            list ($t1, $t2) = explode('/', $plugin_slug);
            $this->slug = str_replace('.php', '', $t2);

            // define the alternative API for updating checking
            add_filter('pre_set_site_transient_update_plugins', array(&$this, 'check_update'));

            // Define the alternative response for information checking
            add_filter('plugins_api', array(&$this, 'check_info'), 10, 3);
        }

        /**
         * Add our self-hosted autoupdate plugin to the filter transient
         *
         * @param $transient
         * @return object $ transient
         */
        public function check_update($transient) {
            
            if (empty($transient->checked)) {
                return $transient;
            }

            // Get the remote version
            $remote_version = $this->getRemote_version();

            // If a newer version is available, add the update
            if (version_compare($this->current_version, $remote_version->new_version, '<')) {
                $remote_version->slug = $this->slug;
                $transient->response[$this->plugin_slug] = $remote_version;
            }
            return $transient;
        }

        /**
         * Add our self-hosted description to the filter
         *
         * @param boolean $false
         * @param array $action
         * @param object $arg
         * @return bool|object
         */
        public function check_info($false, $action, $arg) {

            if ($arg->slug == $this->slug) {
                $information = $this->getRemote_information();
                return $information;
            }
            return false;
        }

        /**
         * Return the remote version
         * @return string $remote_version
         */
        public function getRemote_version() {
            $request = wp_remote_post($this->update_path, array('body' => array('action' => 'version', 'purchase_code' => $this->purchase_code, 'site_info' => $this->prepare_blog_info())));
            if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200) {
                return unserialize($request['body']);
            }
            return false;
        }

        /**
         * Get information about the remote version
         * @return bool|object
         */
        public function getRemote_information() {
            $request = wp_remote_post($this->update_path, array('body' => array('action' => 'info', 'purchase_code' => $this->purchase_code, 'site_info' => $this->prepare_blog_info())));
            if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200) {
                return unserialize($request['body']);
            }
            return false;
        }

        private function prepare_blog_info() {

            return array(
                'wpurl' => get_bloginfo('wpurl'),
                'wp_version' => get_bloginfo('version'),
                'plugin_version' => $this->current_version
            );
        }

    }

}