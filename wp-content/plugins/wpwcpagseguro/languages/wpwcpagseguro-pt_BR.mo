��          �   %   �      p  2   q     �     �     �  x   �     K  	   \     f  �   r  �   /     �     �     �     �       	        '      =     ^  7   k  x   �          "     (  /   �  '   �  �      �  �  1   �	     �	  1   �	     
  �   (
     �
  	   �
     �
  �   �
  �   �     V     ]     p     �     �  	   �     �  4   �       ?   %  �   e     �     �  �   �  8   w  -   �  �   �                                                                                   	          
                              Change your store currency to Brazilian Real (R$). Charset Checkout description. Checkout title. Choose between many payment methods, including credit cards and Brazilian banks. Pay securely and quickly with PagSeguro Create log file? Debug Log Description Do not have a PagSeguro account? <a href="https://pagseguro.uol.com.br/registration/registration.jhtml?ep=11&tipo=cadastro#!vendedor" target="_blank">Click here </a> and register for free. Do not have or do not know your token? <a href="https://pagseguro.uol.com.br/integracao/token-de-seguranca.jhtml" target="_blank">Click here </a> to generate a new one. E-Mail Enable module? Enable/Disable Invoice Prefix Notification URL PagSeguro Path to the log file. Prefix for your invoice numbers. Redirect URL Set the charset according to the coding of your system. Sorry, unfortunately there was an error during checkout. Please contact the store administrator if the problem persists. Title Token Whenever a transaction change its status, the PagSeguro sends a notification to your store or to the URL entered in this field. You should inform your PagSeguro account email. You should inform your PagSeguro token. Your customer will be redirected back to your store or to the URL entered in this field. <a href="https://pagseguro.uol.com.br/integracao/pagamentos-via-api.jhtml" target="_blank">Click here </a> to activate. Project-Id-Version: WordPresWooCommerce PagSeguro Oficial
POT-Creation-Date: 2013-07-29 11:35-0300
PO-Revision-Date: 2013-07-29 11:35-0300
Last-Translator: 
Language-Team: 
Language: Portuguese
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: _;gettext;gettext_noop;__
X-Poedit-Basepath: ..\
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: classes
 Mude sua moeda da loja para Real Brasileiro (R$). Charset Descrição que será visualizada pelo comprador. Nome para exibição. Escolha entre muitos meios de pagamento, incluindo cartões de crédito, transferência eletrônica e boleto. Pague de forma segura e rápida com PagSeguro. Criar arquivo de log? Debug Log Descrição Não tem uma conta no PagSeguro? <a href="https://pagseguro.uol.com.br/registration/registration.jhtml?ep=11&tipo=cadastro#!vendedor" target="_blank">Click aqui </a> e registre-se gratuitamente. Não tem ou não sabe o seu token? <a href="https://pagseguro.uol.com.br/integracao/token-de-seguranca.jhtml" target="_blank">Click aqui </a> para gerar um novo. E-Mail Habilitar módulo? Habilitar/Desabilitar Prefixo dos Pedidos URL de Notificação PagSeguro Caminho para o arquivo de log. Informe um prefixo para os números de seus pedidos. URL de Redirecionamento Definir o charset de acordo com a codificação do seu sistema. Desculpe, infelizmente, houve um erro durante o checkout. Entre em contato com o administrador da loja, se o problema persistir. Título Token Sempre que uma transação mudar seu status, o PagSeguro envia uma notificação para sua loja ou para a URL inserida neste campo. Você deve informar seu e-mail de cadastro no PagSeguro. Você deve informar o seu token no PagSeguro. Seu cliente será redirecionado para sua loja ou para a URL inserida neste campo. <a href="https://pagseguro.uol.com.br/integracao/pagamentos-via-api.jhtml" target="_blank">Click aqui </a> para ativar. 